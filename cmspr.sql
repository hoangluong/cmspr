/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100138
 Source Host           : localhost:3306
 Source Schema         : cmspr

 Target Server Type    : MySQL
 Target Server Version : 100138
 File Encoding         : 65001

 Date: 23/03/2020 16:00:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for agencies
-- ----------------------------
DROP TABLE IF EXISTS `agencies`;
CREATE TABLE `agencies`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_by_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `modified_on` datetime(0) NULL DEFAULT NULL,
  `website_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of agencies
-- ----------------------------
INSERT INTO `agencies` VALUES (1, 'Admicro', 'tesst', NULL, NULL, '2020-03-04 16:26:45', NULL, 17);

-- ----------------------------
-- Table structure for article_logs
-- ----------------------------
DROP TABLE IF EXISTS `article_logs`;
CREATE TABLE `article_logs`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `article_id` int(11) NULL DEFAULT NULL,
  `action` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `modified_on` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 88 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of article_logs
-- ----------------------------
INSERT INTO `article_logs` VALUES (1, 1, 2, '[luonghx] đã khởi tạo bài viết #2', '2020-02-04 16:59:22', NULL);
INSERT INTO `article_logs` VALUES (2, 1, 3, '[luonghx] đã khởi tạo bài viết #3', '2020-02-04 17:01:25', NULL);
INSERT INTO `article_logs` VALUES (3, 1, 4, '[luonghx] đã khởi tạo bài viết #4', '2020-02-05 10:31:30', NULL);
INSERT INTO `article_logs` VALUES (4, 1, 4, '[luonghx] đã hủy bài viết #4', '2020-02-06 15:20:57', NULL);
INSERT INTO `article_logs` VALUES (5, 1, 3, '[luonghx] đã hủy bài viết #3', '2020-02-06 15:21:18', NULL);
INSERT INTO `article_logs` VALUES (6, 1, 2, '[luonghx] đã khôi phục bài viết #2', '2020-02-06 15:32:33', NULL);
INSERT INTO `article_logs` VALUES (7, 1, 2, '[luonghx] đã hủy bài viết #2', '2020-02-06 15:33:27', NULL);
INSERT INTO `article_logs` VALUES (8, 1, 2, '[luonghx] đã khôi phục bài viết #2', '2020-02-06 15:34:11', NULL);
INSERT INTO `article_logs` VALUES (9, 1, 2, '[luonghx] đã hủy bài viết #2', '2020-02-06 15:34:26', NULL);
INSERT INTO `article_logs` VALUES (10, 1, 2, '[luonghx] đã khôi phục bài viết #2', '2020-02-06 15:34:33', NULL);
INSERT INTO `article_logs` VALUES (11, 1, 2, '[luonghx] đã hủy bài viết #2', '2020-02-06 15:35:03', NULL);
INSERT INTO `article_logs` VALUES (12, 1, 2, '[luonghx] đã khôi phục bài viết #2', '2020-02-06 15:35:52', NULL);
INSERT INTO `article_logs` VALUES (13, 1, 2, '[luonghx] đã hủy bài viết #2', '2020-02-06 15:36:08', NULL);
INSERT INTO `article_logs` VALUES (14, 1, 2, '[luonghx] đã khôi phục bài viết #2', '2020-02-06 15:36:28', NULL);
INSERT INTO `article_logs` VALUES (15, 1, 22963, '[luonghx] đã hủy bài viết #22963', '2020-02-06 15:43:07', NULL);
INSERT INTO `article_logs` VALUES (16, 1, 22963, '[luonghx] đã khôi phục bài viết #22963', '2020-02-06 15:43:11', NULL);
INSERT INTO `article_logs` VALUES (17, 1, 22963, '[luonghx] đã hủy bài viết #22963', '2020-02-06 15:43:49', NULL);
INSERT INTO `article_logs` VALUES (18, 1, 22963, '[luonghx] đã khôi phục bài viết #22963', '2020-02-06 15:43:52', NULL);
INSERT INTO `article_logs` VALUES (19, 1, 22963, '[luonghx] đã gửi duyệt bài viết #22963', '2020-02-06 15:51:49', NULL);
INSERT INTO `article_logs` VALUES (20, 1, 22963, '[luonghx] Duyệt bài viết #22963', '2020-02-06 16:03:01', NULL);
INSERT INTO `article_logs` VALUES (21, 1, 22963, '[luonghx] Đăng bài viết #22963', '2020-02-06 16:11:43', NULL);
INSERT INTO `article_logs` VALUES (22, 1, 2, '[luonghx] đã hủy bài viết #2', '2020-02-06 16:18:39', NULL);
INSERT INTO `article_logs` VALUES (23, 1, 2, '[luonghx] đã khôi phục bài viết #2', '2020-02-06 16:18:45', NULL);
INSERT INTO `article_logs` VALUES (24, 1, 2, '[luonghx] đã hủy bài viết #2', '2020-02-06 16:18:51', NULL);
INSERT INTO `article_logs` VALUES (25, 1, 2, '[luonghx] đã khôi phục bài viết #2', '2020-02-06 16:19:16', NULL);
INSERT INTO `article_logs` VALUES (26, 1, 2, '[luonghx] đã hủy bài viết #2', '2020-02-06 16:25:04', NULL);
INSERT INTO `article_logs` VALUES (27, 1, 2, '[luonghx] đã khôi phục bài viết #2', '2020-02-06 16:25:15', NULL);
INSERT INTO `article_logs` VALUES (28, 1, 2, '[luonghx] đã hủy bài viết #2', '2020-02-06 16:25:18', NULL);
INSERT INTO `article_logs` VALUES (29, 1, 2, '[luonghx] đã khôi phục bài viết #2', '2020-02-06 16:25:48', NULL);
INSERT INTO `article_logs` VALUES (30, 1, 2, '[luonghx] đã hủy bài viết #2', '2020-02-06 16:27:35', NULL);
INSERT INTO `article_logs` VALUES (31, 1, 2, '[luonghx] đã khôi phục bài viết #2', '2020-02-06 16:30:15', NULL);
INSERT INTO `article_logs` VALUES (32, 1, 2, '[luonghx] đã hủy bài viết #2', '2020-02-06 16:41:18', NULL);
INSERT INTO `article_logs` VALUES (33, 1, 4, '[luonghx] đã khôi phục bài viết #4', '2020-02-07 10:46:53', NULL);
INSERT INTO `article_logs` VALUES (34, 1, 4, '[luonghx] đã gửi duyệt bài viết #4', '2020-02-07 10:46:59', NULL);
INSERT INTO `article_logs` VALUES (35, 1, 4, '[luonghx] Duyệt bài viết #4', '2020-02-07 10:54:13', NULL);
INSERT INTO `article_logs` VALUES (36, 1, 2, '[luonghx] đã khôi phục bài viết #2', '2020-02-11 17:40:20', NULL);
INSERT INTO `article_logs` VALUES (37, 1, 4, '[luonghx] Đăng bài viết #4', '2020-02-13 16:22:33', NULL);
INSERT INTO `article_logs` VALUES (38, 15, 22964, '[Bientapvien_k14] đã khởi tạo bài viết #22964', '2020-02-17 16:06:28', NULL);
INSERT INTO `article_logs` VALUES (39, 15, 22964, '[Bientapvien_k14] đã gửi duyệt bài viết #22964', '2020-02-17 16:08:11', NULL);
INSERT INTO `article_logs` VALUES (40, 2, 22964, '[nam1] Duyệt bài viết #22964', '2020-02-17 16:15:27', NULL);
INSERT INTO `article_logs` VALUES (41, 2, 22964, '[nam1] Đăng bài viết #22964', '2020-02-17 16:24:19', NULL);
INSERT INTO `article_logs` VALUES (42, 15, 22965, '[Bientapvien_k14] đã khởi tạo bài viết #22965', '2020-02-20 11:21:14', NULL);
INSERT INTO `article_logs` VALUES (43, 2, 22966, '[nam1] đã khởi tạo bài viết #22966', '2020-02-20 11:44:18', NULL);
INSERT INTO `article_logs` VALUES (44, 15, 22965, '[Bientapvien_k14] đã gửi duyệt bài viết #22965', '2020-02-20 11:44:56', NULL);
INSERT INTO `article_logs` VALUES (45, 18, 22966, '[btv_cafef] đã khởi tạo bài viết #22966', '2020-02-26 15:35:50', NULL);
INSERT INTO `article_logs` VALUES (46, 18, 22966, '[btv_cafef] đã gửi duyệt bài viết #22966', '2020-02-26 15:50:56', NULL);
INSERT INTO `article_logs` VALUES (47, 19, 22966, '[duyet_cafef] Duyệt bài viết #22966', '2020-02-26 15:58:21', NULL);
INSERT INTO `article_logs` VALUES (48, 20, 22966, '[dang_cafef] Đăng bài viết #22966', '2020-02-26 16:03:47', NULL);
INSERT INTO `article_logs` VALUES (49, 18, 22967, '[btv_cafef] đã khởi tạo bài viết #22967', '2020-02-26 16:04:25', NULL);
INSERT INTO `article_logs` VALUES (50, 18, 22967, '[btv_cafef] đã gửi duyệt bài viết #22967', '2020-02-26 16:04:55', NULL);
INSERT INTO `article_logs` VALUES (51, 19, 22967, '[duyet_cafef] Duyệt bài viết #22967', '2020-02-26 16:05:18', NULL);
INSERT INTO `article_logs` VALUES (52, 20, 22967, '[dang_cafef] Đăng bài viết #22967', '2020-02-26 16:06:18', NULL);
INSERT INTO `article_logs` VALUES (53, 18, 22968, '[btv_cafef] đã khởi tạo bài viết #22968', '2020-02-26 16:18:13', NULL);
INSERT INTO `article_logs` VALUES (54, 18, 22968, '[btv_cafef] đã hủy bài viết #22968', '2020-02-26 16:18:40', NULL);
INSERT INTO `article_logs` VALUES (55, 22, 22969, '[btv_tinhte] đã khởi tạo bài viết #22969', '2020-02-27 14:51:07', NULL);
INSERT INTO `article_logs` VALUES (56, 22, 22969, '[btv_tinhte] đã gửi duyệt bài viết #22969', '2020-02-27 14:51:59', NULL);
INSERT INTO `article_logs` VALUES (57, 23, 22969, '[duyet_tinhte] Duyệt bài viết #22969', '2020-02-27 14:52:20', NULL);
INSERT INTO `article_logs` VALUES (58, 23, 22969, '[duyet_tinhte] Duyệt bài viết #22969', '2020-02-27 14:52:51', NULL);
INSERT INTO `article_logs` VALUES (59, 23, 22969, '[duyet_tinhte] Duyệt bài viết #22969', '2020-02-27 14:53:35', NULL);
INSERT INTO `article_logs` VALUES (60, 24, 22969, '[dang_tinhte] Đăng bài viết #22969', '2020-02-27 14:54:03', NULL);
INSERT INTO `article_logs` VALUES (61, 18, 22970, '[btv_cafef] đã khởi tạo bài viết #22970', '2020-02-27 14:58:46', NULL);
INSERT INTO `article_logs` VALUES (62, 18, 22970, '[btv_cafef] đã gửi duyệt bài viết #22970', '2020-02-27 14:58:59', NULL);
INSERT INTO `article_logs` VALUES (63, 18, 22970, '[btv_cafef] Duyệt bài viết #22970', '2020-02-27 14:59:13', NULL);
INSERT INTO `article_logs` VALUES (64, 20, 22970, '[dang_cafef] Đăng bài viết #22970', '2020-02-27 14:59:25', NULL);
INSERT INTO `article_logs` VALUES (65, 1, 2, '[luonghx] đã hủy bài viết #2', '2020-02-27 15:13:06', NULL);
INSERT INTO `article_logs` VALUES (66, 18, 22971, '[btv_cafef] đã khởi tạo bài viết #22971', '2020-03-02 17:51:30', NULL);
INSERT INTO `article_logs` VALUES (67, 18, 22971, '[btv_cafef] đã gửi duyệt bài viết #22971', '2020-03-02 17:51:48', NULL);
INSERT INTO `article_logs` VALUES (68, 19, 22971, '[duyet_cafef] Duyệt bài viết #22971', '2020-03-02 18:04:24', NULL);
INSERT INTO `article_logs` VALUES (69, 18, 22972, '[btv_cafef] đã khởi tạo bài viết #22972', '2020-03-03 14:49:59', NULL);
INSERT INTO `article_logs` VALUES (70, 18, 22972, '[btv_cafef] đã gửi duyệt bài viết #22972', '2020-03-03 14:50:31', NULL);
INSERT INTO `article_logs` VALUES (71, 19, 22972, '[duyet_cafef] đã không duyệt bài viết <b>Coronavirus có thể gây ra một cuộc suy thoái toàn cầu lớn hơn nhiều so với Đại khủng hoảng 2008 và đây là cách để ứng phó</b>', '2020-03-03 15:16:18', NULL);
INSERT INTO `article_logs` VALUES (72, 18, 22972, '[btv_cafef] đã gửi duyệt bài viết #22972', '2020-03-03 15:22:35', NULL);
INSERT INTO `article_logs` VALUES (73, 19, 22972, '[duyet_cafef] đã không duyệt bài viết <span style=\"color: #0000FF;\">Coronavirus có thể gây ra một cuộc suy thoái toàn cầu lớn hơn nhiều so với Đại khủng hoảng 2008 và đây là cách để ứng phó</span>. lý do: <span style=\"color: #0000FF;\">Làm lại</span>', '2020-03-03 15:23:06', NULL);
INSERT INTO `article_logs` VALUES (74, 18, 22972, '[btv_cafef] đã gửi duyệt bài viết #22972', '2020-03-03 15:25:31', NULL);
INSERT INTO `article_logs` VALUES (75, 19, 22972, '[duyet_cafef] đã không duyệt bài viết <span style=\"color: #0000FF;\">Coronavirus có thể gây ra...</span>. lý do: <span style=\"color: #0000FF;\">tiếp tục làm lại</span>', '2020-03-03 15:25:53', NULL);
INSERT INTO `article_logs` VALUES (76, 18, 22972, '[btv_cafef] đã gửi duyệt bài viết #22972', '2020-03-03 15:48:02', NULL);
INSERT INTO `article_logs` VALUES (77, 19, 22972, '[duyet_cafef] Duyệt bài viết \"<span style=\"color: #0000FF;\">Coronavirus có thể gây ra...</span>\"', '2020-03-03 15:54:56', NULL);
INSERT INTO `article_logs` VALUES (78, 20, 22971, '[dang_cafef] đã không đăng bài viết \"<span style=\"color: #0000FF;\">Thấy gì từ quỹ đất...</span>\". lý do: <span style=\"color: #0000FF;\">quá thời gian đăng</span>', '2020-03-03 15:56:58', NULL);
INSERT INTO `article_logs` VALUES (79, 20, 22972, '[dang_cafef] đã không đăng bài viết \"<span style=\"color: #0000FF;\">Coronavirus có thể gây ra...</span>\". lý do: <span style=\"color: #0000FF;\">Hết khung giờ</span>', '2020-03-03 15:57:13', NULL);
INSERT INTO `article_logs` VALUES (80, 2, 22973, '[nam1] đã khởi tạo bài viết #22973', '2020-03-05 17:02:10', NULL);
INSERT INTO `article_logs` VALUES (81, 2, 22973, '[nam1] đã gửi duyệt bài viết #22973', '2020-03-05 17:02:23', NULL);
INSERT INTO `article_logs` VALUES (82, 19, 22973, '[duyet_cafef] Duyệt bài viết \"<span style=\"color: #0000FF;\">Thấy gì từ quỹ đất...</span>\"', '2020-03-05 17:02:48', NULL);
INSERT INTO `article_logs` VALUES (83, 20, 22973, '[dang_cafef] Đăng bài viết <b>Thấy gì từ quỹ đất gần 5.000ha của tập đoàn Novaland?</b>', '2020-03-05 17:03:01', NULL);
INSERT INTO `article_logs` VALUES (84, 22, 22974, '[btv_tinhte] đã khởi tạo bài viết #22974', '2020-03-20 11:11:59', NULL);
INSERT INTO `article_logs` VALUES (85, 22, 22974, '[btv_tinhte] đã gửi duyệt bài viết #22974', '2020-03-20 11:12:49', NULL);
INSERT INTO `article_logs` VALUES (86, 23, 22974, '[duyet_tinhte] Duyệt bài viết \"<span style=\"color: #0000FF;\">Ryzen Mobile 4000 có những...</span>\"', '2020-03-20 11:13:39', NULL);
INSERT INTO `article_logs` VALUES (87, 24, 22974, '[dang_tinhte] Đăng bài viết <b>Ryzen Mobile 4000 có những cải tiến gì khiến AMD tự tin đối đầu Intel cả về hiệu năng lẫn pin?</b>', '2020-03-20 11:14:39', NULL);

-- ----------------------------
-- Table structure for articles
-- ----------------------------
DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles`  (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `website_id` int(11) NULL DEFAULT NULL,
  `category_id` int(11) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `sapo` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `publish_date` date NULL DEFAULT NULL,
  `publish_time` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `modified_on` datetime(0) NULL DEFAULT NULL,
  `channel_article_id` int(11) NULL DEFAULT NULL,
  `customer_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '-1: Bài đã hủy, 0: chưa gửi duyệt, 1 : gửi duyệt, 2 : đã duyệt, 3: gửi đăng, 4: đã đăng, -2: từ chối duyệt, -4: từ chối đăng',
  `layout` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `approved_by_id` int(10) NULL DEFAULT NULL,
  `published_by_id` int(10) NULL DEFAULT NULL,
  `approved_by_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `published_by_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `position_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22975 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of articles
-- ----------------------------
INSERT INTO `articles` VALUES (2, 1, 2, 'Thấy gì từ quỹ đất gần 5.000ha của tập đoàn Novaland?', 'ẻwerewrew', NULL, '2020-02-04', '15:00', '2020-02-04 16:59:22', '2020-02-27 15:13:06', 1994, 0, 1, '-1', 'http://prlayout.cnnd.vn/previews/komagroup-ngay-lap-tuc-bat-tay-03-doi-tac-chien-luoc-trong-ngay-ra-mat-thi-truong-viet-nam-20200204151200388.chn', 'http://kenh14.vn/bi-quyet-hot-li-xi-bac-trieu-tet-canh-ty-tu-jack-mouse-20200122130950453.chn', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `articles` VALUES (3, 1, 2, NULL, NULL, NULL, '2020-02-04', '18:00', '2020-02-04 17:01:25', '2020-02-06 15:21:18', 11956, 0, 1, '-1', NULL, 'http://kenh14.vn/bi-quyet-hot-li-xi-bac-trieu-tet-canh-ty-tu-jack-mouse-20200122130950453.chn', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `articles` VALUES (4, 1, 2, 'Cập nhật virus corona: Số ca nhiễm bệnh tăng lên hơn 3000, 636 người tử vong nhưng có hơn 1500 người đã được chữa khỏi\r\n', 'Đây là một tín hiệu đáng mừng khi số ca nhiễm bệnh tăng ít hơn so với thống kê cùng thời điểm ngày hôm qua.\r\n', 'https://kenh14cdn.com/thumb_w/620/2020/2/7/984e9c5c-4891-11ea-befc-ef9687daaa85imagehires200934-15810398100792023872564.jpg', '2020-02-11', '08:30', '2020-02-05 10:31:30', '2020-02-13 16:22:33', 11956, 0, 1, '4', NULL, 'http://kenh14.vn/bi-quyet-hot-li-xi-bac-trieu-tet-canh-ty-tu-jack-mouse-20200122130950453.chn', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `articles` VALUES (11956, 1, 4, NULL, NULL, NULL, '2020-02-20', '11:00', '2020-02-20 11:44:18', NULL, 11956, 0, 2, '0', NULL, 'http://kenh14.vn/bi-quyet-hot-li-xi-bac-trieu-tet-canh-ty-tu-jack-mouse-20200122130950453.chn', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `articles` VALUES (22963, 1, 2, 'Bí quyết hốt lì xì bạc triệu tết Canh Tý từ Jack Mouse\r\n', 'Dù ai nói ngả nói nghiêng, Tết được lì xì vẫn thích đúng không cả nhà! Muốn Tết này bội thu thì học ngay bí kíp hốt bạc lì xì từ Jack Mouse nhé!', 'http://channel.mediacdn.vn/2020/1/22/photo-1-1579666669104682914776.jpg', '2020-02-04', '12:00', '2020-02-04 16:58:55', '2020-02-06 16:11:43', 11956, 1, 1, '4', 'http://prlayout.cnnd.vn/previews/komagroup-ngay-lap-tuc-bat-tay-03-doi-tac-chien-luoc-trong-ngay-ra-mat-thi-truong-viet-nam-20200204151200388.chn', 'http://kenh14.vn/bi-quyet-hot-li-xi-bac-trieu-tet-canh-ty-tu-jack-mouse-20200122130950453.chn', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `articles` VALUES (22964, 1, 3, 'Thủ môn Văn Biểu khoe người yêu xinh miễn bàn, còn từng lọt vào chung kết Miss World Vietnam 2019\r\n', 'Dù ai nói ngả nói nghiêng, Tết được lì xì vẫn thích đúng không cả nhà! Muốn Tết này bội thu thì học ngay bí kíp hốt bạc lì xì từ Jack Mouse nhé!', 'http://channel.mediacdn.vn/2020/1/22/photo-1-1579666669104682914776.jpg', '2020-02-17', '12:12', '2020-02-17 16:06:28', '2020-02-17 16:24:19', 11956, 1, 15, '4', 'http://prlayout.cnnd.vn/previews/komagroup-ngay-lap-tuc-bat-tay-03-doi-tac-chien-luoc-trong-ngay-ra-mat-thi-truong-viet-nam-20200204151200388.chn', 'http://kenh14.vn/bi-quyet-hot-li-xi-bac-trieu-tet-canh-ty-tu-jack-mouse-20200122130950453.chn', 2, 2, 'nam1', 'nam1', NULL);
INSERT INTO `articles` VALUES (22965, 1, 3, 'Bí quyết hốt lì xì bạc triệu tết Canh Tý từ Jack Mouse\r\n', 'Đây là một tín hiệu đáng mừng khi số ca nhiễm bệnh tăng ít hơn so với thống kê cùng thời điểm ngày hôm qua.\r\n', 'http://channel.mediacdn.vn/2020/1/22/photo-1-1579666669104682914776.jpg', '2020-02-20', '11:00', '2020-02-20 11:21:14', '2020-02-20 11:44:56', 11956, 0, 15, '1', NULL, 'http://kenh14.vn/bi-quyet-hot-li-xi-bac-trieu-tet-canh-ty-tu-jack-mouse-20200122130950453.chn', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `articles` VALUES (22966, 17, 14, 'Điện toán đám mây đã được các doanh nghiệp hàng đầu Việt Nam ứng dụng thành công như thế nào?', 'Nếu năm 2019 là xu hướng toàn cầu áp dụng đám mây thì 2020 là thời điểm không thể thích hợp hơn để hiện thực hóa điều đó. Điện toán đám mây sẽ là cách nhanh nhất, tiết kiệm nhất để biến những ý tưởng \"tham vọng\" thành các ứng dụng tiềm năng.\r\n', NULL, '2020-02-26', '23:00', '2020-02-26 15:35:50', '2020-02-26 16:03:47', 2491, 0, 18, '4', 'http://prlayout.cnnd.vn/previews/komagroup-ngay-lap-tuc-bat-tay-03-doi-tac-chien-luoc-trong-ngay-ra-mat-thi-truong-viet-nam-20200204151200388.chn', 'http://kenh14.vn/bi-quyet-hot-li-xi-bac-trieu-tet-canh-ty-tu-jack-mouse-20200122130950453.chn', 19, 20, NULL, 'dang_cafef', 7);
INSERT INTO `articles` VALUES (22967, 17, 12, 'Thấy gì từ quỹ đất gần 5.000ha của tập đoàn Novaland?', 'Chỉ hơn một năm nay, quỹ đất mà tập đoàn Novaland đang sở hữu và nghiên cứu triển khai các dự án tăng gần gấp 2 lần từ khoảng 2.700ha vào 2018 lên khoảng 4.900ha tính đến 31/12/2019.\r\n', NULL, '2020-02-26', '16:00', '2020-02-26 16:04:25', '2020-02-26 16:06:18', 1643, 0, 18, '4', 'http://prlayout.cnnd.vn/previews/komagroup-ngay-lap-tuc-bat-tay-03-doi-tac-chien-luoc-trong-ngay-ra-mat-thi-truong-viet-nam-20200204151200388.chn', 'http://kenh14.vn/bi-quyet-hot-li-xi-bac-trieu-tet-canh-ty-tu-jack-mouse-20200122130950453.chn', 19, 20, NULL, 'dang_cafef', 8);
INSERT INTO `articles` VALUES (22968, 17, 14, 'Thấy gì từ quỹ đất gần 5.000ha của tập đoàn Novaland?', 'Chỉ hơn một năm nay, quỹ đất mà tập đoàn Novaland đang sở hữu và nghiên cứu triển khai các dự án tăng gần gấp 2 lần từ khoảng 2.700ha vào 2018 lên khoảng 4.900ha tính đến 31/12/2019.\r\n', NULL, '2020-02-26', '16:00', '2020-02-26 16:18:13', '2020-02-26 16:18:40', 3407, 0, 18, '-1', 'http://prlayout.cnnd.vn/previews/komagroup-ngay-lap-tuc-bat-tay-03-doi-tac-chien-luoc-trong-ngay-ra-mat-thi-truong-viet-nam-20200204151200388.chn', 'http://kenh14.vn/bi-quyet-hot-li-xi-bac-trieu-tet-canh-ty-tu-jack-mouse-20200122130950453.chn', NULL, NULL, NULL, NULL, 8);
INSERT INTO `articles` VALUES (22969, 18, 15, 'Giới thiệu tính năng: chụp macro trên Galaxy A51', 'Trong nhiếp ảnh thì chụp macro và chụp phơi đêm theo mình là hai hiệu ứng mang lại sự ấn tượng nhất, dễ dàng nhất.', NULL, '2020-02-27', '14:00', '2020-02-27 14:51:07', '2020-02-27 14:54:03', 8067, 3, 22, '4', 'http://prlayout.cnnd.vn/previews/komagroup-ngay-lap-tuc-bat-tay-03-doi-tac-chien-luoc-trong-ngay-ra-mat-thi-truong-viet-nam-20200204151200388.chn', 'http://kenh14.vn/bi-quyet-hot-li-xi-bac-trieu-tet-canh-ty-tu-jack-mouse-20200122130950453.chn', 23, 24, 'duyet_tinhte', 'dang_tinhte', NULL);
INSERT INTO `articles` VALUES (22970, 17, 12, 'Thấy gì từ quỹ đất gần 5.000ha của tập đoàn Novaland?', 'àdassadsadsad', NULL, '2020-02-27', '14:00', '2020-02-27 14:58:46', '2020-02-27 14:59:25', 7280, 0, 18, '4', 'http://prlayout.cnnd.vn/previews/komagroup-ngay-lap-tuc-bat-tay-03-doi-tac-chien-luoc-trong-ngay-ra-mat-thi-truong-viet-nam-20200204151200388.chn', 'http://kenh14.vn/bi-quyet-hot-li-xi-bac-trieu-tet-canh-ty-tu-jack-mouse-20200122130950453.chn', 18, 20, 'btv_cafef', 'dang_cafef', 7);
INSERT INTO `articles` VALUES (22971, 17, 14, 'Thấy gì từ quỹ đất gần 5.000ha của tập đoàn Novaland?', 'grdsfdsfds', NULL, '2020-03-02', '17:00', '2020-03-02 17:51:30', '2020-03-03 15:56:58', 1128, 4, 18, '-4', 'http://prlayout.cnnd.vn/previews/komagroup-ngay-lap-tuc-bat-tay-03-doi-tac-chien-luoc-trong-ngay-ra-mat-thi-truong-viet-nam-20200204151200388.chn', 'http://kenh14.vn/bi-quyet-hot-li-xi-bac-trieu-tet-canh-ty-tu-jack-mouse-20200122130950453.chn', 20, NULL, 'dang_cafef', NULL, 7);
INSERT INTO `articles` VALUES (22972, 17, 14, 'Coronavirus có thể gây ra một cuộc suy thoái toàn cầu lớn hơn nhiều so với Đại khủng hoảng 2008 và đây là cách để ứng phó', 'Vẫn còn quá sớm để dự đoán các tác động dài hạn của coronavirus, nhưng không quá sớm để nhận ra rằng: cuộc suy thoái toàn cầu tiếp theo có thể xảy ra. Và điều quan trọng hơn là, nó có thể khác rất nhiều so với những lần khủng hoảng năm 2001 hay 2008.\r\n', NULL, '2020-03-03', '14:00', '2020-03-03 14:49:59', '2020-03-03 15:57:13', 6483, 4, 18, '-4', 'http://prlayout.cnnd.vn/previews/komagroup-ngay-lap-tuc-bat-tay-03-doi-tac-chien-luoc-trong-ngay-ra-mat-thi-truong-viet-nam-20200204151200388.chn', 'http://kenh14.vn/bi-quyet-hot-li-xi-bac-trieu-tet-canh-ty-tu-jack-mouse-20200122130950453.chn', 20, NULL, 'dang_cafef', NULL, 7);
INSERT INTO `articles` VALUES (22973, 17, 14, 'Thấy gì từ quỹ đất gần 5.000ha của tập đoàn Novaland?', 'rewrew', NULL, '2020-03-05', '17:00', '2020-03-05 17:02:10', '2020-03-05 17:03:01', 7431, 0, 2, '4', 'http://prlayout.cnnd.vn/previews/komagroup-ngay-lap-tuc-bat-tay-03-doi-tac-chien-luoc-trong-ngay-ra-mat-thi-truong-viet-nam-20200204151200388.chn', NULL, 19, 20, 'duyet_cafef', 'dang_cafef', NULL);
INSERT INTO `articles` VALUES (22974, 18, 16, 'Ryzen Mobile 4000 có những cải tiến gì khiến AMD tự tin đối đầu Intel cả về hiệu năng lẫn pin?', 'AMD đã tung ra dòng Ryzen Mobile 4000 series với những con chip có đến 8 nhân cả dòng H hiệu năng cao lẫn U tiết kiệm điện.  \r\n', NULL, '2020-03-20', '11:00', '2020-03-20 11:11:59', '2020-03-20 11:14:39', 7892, 3, 22, '4', 'http://prlayout.cnnd.vn/previews/komagroup-ngay-lap-tuc-bat-tay-03-doi-tac-chien-luoc-trong-ngay-ra-mat-thi-truong-viet-nam-20200204151200388.chn', NULL, 23, 24, 'duyet_tinhte', 'dang_tinhte', NULL);

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `modified_on` datetime(0) NULL DEFAULT NULL,
  `website_id` int(11) NULL DEFAULT NULL,
  `desc` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, 'Đời sống', NULL, NULL, 1, 'Cập nhật thông tin nhanh nhất về các trào lưu, mối quan tâm, chuyển động, đời sống giới trẻ cùng tin tức xã hội nóng hổi trong dư luận và cộng đồng mạng.');
INSERT INTO `categories` VALUES (2, 'Xã hội', NULL, NULL, 1, 'Cập nhật nhanh các vấn đề thời sự, pháp luật, xã hội, thời tiết...nổi bật được dư luận quan tâm cùng cái nhìn toàn cảnh về các chuyển động xã hội Việt Nam.');
INSERT INTO `categories` VALUES (3, 'Thời sự', NULL, NULL, 1, NULL);
INSERT INTO `categories` VALUES (4, 'Fashion', NULL, NULL, 1, 'Cập nhật xu hướng làm đẹp và thời trang nóng hổi nhất, đọc vị style của người nổi tiếng, tất cả những gì &quot;hot hit&quot; nhất về làm đẹp và thời trang mà bạn cần biết.');
INSERT INTO `categories` VALUES (5, '34324', '2020-02-21 11:24:27', NULL, 1, '32432432');
INSERT INTO `categories` VALUES (6, 'ẻe', '2020-02-23 23:47:19', NULL, 13, 'tretre');
INSERT INTO `categories` VALUES (7, 'Xã hội', '2020-02-23 23:47:32', NULL, 13, 'đfdsfdsfds');
INSERT INTO `categories` VALUES (8, 'Xã hội', '2020-02-24 00:09:17', NULL, 15, '');
INSERT INTO `categories` VALUES (9, 'Xã hội', '2020-02-24 00:47:28', NULL, 16, '');
INSERT INTO `categories` VALUES (10, 'Đời sống', '2020-02-24 00:47:34', NULL, 16, '');
INSERT INTO `categories` VALUES (11, 'Pháp luật', '2020-02-24 00:47:40', NULL, 16, '');
INSERT INTO `categories` VALUES (12, 'Tài chính - Ngân hàng', '2020-02-26 15:15:16', NULL, 17, '');
INSERT INTO `categories` VALUES (13, 'Chứng khoán', '2020-02-26 15:15:26', NULL, 17, '');
INSERT INTO `categories` VALUES (14, 'Bất động sản', '2020-02-26 15:15:32', NULL, 17, '');
INSERT INTO `categories` VALUES (15, 'Điện thoại', '2020-02-27 14:42:17', NULL, 18, '');
INSERT INTO `categories` VALUES (16, 'Máy tính bảng', '2020-02-27 14:42:24', NULL, 18, '');
INSERT INTO `categories` VALUES (17, 'Tin tức', '2020-02-27 14:42:30', NULL, 18, '');

-- ----------------------------
-- Table structure for category_roles
-- ----------------------------
DROP TABLE IF EXISTS `category_roles`;
CREATE TABLE `category_roles`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NULL DEFAULT NULL,
  `website_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `modified_on` datetime(0) NULL DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 61 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of category_roles
-- ----------------------------
INSERT INTO `category_roles` VALUES (5, 4, 1, 5, '2020-02-11 17:26:42', NULL, 'approve');
INSERT INTO `category_roles` VALUES (7, 2, 1, 5, '2020-02-11 17:38:51', NULL, 'approve');
INSERT INTO `category_roles` VALUES (8, NULL, 1, 0, '2020-02-12 15:56:13', NULL, 'approve');
INSERT INTO `category_roles` VALUES (16, 4, 1, 4, '2020-02-12 16:23:42', NULL, 'approve');
INSERT INTO `category_roles` VALUES (19, 2, 1, 4, '2020-02-12 16:24:53', NULL, 'publish');
INSERT INTO `category_roles` VALUES (20, 4, 1, 4, '2020-02-12 16:29:58', NULL, 'publish');
INSERT INTO `category_roles` VALUES (21, 1, 1, 4, '2020-02-12 16:30:03', NULL, 'approve');
INSERT INTO `category_roles` VALUES (22, 1, 1, 4, '2020-02-12 16:30:07', NULL, 'publish');
INSERT INTO `category_roles` VALUES (23, 3, 1, 4, '2020-02-12 16:30:10', NULL, 'publish');
INSERT INTO `category_roles` VALUES (24, 4, 1, 2, '2020-02-12 16:50:40', NULL, 'approve');
INSERT INTO `category_roles` VALUES (25, 3, 1, 2, '2020-02-12 16:50:42', NULL, 'publish');
INSERT INTO `category_roles` VALUES (26, 4, 1, 6, '2020-02-13 15:27:06', NULL, 'approve');
INSERT INTO `category_roles` VALUES (27, 2, 1, 6, '2020-02-13 15:27:07', NULL, 'approve');
INSERT INTO `category_roles` VALUES (28, 3, 1, 6, '2020-02-13 15:27:09', NULL, 'publish');
INSERT INTO `category_roles` VALUES (29, 1, 1, 6, '2020-02-13 15:27:10', NULL, 'publish');
INSERT INTO `category_roles` VALUES (30, 2, 1, 2, '2020-02-17 15:58:00', NULL, 'approve');
INSERT INTO `category_roles` VALUES (31, 3, 1, 2, '2020-02-17 16:08:56', NULL, 'approve');
INSERT INTO `category_roles` VALUES (32, 1, 1, 2, '2020-02-17 16:08:57', NULL, 'approve');
INSERT INTO `category_roles` VALUES (33, 4, 1, 2, '2020-02-17 16:16:20', NULL, 'publish');
INSERT INTO `category_roles` VALUES (34, 2, 1, 2, '2020-02-17 16:16:21', NULL, 'publish');
INSERT INTO `category_roles` VALUES (35, 1, 1, 2, '2020-02-17 16:16:22', NULL, 'publish');
INSERT INTO `category_roles` VALUES (36, 8, 15, 2, '2020-02-24 00:10:13', NULL, 'approve');
INSERT INTO `category_roles` VALUES (37, 11, 16, 17, '2020-02-24 11:46:04', NULL, 'approve');
INSERT INTO `category_roles` VALUES (38, 8, 15, 18, '2020-02-26 15:16:27', NULL, 'approve');
INSERT INTO `category_roles` VALUES (39, 14, 17, 18, '2020-02-26 15:16:29', NULL, 'publish');
INSERT INTO `category_roles` VALUES (40, 13, 17, 18, '2020-02-26 15:16:30', NULL, 'publish');
INSERT INTO `category_roles` VALUES (41, 12, 17, 18, '2020-02-26 15:16:31', NULL, 'publish');
INSERT INTO `category_roles` VALUES (43, 13, 17, 18, '2020-02-26 15:16:33', NULL, 'approve');
INSERT INTO `category_roles` VALUES (44, 12, 17, 18, '2020-02-26 15:16:34', NULL, 'approve');
INSERT INTO `category_roles` VALUES (45, 14, 17, 19, '2020-02-26 15:58:08', NULL, 'approve');
INSERT INTO `category_roles` VALUES (46, 13, 17, 19, '2020-02-26 15:58:09', NULL, 'approve');
INSERT INTO `category_roles` VALUES (47, 12, 17, 19, '2020-02-26 15:58:10', NULL, 'approve');
INSERT INTO `category_roles` VALUES (48, 14, 17, 20, '2020-02-26 15:59:44', NULL, 'publish');
INSERT INTO `category_roles` VALUES (49, 13, 17, 20, '2020-02-26 15:59:45', NULL, 'publish');
INSERT INTO `category_roles` VALUES (50, 12, 17, 20, '2020-02-26 15:59:46', NULL, 'publish');
INSERT INTO `category_roles` VALUES (51, 17, 18, 23, '2020-02-27 14:44:41', NULL, 'approve');
INSERT INTO `category_roles` VALUES (52, 16, 18, 23, '2020-02-27 14:44:41', NULL, 'approve');
INSERT INTO `category_roles` VALUES (53, 15, 18, 23, '2020-02-27 14:44:43', NULL, 'approve');
INSERT INTO `category_roles` VALUES (54, 17, 18, 24, '2020-02-27 14:45:10', NULL, 'publish');
INSERT INTO `category_roles` VALUES (55, 16, 18, 24, '2020-02-27 14:45:11', NULL, 'publish');
INSERT INTO `category_roles` VALUES (56, 15, 18, 24, '2020-02-27 14:45:12', NULL, 'publish');
INSERT INTO `category_roles` VALUES (57, 15, 18, 22, '2020-03-20 11:15:17', NULL, 'approve');
INSERT INTO `category_roles` VALUES (58, 16, 18, 22, '2020-03-20 11:15:18', NULL, 'approve');
INSERT INTO `category_roles` VALUES (59, 17, 18, 22, '2020-03-20 11:15:19', NULL, 'approve');
INSERT INTO `category_roles` VALUES (60, 17, 18, 22, '2020-03-20 11:15:37', NULL, 'publish');

-- ----------------------------
-- Table structure for customers
-- ----------------------------
DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `website_id` int(11) NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `modified_on` datetime(0) NULL DEFAULT NULL,
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of customers
-- ----------------------------
INSERT INTO `customers` VALUES (1, 'Ngân Hàng VPBank', 1, '2020-02-20 10:54:21', NULL, 'Là một trong những ngân hàng TMCP thành lập sớm nhất tại Việt Nam, VPBank đã có những bước phát triển vững chắc trong suốt lịch sử của ngân hàng. VPBank đã đặt mục tiêu chiến lược trong giai đoạn 2018-2022 với tham vọng trở thành Ngân hàng thân thiện nhất');
INSERT INTO `customers` VALUES (2, 'BIDV', 18, '2020-02-26 17:27:22', NULL, '');
INSERT INTO `customers` VALUES (3, 'ACER', 18, '2020-02-27 14:46:25', NULL, 'Tập đoàn Acer');
INSERT INTO `customers` VALUES (4, 'Unilever', 17, '2020-02-27 15:00:01', NULL, '');

-- ----------------------------
-- Table structure for departments
-- ----------------------------
DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments`  (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `modified_on` datetime(0) NULL DEFAULT NULL,
  `website_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for positions
-- ----------------------------
DROP TABLE IF EXISTS `positions`;
CREATE TABLE `positions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `price` bigint(10) NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `modified_on` datetime(0) NULL DEFAULT NULL,
  `website_id` int(11) NULL DEFAULT NULL,
  `desc` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of positions
-- ----------------------------
INSERT INTO `positions` VALUES (1, 'Đặc biệt homeweb', 30000000, NULL, NULL, 1, NULL);
INSERT INTO `positions` VALUES (2, 'Đặc biệt homemobile', 30000000, NULL, NULL, 1, NULL);
INSERT INTO `positions` VALUES (3, 'Đặc biệt homemix', 40000000, NULL, NULL, 1, NULL);
INSERT INTO `positions` VALUES (4, 'test', 1000000, '2020-02-24 00:56:27', NULL, 16, '');
INSERT INTO `positions` VALUES (5, 'rưerew', 30000000, '2020-02-24 00:57:05', NULL, 16, '');
INSERT INTO `positions` VALUES (6, '4545', 20000000, '2020-02-24 11:18:02', NULL, 1, '');
INSERT INTO `positions` VALUES (7, 'Bài loại 1', 35000000, '2020-02-26 15:35:01', NULL, 17, '');
INSERT INTO `positions` VALUES (8, 'Bài loại 2', 30000000, '2020-02-26 15:35:15', NULL, 17, '');
INSERT INTO `positions` VALUES (9, 'Bài loại 1', 50000000, '2020-02-27 14:45:40', NULL, 18, '');
INSERT INTO `positions` VALUES (10, 'Bài loại 2', 40000000, '2020-02-27 14:45:51', NULL, 18, '');

-- ----------------------------
-- Table structure for user_roles
-- ----------------------------
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `website_id` int(11) NULL DEFAULT NULL,
  `category_id` int(11) NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `modified_on` datetime(0) NULL DEFAULT NULL,
  `approve_rule` int(1) NULL DEFAULT NULL COMMENT '1 : Có quyền duyệt bài',
  `publish_rule` int(1) NULL DEFAULT NULL COMMENT '1 : Có quyền đăng bài',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user_roles
-- ----------------------------
INSERT INTO `user_roles` VALUES (1, 1, 1, 1, NULL, NULL, 1, 1);
INSERT INTO `user_roles` VALUES (2, 1, 1, 2, NULL, NULL, 1, 1);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `modified_on` datetime(0) NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT 1,
  `role` int(255) NULL DEFAULT NULL COMMENT '-1 : Root, 1 : Admin site, 2 : BTV',
  `created_by` int(11) NULL DEFAULT NULL,
  `fullname` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `birthday` date NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `email` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `position` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `agency_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'luonghx', 'c4ca4238a0b923820dcc509a6f75849b', NULL, NULL, 1, -1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `users` VALUES (2, 'nam1', 'c4ca4238a0b923820dcc509a6f75849b', '2020-02-11 15:30:26', NULL, 1, 1, 1, 'Hoang Luong', NULL, '311 T2 - Golden An Khanh Building, Hoai Duc district\r\n311 T2 - Golden An Khanh Building', 'luonghx@gmail.com', 'Programmer', NULL, NULL);
INSERT INTO `users` VALUES (3, 'nam2', 'a91dcedc41a3afb372406ec7e94f6aa7', '2020-02-11 15:32:23', NULL, 1, 2, 1, 'Hoang Luong', NULL, '311 T2 - Golden An Khanh Building, Hoai Duc district\r\n311 T2 - Golden An Khanh Building', 'luonghx@gmail.com', 'Programmer', NULL, NULL);
INSERT INTO `users` VALUES (4, 'nam3', '83e64ae881554d490f12f9280a9f98a8', '2020-02-11 15:32:57', NULL, 1, 2, 1, '432432432', NULL, '311 T2 - Golden An Khanh Building, Hoai Duc district\r\n311 T2 - Golden An Khanh Building', 'luonghx@gmail.com', 'Programmer', NULL, NULL);
INSERT INTO `users` VALUES (5, 'nam4', '83e64ae881554d490f12f9280a9f98a8', '2020-02-11 15:33:53', NULL, 1, 2, 1, '432432432', NULL, '311 T2 - Golden An Khanh Building, Hoai Duc district\r\n311 T2 - Golden An Khanh Building', 'luonghx@gmail.com', 'Programmer', NULL, NULL);
INSERT INTO `users` VALUES (14, 'rưerew', 'ab024e26556f0428d4fea4b25662038b', '2020-02-13 16:15:00', '2020-02-26 17:07:37', 1, 1, 1, '', NULL, '', '', '', NULL, NULL);
INSERT INTO `users` VALUES (15, 'Bientapvien_k14', 'c4ca4238a0b923820dcc509a6f75849b', '2020-02-17 16:01:38', NULL, 1, 2, 2, 'BTV K14', NULL, 'êfewfewe', '21321321@sfdsfds.com', 'BTV', NULL, NULL);
INSERT INTO `users` VALUES (16, 'hgfhgfhgf', 'e615c82aba461681ade82da2da38004a', '2020-02-20 11:40:47', NULL, 1, 2, 2, 'hgfhgfhgfh', NULL, 'gfdgfgfd', 'luonghx@gmail.com', '', NULL, NULL);
INSERT INTO `users` VALUES (17, 'btv_vnn', 'c4ca4238a0b923820dcc509a6f75849b', '2020-02-24 00:47:07', '2020-02-24 16:17:19', 1, 2, 2, 'BTV VNN', NULL, 'Ha Noi, 3, 3, 3, 3\r\n3', 'luonghx@gmail.com', 'BTV', NULL, NULL);
INSERT INTO `users` VALUES (18, 'btv_cafef', 'c4ca4238a0b923820dcc509a6f75849b', '2020-02-26 15:16:20', NULL, 1, 2, 2, 'Xuan Luong Hoang', NULL, 'Ha Noi, 3, 3, 3, 3\r\n3', 'luonghx@gmail.com', 'BTV', NULL, NULL);
INSERT INTO `users` VALUES (19, 'duyet_cafef', 'c4ca4238a0b923820dcc509a6f75849b', '2020-02-26 15:57:27', NULL, 1, 2, 2, 'Xuan Luong Hoang', NULL, 'Ha Noi, 3, 3, 3, 3\r\n3', 'luonghx@gmail.com', '', NULL, NULL);
INSERT INTO `users` VALUES (20, 'dang_cafef', 'c4ca4238a0b923820dcc509a6f75849b', '2020-02-26 15:59:40', NULL, 1, 2, 2, 'Xuan Luong Hoang', NULL, 'Ha Noi, 3, 3, 3, 3\r\n3', 'luonghx@gmail.com', 'BTV', NULL, NULL);
INSERT INTO `users` VALUES (21, 'qly_tinhte', 'c4ca4238a0b923820dcc509a6f75849b', '2020-02-27 14:15:01', NULL, 1, 1, 1, 'Xuan Luong Hoang', NULL, 'Ha Noi, 3, 3, 3, 3\r\n3', 'luonghx@gmail.com', 'BTV', NULL, NULL);
INSERT INTO `users` VALUES (22, 'btv_tinhte', 'c4ca4238a0b923820dcc509a6f75849b', '2020-02-27 14:44:05', NULL, 1, 2, 21, 'Xuan Luong Hoang', NULL, 'Ha Noi, 3, 3, 3, 3\r\n3', 'luonghx@gmail.com', 'BTV', NULL, NULL);
INSERT INTO `users` VALUES (23, 'duyet_tinhte', 'c4ca4238a0b923820dcc509a6f75849b', '2020-02-27 14:44:36', NULL, 1, 2, 21, 'Xuan Luong Hoang', NULL, 'Ha Noi, 3, 3, 3, 3\r\n3', 'luonghx@gmail.com', 'BTV', NULL, NULL);
INSERT INTO `users` VALUES (24, 'dang_tinhte', 'c4ca4238a0b923820dcc509a6f75849b', '2020-02-27 14:45:06', NULL, 1, 2, 21, 'Xuan Luong Hoang', NULL, 'Ha Noi, 3, 3, 3, 3\r\n3', 'luonghx@gmail.com', 'BTV', NULL, NULL);

-- ----------------------------
-- Table structure for website_roles
-- ----------------------------
DROP TABLE IF EXISTS `website_roles`;
CREATE TABLE `website_roles`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `website_id` int(11) NULL DEFAULT NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `modified_on` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 39 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of website_roles
-- ----------------------------
INSERT INTO `website_roles` VALUES (1, 1, 1, NULL, NULL);
INSERT INTO `website_roles` VALUES (2, 1, 2, NULL, NULL);
INSERT INTO `website_roles` VALUES (8, 2, 1, '2020-02-17 15:57:51', NULL);
INSERT INTO `website_roles` VALUES (9, 15, 1, '2020-02-17 16:01:38', NULL);
INSERT INTO `website_roles` VALUES (10, 2, 2, '2020-02-17 17:05:45', NULL);
INSERT INTO `website_roles` VALUES (11, 16, 1, '2020-02-20 11:40:47', NULL);
INSERT INTO `website_roles` VALUES (12, 16, 2, '2020-02-20 11:40:47', NULL);
INSERT INTO `website_roles` VALUES (13, 2, 15, '2020-02-24 00:05:56', NULL);
INSERT INTO `website_roles` VALUES (14, 2, 16, '2020-02-24 00:45:43', NULL);
INSERT INTO `website_roles` VALUES (16, 17, 1, '2020-02-24 16:17:19', NULL);
INSERT INTO `website_roles` VALUES (17, 17, 2, '2020-02-24 16:17:19', NULL);
INSERT INTO `website_roles` VALUES (18, 2, 17, '2020-02-26 15:15:00', NULL);
INSERT INTO `website_roles` VALUES (19, 18, 15, '2020-02-26 15:16:20', NULL);
INSERT INTO `website_roles` VALUES (20, 18, 17, '2020-02-26 15:16:20', NULL);
INSERT INTO `website_roles` VALUES (21, 19, 17, '2020-02-26 15:57:27', NULL);
INSERT INTO `website_roles` VALUES (22, 20, 17, '2020-02-26 15:59:40', NULL);
INSERT INTO `website_roles` VALUES (31, 14, 1, '2020-02-26 17:07:37', NULL);
INSERT INTO `website_roles` VALUES (32, 14, 2, '2020-02-26 17:07:37', NULL);
INSERT INTO `website_roles` VALUES (33, 14, 15, '2020-02-26 17:07:37', NULL);
INSERT INTO `website_roles` VALUES (34, 14, 17, '2020-02-26 17:07:37', NULL);
INSERT INTO `website_roles` VALUES (35, 21, 18, '2020-02-27 14:41:55', NULL);
INSERT INTO `website_roles` VALUES (36, 22, 18, '2020-02-27 14:44:05', NULL);
INSERT INTO `website_roles` VALUES (37, 23, 18, '2020-02-27 14:44:36', NULL);
INSERT INTO `website_roles` VALUES (38, 24, 18, '2020-02-27 14:45:06', NULL);

-- ----------------------------
-- Table structure for websites
-- ----------------------------
DROP TABLE IF EXISTS `websites`;
CREATE TABLE `websites`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `domain` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `desc` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `created_on` datetime(0) NULL DEFAULT NULL,
  `modified_on` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of websites
-- ----------------------------
INSERT INTO `websites` VALUES (1, 'Kenh14', 'Kenh14.vn', NULL, NULL, NULL);
INSERT INTO `websites` VALUES (2, 'Afamily', 'Afamily.vn', NULL, NULL, NULL);
INSERT INTO `websites` VALUES (14, 'soha', 'soha.vn', '', '2020-02-24 00:05:28', NULL);
INSERT INTO `websites` VALUES (15, 'soha', 'soha.vn', '', '2020-02-24 00:05:56', NULL);
INSERT INTO `websites` VALUES (16, 'vietnamnet', 'vietnamnet.vn', '', '2020-02-24 00:45:43', NULL);
INSERT INTO `websites` VALUES (17, 'cafef', 'cafef.vn', '', '2020-02-26 15:15:00', NULL);
INSERT INTO `websites` VALUES (18, 'Tinh tế', 'tinhte.vn', '', '2020-02-27 14:41:55', NULL);

SET FOREIGN_KEY_CHECKS = 1;
