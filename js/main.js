$(document).ready(function () {

    /*
    * Action: Change website
    * */

    $('#headerChangeWebsite').change(function () {
        $.ajax({
            url: base_url + 'auth/ajxChangeDomain/' + $(this).val(),
            type: 'GET',
            success: function () {
                Swal.fire({
                    position: 'top-end',
                    title: 'Chuyển website',
                    text: "Chuyển website thành công",
                    icon: 'success',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'OK'
                }).then((result) => {

                    location.reload();

                })
            }
        })
    });

    /*
    * Save Article
    * */
    $("#formSaveArticle").submit(function (e) {
        e.preventDefault();
        var form = $(this);
        var url = form.attr('action');
        $('#errorAlert').hide();
        Swal.fire({
            icon: 'success',
            html: '<b>Hệ thống đang xử lý ...</b><br/>Vui lòng không đóng trình duyệt',
            showConfirmButton: false,
            allowOutsideClick: false
        });

        $.ajax({
            type: "POST",
            url: url,
            dataType: 'JSON',
            data: form.serialize(), // serializes the form's elements.
            success: function (response) {
                setTimeout(function () {
                    swal.close();
                    var dat = (response);
                    if (dat.status == -1) {
                        $('#errorAlert').show().html(dat.message);
                    } else {
                        window.location.href = dat.redirect;
                    }

                }, 1000);
            }
        });

    });

    /*
    * Get popup Article from list
    * */
    $('.article-list-item').click(function () {
        var articleId = $(this).attr('data-article-id');

        $.ajax({
            url: base_url + 'article/ajx_LoadArticleInfo',
            type: "get",
            dateType: "text",
            data: {
                article_id: articleId
            },
            success: function (result) {
                $('.loadArticleInfo').html(result);
                $('#myModal2').modal();
            }
        });
    });

    /* User: create user*/
    $("#userCreate").submit(function (e) {
        e.preventDefault();
        var form = $(this);
        var url = form.attr('action');
        $('#errorAlert').hide();
        Swal.fire({
            icon: 'success',
            html: '<b>Hệ thống đang xử lý ...</b><br/>Vui lòng không đóng trình duyệt',
            showConfirmButton: false,
            allowOutsideClick: false
        });

        $.ajax({
            type: "POST",
            url: url,
            dataType: 'JSON',
            data: form.serialize(), // serializes the form's elements.
            success: function (response) {
                setTimeout(function () {
                    swal.close();
                    var dat = (response);
                    if (dat.status == -1) {
                        $('#errorAlert').show().html(dat.message);
                    } else {
                        window.location.href = dat.redirect;
                    }

                }, 1000);
            }
        });

    });


    /*
    * Grand Rule
    * */

    $('.grandRuleApprove').change(function () {
        var el = $(this);
        var approver = el.attr('approve-id');
        var CatId = el.attr('id');
        var website = el.attr('website-id');
        var type = el.attr('type');
        var val = el.val();
        $.ajax({
            url: base_url + 'user/actionGrandApprove',
            type: 'POST',
            dataType: 'JSON',
            data: {approver: approver, catId: CatId, website: website, value: val, type: type},
            success: function () {

            }
        });
    });
    /*
    * Create Website
    * */
    $("#websiteCreate").submit(function (e) {
        e.preventDefault();
        var form = $(this);
        var url = form.attr('action');
        $('#errorAlert').hide();
        Swal.fire({
            icon: 'success',
            html: '<b>Hệ thống đang xử lý ...</b><br/>Vui lòng không đóng trình duyệt',
            showConfirmButton: false,
            allowOutsideClick: false
        });

        $.ajax({
            type: "POST",
            url: url,
            dataType: 'JSON',
            data: form.serialize(), // serializes the form's elements.
            success: function (response) {
                setTimeout(function () {
                    swal.close();
                    var dat = (response);
                    if (dat.status == -1) {
                        $('#errorAlert').show().html(dat.message);
                    } else {
                        window.location.href = dat.redirect;
                    }

                }, 1000);
            }
        });

    });

    /*
      * Create Website
      * */

    $("#categoryCreate").submit(function (e) {
        e.preventDefault();
        var form = $(this);
        var url = form.attr('action');
        $('#errorAlert').hide();
        Swal.fire({
            icon: 'success',
            html: '<b>Hệ thống đang xử lý ...</b><br/>Vui lòng không đóng trình duyệt',
            showConfirmButton: false,
            allowOutsideClick: false
        });

        $.ajax({
            type: "POST",
            url: url,
            dataType: 'JSON',
            data: form.serialize(), // serializes the form's elements.
            success: function (response) {
                setTimeout(function () {
                    swal.close();
                    var dat = (response);
                    if (dat.status == -1) {
                        $('#errorAlert').show().html(dat.message);
                    } else {
                        window.location.href = dat.redirect;
                    }

                }, 1000);
            }
        });

    });

    /*
     * Create position
     * */

    $("#positionCreate").submit(function (e) {
        e.preventDefault();
        var form = $(this);
        var url = form.attr('action');
        $('#errorAlert').hide();
        Swal.fire({
            icon: 'success',
            html: '<b>Hệ thống đang xử lý ...</b><br/>Vui lòng không đóng trình duyệt',
            showConfirmButton: false,
            allowOutsideClick: false
        });

        $.ajax({
            type: "POST",
            url: url,
            dataType: 'JSON',
            data: form.serialize(), // serializes the form's elements.
            success: function (response) {
                setTimeout(function () {
                    swal.close();
                    var dat = (response);
                    if (dat.status == -1) {
                        $('#errorAlert').show().html(dat.message);
                    } else {
                        window.location.href = dat.redirect;
                    }

                }, 1000);
            }
        });

    });

    /*
 * Create position
 * */

    $("#editorSave").submit(function (e) {
        e.preventDefault();
        var form = $(this);
        var url = form.attr('action');
        $('#errorAlert').hide();
        Swal.fire({
            icon: 'success',
            html: '<b>Hệ thống đang xử lý ...</b><br/>Vui lòng không đóng trình duyệt',
            showConfirmButton: false,
            allowOutsideClick: false
        });

        $.ajax({
            type: "POST",
            url: url,
            dataType: 'JSON',
            data: form.serialize(), // serializes the form's elements.
            success: function (response) {
                setTimeout(function () {
                    swal.close();
                    var dat = (response);
                    if (dat.status == -1) {
                        $('#errorAlert').show().html(dat.message);
                    } else {
                        window.location.href = dat.redirect;
                    }

                }, 1000);
            }
        });

    });
    /*
* Create Customer
* */

    $("#customerCreate").submit(function (e) {
        e.preventDefault();
        var form = $(this);
        var url = form.attr('action');
        $('#errorAlert').hide();
        Swal.fire({
            icon: 'success',
            html: '<b>Hệ thống đang xử lý ...</b><br/>Vui lòng không đóng trình duyệt',
            showConfirmButton: false,
            allowOutsideClick: false
        });

        $.ajax({
            type: "POST",
            url: url,
            dataType: 'JSON',
            data: form.serialize(), // serializes the form's elements.
            success: function (response) {
                setTimeout(function () {
                    swal.close();
                    var dat = (response);
                    if (dat.status == -1) {
                        $('#errorAlert').show().html(dat.message);
                    } else {
                        window.location.href = dat.redirect;
                    }

                }, 1000);
            }
        });

    });

    /*
    * Create Agency
    * */
    $("#agencyCreate").submit(function (e) {
        e.preventDefault();
        var form = $(this);
        var url = form.attr('action');
        $('#errorAlert').hide();
        Swal.fire({
            icon: 'success',
            html: '<b>Hệ thống đang xử lý ...</b><br/>Vui lòng không đóng trình duyệt',
            showConfirmButton: false,
            allowOutsideClick: false
        });

        $.ajax({
            type: "POST",
            url: url,
            dataType: 'JSON',
            data: form.serialize(), // serializes the form's elements.
            success: function (response) {
                setTimeout(function () {
                    swal.close();
                    var dat = (response);
                    if (dat.status == -1) {
                        $('#errorAlert').show().html(dat.message);
                    } else {
                        window.location.href = dat.redirect;
                    }

                }, 1000);
            }
        });

    });

});