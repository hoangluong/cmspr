<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Front_Controller extends Vcc_Controller{
    public function __construct(){
        parent::__construct();
    }
    protected function default_data(&$temp){
    }

    protected function get_editor_token(){
        $url = API_CHANNEL_URL.'/ExternalRequest/PRAuthService.ashx';
        $token = $this->generateChannelToken();
        $headers = array('Authorization:'.$token);
        $result = $this->call_api($url, array(), $headers, 0);
        if(!$result) return '';
        $result = json_decode($result);
        if($result->success === false){
            return '';
        }
        return $result->token;
    }

    protected function validateChanelToken($token, $product_private_key)
    {
        return $this->jwt->decode($token, $product_private_key);
    }

    protected function generateChannelToken($data = null)
    {
        $user_data = $this->session->userdata('user_data');

        $payload = array(
            "create" => time() - 5000,
            "expire" => time() + 5000,
            "user_id" => $user_data['id'],
            "username" => $user_data['username']
        );
        return $this->jwt->encode($payload, API_CHANNEL_SECRETKEY);
    }

    protected function generateBookingToken($data = null)
    {
        $user_data = $this->session->userdata('user_data');
        $payload = array(
            "create" => time() - 5000,
            "expire" => time() + 5000,
            "user_id" => $user_data['id'],
            "username" => $user_data['username']
        );
        return $this->jwt->encode($payload, PRODUCT_KEY);
    }

    public function call_api($url, $data=array(), $headers=array(), $post=0){
        $data_str = '';
        if(is_array($data)){
            if($data){
                foreach($data as $key=>$val){
                    $data_str .= $data_str ? '&' : '';
                    $data_str .= $key.'='.urlencode($val);
                }
            }
            if(!$post && $data_str){
                $url = strpos($url, '?')!==false ? $url : $url.'?';
                $url = $url . $data_str;
                $data_str = '';
            }
        }else{
            $data_str = $data;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        // Include header in result? (0 = yes, 1 = no)
        curl_setopt($ch, CURLOPT_HEADER, 0);

        //set header token
        //var_dump($headers);exit();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);


        if($post){
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_str);
        }else{
            curl_setopt($ch, CURLOPT_URL, $url.($data_str ? '?'.$data_str : ''));
            curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
        }
        //curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}