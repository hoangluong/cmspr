<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common
{

    function element($type, $value, $class, $id, $name)
    {
        $element = '';
        switch ($type) {
            case 'text':
                {
                    $element = '<input class="' . $class . '" id="' . $id . '" name="' . $name . '" value="' . $value . '"/>';
                }
                break;
            case 'textarea':
                {
                    $element = '<textarea class="' . $class . '" id="' . $id . '" name="' . $name . '">' . $value . '</textarea>';
                }
                break;
        }

        echo $element;
    }

    function articleStatus($status)
    {

        switch ($status) {
            case -1:
                $stt = '<span class="label label-danger">Đã hủy</span>';
                break;
            case -2:
                $stt = '<span class="label label-danger">Từ chối duyệt</span>';
                break;
            case -4:
                $stt = '<span class="label label-danger">Từ chối đăng</span>';
                break;
            case 0:
                $stt = '<span class="label label-default">Chưa gửi duyệt</span>';
                break;
            case 1:
                $stt = '<span class="label label-warning">Chờ duyệt</span>';
                break;
            case 2:
                $stt = '<span class="label label-primary">Đã duyệt</span>';
                break;
            case 3:
                $stt = '<span class="label label-primary">Đã duyệt</span>';
                break;
            case 4:
                $stt = '<span class="label label-success">Đã đăng</span>';
                break;
            default :
                $stt = 'error';
                break;
        }

        return $stt;

    }


}