<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Dashboard extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('category/Category_model');
        $this->load->model('article/Article_model');
        $this->load->model('log/Log_model');
        $this->load->model('website/Website_model');
        $this->load->model('user/User_model');
        $this->load->model('agency/Agency_model');
    }

    public function index()
    {
        $user_data = $this->session->userdata('user_data');
        if (!$this->session->userdata('user_id')) {
            redirect('login');
        }
        if ($user_data['role'] == 2) {
            $this->forBTV();
        }
        if ($user_data['role'] == 1) {
            $this->forQL();
        }
        if ($user_data['role'] == -1) {
            $this->forRoot();
        }
    }

    private function forBTV()
    {
        $user_id = $this->session->userdata('user_id');
        $currentWebsite = $this->session->userdata('currentWebsite');
        $comments = $this->Log_model->getLogByUser($user_id, $currentWebsite['id']);

        $currentDate = $this->session->userdata('currentDate');
        $startdate = !empty($currentDate) ? date('Y-m-d', strtotime($currentDate['start'])) : date('Y-m-d', strtotime('-7 days'));
        $enddate = !empty($currentDate) ? date('Y-m-d', strtotime($currentDate['end'])) : date('Y-m-d');
        

        $articles_approve = $this->Article_model->getArticleContinue($user_id,array(3,4,-4,2),$startdate,$enddate);
        $articles_publish = $this->Article_model->getArticleContinue($user_id,array(-2,-1,0,1,2,4),$startdate,$enddate);
        $temp['articles_approve'] = $articles_approve;
        $temp['articles_publish'] = $articles_publish;
        $temp['startdate'] =  date('d/m/Y', strtotime($startdate));
        $temp['enddate'] = date('d/m/Y', strtotime($enddate));
        $temp['comments'] = $comments;
        $temp['total_price_approve'] = [
            'wait_approve' => $this->Article_model->getArticlePrice($user_id,array(1),$startdate,$enddate),
            'not_approve' => $this->Article_model->getArticlePrice($user_id,array(-2),$startdate,$enddate),
            'is_approve' => $this->Article_model->getArticlePrice($user_id,array(2),$startdate,$enddate)
        ];
        $temp['total_price_publish'] = [
            'wait_publish' => $this->Article_model->getArticlePrice($user_id,array(3),$startdate,$enddate),
            'not_publish' => $this->Article_model->getArticlePrice($user_id,array(-4),$startdate,$enddate),
            'is_publish' => $this->Article_model->getArticlePrice($user_id,array(4),$startdate,$enddate)
        ];
        $temp['template'] = 'btv_index_view';
        $this->load->view('site_layout.php', $temp);
        

        // echo '<pre>';
        // var_dump($currentDate);
        // echo '</pre>';


    }

    private function forQL()
    {
        $user_id = $this->session->userdata('user_id');
        $currentWebsite = $this->session->userdata('currentWebsite');
        
        $currentDate = $this->session->userdata('currentDate');
        $startdate = !empty($currentDate) ? date('Y-m-d', strtotime($currentDate['start'])) : date('Y-m-d', strtotime('-7 days'));
        $enddate = !empty($currentDate) ? date('Y-m-d', strtotime($currentDate['end'])) : date('Y-m-d');
        
       // $articles = $this->Article_model->getArticleBtv('all', $startdate, $enddate);
        $articles = $this->Article_model->getArticlePublishedDay($startdate, $enddate);
       // $agencies = $this->Agency_model->find_all_by('website_id', $currentWebsite['id']);
        $agencies = $this->Agency_model->agencyPrice($startdate,$enddate);


        $temp['articles'] = $articles;
        $temp['agencies'] = $agencies;
        $temp['startdate'] =  date('d/m/Y', strtotime($startdate));
        $temp['enddate'] = date('d/m/Y', strtotime($enddate));
        $users = $this->User_model->getAllUserManager();

        $dataUser = array();
        foreach ($users as $item) {

            $totalxb = $this->Article_model->total(array('user_id' => $item->id, 'status' => 4));
            $tduyet = $this->Article_model->total(array('approved_by_id' => $item->id, 'status' => 4));
            $tdang = $this->Article_model->total(array('published_by_id' => $item->id, 'status' => 4));
            $item->totalxb = $totalxb;
            $item->tduyet = $tduyet;
            $item->tdang = $tdang;
            $dataUser[] = $item;
        }
        $temp['users'] = $dataUser;


        $temp['categorys'] =  $this->Article_model->getArticleWithCategory($startdate,$enddate);
        
        $charlinesl = [
            "type"=> "line",
            "min" =>$startdate,
            "max" =>$enddate,
            "items"=> [
                [
                    "label"=> "Bài viết đã đăng",
                    "data"=> $this->Article_model->getArticleAmountDay(array(4),$startdate,$enddate),
                    "fill"=> false,
                    "borderColor"=> "rgb(54, 162, 235)",
                    "backgroundColor" => "rgb(54, 162, 235)",
                    "lineTension"=> 0.1
                ],
                [
                    "label"=> "Bài viết huỷ duyệt",
                    "data"=> $this->Article_model->getArticleAmountDay(array(-2),$startdate,$enddate),
                    "fill"=> false,
                    "borderColor"=> "rgb(255, 159, 64)",
                    "backgroundColor" => "rgb(255, 159, 64)",
                    "lineTension"=> 0.1
                ],
                [
                    "label"=> "Bài viết huỷ đăng",
                    "data"=> $this->Article_model->getArticleAmountDay(array(-4),$startdate,$enddate),
                    "fill"=> false,
                    "borderColor"=> "rgb(255, 205, 86)",
                    "backgroundColor" => "rgb(255, 205, 86)",
                    "lineTension"=> 0.1
                ]
            ]
        ];
        $charlineds = [
            "type"=> "line",
            "min" =>$startdate,
            "max" =>$enddate,
            "items"=> [
                [
                    "label"=> "Bài viết đã đăng",
                    "data"=> $this->Article_model->getArticlePriceDay(array(4),$startdate,$enddate),
                    "fill"=> false,
                    "borderColor"=> "rgb(54, 162, 235)",
                    "backgroundColor" => "rgb(54, 162, 235)",
                    "lineTension"=> 0.1
                ],
                [
                    "label"=> "Bài viết huỷ duyệt",
                    "data"=> $this->Article_model->getArticlePriceDay(array(-2),$startdate,$enddate),
                    "fill"=> false,
                    "borderColor"=> "rgb(255, 159, 64)",
                    "backgroundColor" => "rgb(255, 159, 64)",
                    "lineTension"=> 0.1
                ],
                [
                    "label"=> "Bài viết huỷ đăng",
                    "data"=> $this->Article_model->getArticlePriceDay(array(-4),$startdate,$enddate),
                    "fill"=> false,
                    "borderColor"=> "rgb(255, 205, 86)",
                    "backgroundColor" => "rgb(255, 205, 86)",
                    "lineTension"=> 0.1
                ]
            ]
        ];

        $temp['charlinesl'] = json_encode($charlinesl);
        $temp['charlineds'] = json_encode($charlineds);

        $temp['template'] = 'ql_index_view';
        $this->load->view('site_layout.php', $temp);
    }

    private function forRoot()
    {
        $articles = $this->Article_model->find_all_by('status', 4);
        $websites = $this->Website_model->find_all();
        $total = array();
        foreach ($websites as $item) {
            $total[] = array(
                'website_name' => $item->name,
                'publish' => $this->Article_model->getArticlePublished(null, $item->id)
            );
        }
        $temp['total'] = $total;
        $temp['articles'] = $articles;
        $temp['template'] = 'dashboard_root_index';
        $this->load->view('site_layout.php', $temp);
    }

    private function forBTV_bk()
    {
        $fromdate = strtotime(date('Y-m-d'));
        $todate = $fromdate - 7 * 86400;
        $arrDate = array();
        $totalDestroy = array();
        $totalDestroyNum = 0;
        $totalPublishNum = 0;
        $user_data = $this->session->userdata('user_data');
        $currentWebsite = $this->session->userdata('currentWebsite');
        $totalPublishMonthNum = $this->Article_model->total(
            array(
                'status' => 4,
                'website_id' => $currentWebsite['id'],
                'publish_date >=' => date('Y-m-01'),
                'publish_date <=' => date('Y-m-d')
            )
        );
        $totalDestroyMonthNum = $this->Article_model->total(
            array(
                'status' => -1,
                'website_id' => $currentWebsite['id'],
                'created_on >=' => date('Y-m-01 00:00:00'),
                'created_on <=' => date('Y-m-d H:i:s')
            )
        );
        $temp['totalDestroyMonthNum'] = $totalDestroyMonthNum;
        $temp['totalPublishMonthNum'] = $totalPublishMonthNum;
        for ($i = $fromdate; $i >= $todate; $i = $i - 86400) {
            $arrDate[] = $i;
            // get total Article Publish by date
            $arrPublish = array(
                'status' => 4,
                'website_id' => $currentWebsite['id'],
                'publish_date' => date('Y-m-d', $i)
            );

            $totalPublish[$i] = $this->Article_model->total($arrPublish);
            $totalPublishNum += $totalPublish[$i];
            // get total Article
            $arrunDestroy = array(
                'status' => -1,
                'website_id' => $currentWebsite['id'],
                'publish_date' => date('Y-m-d', $i)
            );
            $totalDestroy[$i] = $this->Article_model->total($arrunDestroy);
            $totalDestroyNum += $totalDestroy[$i];
        }
        $temp['totalPublishNum'] = $totalPublishNum;
        $temp['totalDestroyNum'] = $totalDestroyNum;
        $temp['totalArticleRangerPublish'] = $totalPublish;
        $temp['totalArticleRangerDestroy'] = $totalDestroy;
        $temp['arrDate'] = $arrDate;
        $temp['template'] = 'index_view';
        $this->load->view('site_layout.php', $temp);

    }

    

}