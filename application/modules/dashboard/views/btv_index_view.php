<?php
$user_data = $this->session->userdata('userdata');
?>
<div class="head-content">
    <div class="container-fluid">
        <h3>Bảng điều khiển</h3>
        <div class="boxhead">
            <input type="text" size="3" class="Inputdaterangers form-control" value="<?php echo $startdate.' - '.$enddate ?>" >
        </div>
    </div>
</div>

<div class="container-fluid" style="background: transparent;">
    <div class="row">
        <div class="clearfix" style="margin: 20px 0">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-9">
                        <div class="panel pad5 clearfix">
                            <div class="clearfix pad5">
                                <div class="col-md-9"><h4>Thống kê sản lượng</h4></div>
                                <div class="col-md-3">
                                    <!-- <input type="text"
                                           value="<?php //echo date('d-m-Y', strtotime($fromdate)) . ' to ' . date('d-m-Y', strtotime($todate)) ?>"
                                           class="Inputdateranger form-control"> -->
                                </div>
                            </div>
                            <div class="col-md-12" style="padding-bottom:20px">


                                <div class="col-md-4">
                                    <div class="dashboard-item">
                                        <div class="clearfix">
                                            <div class="col-md-4">
                                                <div class="row">
                                                    <button class="btn btn-default btn-lg">Chờ duyệt</button>
                                                    <button class="btn btn-primary btn-lg"><?php echo $total_price_approve['wait_approve']->amount ?></button>
                                                    <button class="btn btn-warning btn-lg"><?php echo $total_price_approve['wait_approve']->price ? number_format($total_price_approve['wait_approve']->price,0, ',', ',') : 0  ?></button>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="row">
                                                    <button class="btn btn-default btn-lg">Từ chối</button>
                                                    <button class="btn btn-danger btn-lg"><?php echo $total_price_approve['not_approve']->amount ?></button>
                                                    <button class="btn btn-warning btn-lg"><?php echo $total_price_approve['not_approve']->price ? number_format($total_price_approve['not_approve']->price,0, ',', ','): 0 ?></button>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="row">
                                                    <button class="btn btn-default btn-lg">Đã duyệt</button>
                                                    <button class="btn btn-success btn-lg"><?php echo $total_price_approve['is_approve']->amount ?></button>
                                                    <button class="btn btn-warning btn-lg"><?php echo $total_price_approve['is_approve']->price ? number_format($total_price_approve['is_approve']->price,0, ',', ',') : 0 ?></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="dashboard-item">
                                        <div class="clearfix">
                                            <div class="col-md-2" style="width:20%">
                                                <div class="row">
                                                    <button class="btn btn-default btn-lg">Chờ xử lý</button>
                                                    <button class="btn btn-primary btn-lg"><?php echo $total_price_publish['wait_publish']->amount ?></button>
                                                    <button class="btn btn-warning btn-lg"><?php echo $total_price_publish['wait_publish']->price ? number_format($total_price_publish['wait_publish']->price,0, ',', ',') : 0  ?></button>
                                                </div>
                                            </div>
                                            <div class="col-md-2" style="width:20%">
                                                <div class="row">
                                                    <button class="btn btn-default btn-lg">Chờ xuất bản</button>
                                                    <button class="btn btn-primary btn-lg"><?php echo $total_price_publish['wait_publish']->amount ?></button>
                                                    <button class="btn btn-warning btn-lg"><?php echo $total_price_publish['wait_publish']->price ? number_format($total_price_publish['wait_publish']->price,0, ',', ',') : 0  ?></button>
                                                </div>
                                            </div>
                                            <div class="col-md-2" style="width:20%">
                                                <div class="row">
                                                    <button class="btn btn-default btn-lg">Đã xuất bản</button>
                                                    <button class="btn btn-primary btn-lg"><?php echo $total_price_publish['wait_publish']->amount ?></button>
                                                    <button class="btn btn-warning btn-lg"><?php echo $total_price_publish['wait_publish']->price ? number_format($total_price_publish['wait_publish']->price,0, ',', ',') : 0  ?></button>
                                                </div>
                                            </div>
                                            <!-- <div class="clearfix" style="margin-bottom:10px"></div> -->
                                            <div class="col-md-2" style="width:20%">
                                                <div class="row">
                                                    <button class="btn btn-default btn-lg">Từ chối</button>
                                                    <button class="btn btn-primary btn-lg"><?php echo $total_price_publish['not_publish']->amount ?></button>
                                                    <button class="btn btn-warning btn-lg"><?php echo $total_price_publish['not_publish']->price ? number_format($total_price_publish['not_publish']->price,0, ',', ',') : 0  ?></button>
                                                </div>
                                            </div>
                                            <div class="col-md-2" style="width:20%">
                                                <div class="row">
                                                    <button class="btn btn-default btn-lg">Đã đăng</button>
                                                    <button class="btn btn-primary btn-lg"><?php echo $total_price_publish['is_publish']->amount ?></button>
                                                    <button class="btn btn-warning btn-lg"><?php echo $total_price_publish['is_publish']->price ? number_format($total_price_publish['is_publish']->price,0, ',', ',') : 0  ?></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel pad5 clearfix">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4>Bài cần xử lý duyệt</h4>
                                            <table class="table">
                                                <thead class="thead-dark">
                                                <tr>
                                                    <td scope="col" style="width: 60%;">Bài viết</td>
                                                    <td scope="col">Ngày đăng</td>
                                                    <td scope="col">Giá tiền</td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                if ($articles_approve) {
                                                    foreach ($articles_approve as $item) {
                                                        ?>
                                                        <tr>
                                                            <td>
                                                            <?php echo $item->title; ?>
                                                            </td>
                                                            <td><?php echo $item->publish_time . ' ' . date('d-m-Y', strtotime($item->publish_date)); ?></td>
                                                            <td><?php echo $this->common->articleStatus($item->status); ?></td>
                                                        </tr>
                                                    <?php }
                                                } else { ?>
                                                    <tr>
                                                        <td colspan="3" style="text-align: center;">Hiện chưa có bài viết nào</td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        
                                        </div>
                                        <div class="col-md-12">
                                        <h4>Bài cần xử lý đăng</h4>
                                            <table class="table">
                                                <thead class="thead-dark">
                                                <tr>
                                                    <td scope="col" style="width: 60%;">Bài viết</td>
                                                    <td scope="col">Ngày đăng</td>
                                                    <td scope="col">Giá tiền</td>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                if ($articles_publish) {
                                                    foreach ($articles_publish as $item) {
                                                        ?>
                                                        <tr>
                                                            <td>
                                                            <?php echo $item->title; ?>
                                                            </td>
                                                            <td><?php echo $item->publish_time . ' ' . date('d-m-Y', strtotime($item->publish_date)); ?></td>
                                                            <td><?php echo $this->common->articleStatus($item->status); ?></td>
                                                        </tr>
                                                    <?php }
                                                } else { ?>
                                                    <tr>
                                                        <td colspan="3" style="text-align: center;">Hiện chưa có bài viết nào</td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                
                </div>

                    </div>
                    <div class="col-md-3">
                        <div class="panel" id="browsersFlowWidget">

                            <h3 class="panel-title">
                                Phản hồi gần đây
                            </h3>
                            <table class="table table-analytics mb-0">

                                <tbody>

                                <?php
                                if ($comments) {
                                    foreach ($comments as $item) {
                                        ?>
                                        <tr>
                                            <td>
                                                <span style="color: red">[<?php echo date('H:i d/m/Y', strtotime($item->created_on)) ?>]</span><br>
                                                <span style="color: #c0c0c0"> <?php echo $item->action ?></span>
                                            </td>
                                            <td></td>
                                        </tr>
                                    <?php }
                                } else { ?>
                                    <tr>
                                        <td colspan="2" style="text-align: center;">Hiện chưa có phản hồi nào</td>

                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>

               

            </div>
        </div>

    </div>
</div>
<style>
    .Inputdateranger {
        text-align: center;
    }

    .card {
        padding: 10px 20px;
        background-clip: padding-box;
        background: #ffffff;
        box-shadow: 0 1px 4px rgba(24, 28, 33, 0.012);
    }

    #browsersFlowWidget {
        height: auto;
    }

    .dashboard-item {
        background: #f0f8ff;
    }

    .dashboard-item h4 {
        padding-top: 10px;
        font-weight: bold;
    }

    .dashboard-item button {
        width: 100%;
    }
</style>
<script>
        $(document).ready(function () {
            $('.datepicker').datepicker({
                autoclose: true,
                format: 'dd-mm-yyyy'
            });

            $('.Inputdaterangers').daterangepicker({
                opens: 'left',
                // minDate: new Date("2019-01-01"),
                // maxDate: new Date("2050-01-01"),
                locale: {
                    format: 'DD/MM/YYYY'
                }
            }, function(start, end, label) {
                var formData = {
                    'start': start.format('YYYY-MM-DD'),
                    'end': end.format('YYYY-MM-DD'),
                };
                $.ajax({
                    url: base_url + 'auth/ajxChangeDate',
                    type: 'POST',
                    data: formData,
                    success: function (res) {
                        res = JSON.parse(res);
                        Swal.fire({
                            position: 'top-end',
                            title: 'Chuyển Ngày',
                            text: res.msg,
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'OK'
                        }).then((result) => {
                            location.reload();
                        })
                    }
                })
            });
        });
    </script>