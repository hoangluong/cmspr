<?php
$user_data = $this->session->userdata('userdata');
?>
<link rel="stylesheet"
      href="https://getbootstrapadmin.com/remark/mmenu/assets/examples/css/dashboard/analytics.min.css?v4.0.2">
<script src="https://www.chartjs.org/dist/2.9.3/Chart.min.js"></script>
<script src="https://www.chartjs.org/samples/latest/utils.js"></script>
<div class="container-fluid" style="background: transparent;">
    <div class="row">
        <div class="clearfix" style="margin: 20px 0">
            <div class="col-md-12">
                <div class="panel" id="browsersFlowWidget">

                    <h3 class="panel-title">
                        Bảng điều khiển
                    </h3>

                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel" id="browsersFlowWidget">

                <h3 class="panel-title">
                    Thống kê trong tuần
                </h3>

                <div class="panel-body">
                    <div class="form-group">
                        <table class="table table-analytics mb-0">

                            <tbody>


                            <tr>
                                <td><strong>Đã đăng</strong></td>
                                <td> <?php echo $totalPublishNum ?></td>
                            </tr>
                            <tr>
                                <td><strong>Đã hủy</strong></td>
                                <td> <?php echo $totalDestroyNum; ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br>
            <div class="panel" id="browsersFlowWidget">

                <h3 class="panel-title">
                    Thống kê trong tháng
                </h3>

                <div class="panel-body">
                    <div class="form-group">
                        <table class="table table-analytics mb-0">

                            <tbody>

                            <tr>
                                <td><strong>Đã đăng</strong></td>
                                <td> <?php echo $totalPublishMonthNum; ?></td>
                            </tr>
                            <tr>
                                <td><strong>Đã hủy</strong></td>
                                <td> <?php echo $totalDestroyMonthNum; ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div id="browsersFlowWidget" class="card card-shadow card-md">
                <h3 class="panel-title">Thống kê nhanh</h3>
                <div class="card-block px-30">
                    <div class="row">
                        <div class="col-md-6">
                            <!-- Browsers Vists Table -->
                            <table class="table table-analytics mb-0">
                                <thead>

                                <tr>
                                    <th></th>
                                    <th>Bài đăng</th>
                                    <th>Bài hủy</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $arrPublishChart = $arrDestroyChart = array();
                                $arrDateChart = array();
                                foreach ($arrDate as $item) {
                                    $arrPublishChart[] = $totalArticleRangerPublish[$item];
                                    $arrDestroyChart[] = '-' . $totalArticleRangerDestroy[$item];
                                    $arrDateChart[] = date('d-m', $item);
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo date('d/m/Y', $item) ?>
                                        </td>
                                        <td>
                                            <?php echo ($totalArticleRangerPublish[$item] > 0) ? $totalArticleRangerPublish[$item] : '-'; ?>
                                        </td>
                                        <td>
                                            <?php echo ($totalArticleRangerDestroy[$item] > 0) ? $totalArticleRangerDestroy[$item] : '-'; ?>
                                        </td>

                                    </tr>
                                <?php } ?>

                                </tbody>
                            </table>
                            <!-- End Browsers Vists Table -->

                        </div>
                        <div class="col-md-6">
                            <canvas id="canvas">

                            </canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<style>
    .card {
        padding: 10px 20px;
        background-clip: padding-box;
        background: #ffffff;
        box-shadow: 0 1px 4px rgba(24, 28, 33, 0.012);
    }

    #browsersFlowWidget {
        height: auto;
    }

    .card-body {

    }
</style>
<script>

    var barChartData = {
        labels: <?php echo json_encode($arrDateChart)?>,
        datasets: [{
            label: 'Bài hủy',
            backgroundColor: window.chartColors.red,
            data: <?php echo json_encode($arrDestroyChart)?>
        }, {
            label: 'Bài đăng',
            backgroundColor: window.chartColors.blue,
            data:<?php echo json_encode($arrPublishChart)?>
        }]

    };
    var ctx = document.getElementById('canvas').getContext('2d');
    window.myBar = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: {
            title: {
                display: false
            },
            tooltips: {
                mode: 'index',
                intersect: false
            },
            responsive: true,
            scales: {
                xAxes: [{
                    stacked: true,
                }],
                yAxes: [{
                    stacked: true
                }]
            }
        }
    });
</script>
