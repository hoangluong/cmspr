<?php
$user_data = $this->session->userdata('userdata');
?>
<script src="https://www.chartjs.org/dist/2.9.3/Chart.min.js"></script>

<div class="head-content">
    <div class="container-fluid">
        <h3>Bảng điều khiển</h3>
        <div class="boxhead">
            <input type="text" size="3" class="Inputdaterangers form-control" value="<?php echo $startdate.' - '.$enddate ?>" >
        </div>
    </div>
</div>


<div class="container-fluid" style="background: transparent;">
    <div class="row">
        <div class="clearfix" style="margin: 30px 0 20px 0">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel" id="browsersFlowWidget">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="panel-title">
                                        Doanh số chuyên mục
                                    </h4>
                                    <table class="table table-analytics mb-0">
                                        <thead>
                                        <tr>
                                            <td></td>
                                            <td>Đã duyệt</td>
                                            <td>Đã đăng</td>
                                            <td>Xuất bản</td>
                                            <td>Doanh số (Xuất bản)</td>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?php
                                        if ($categorys) {
                                            foreach ($categorys as $item) {
                                                ?>
                                                <tr>
                                                    <td><span style="color: #c0c0c0"> <?php echo $item->name ?></span></td>
                                                    <td><?php echo !empty($item->approved) ? $item->approved : '--' ?></td>
                                                    <td><?php echo !empty($item->posted) ? $item->posted : '--' ?></td>
                                                    <td><?php echo !empty($item->publish) ? $item->publish : '--' ?></td>
                                                    <td><?php echo !empty($item->publish_price) ? number_format($item->publish_price ,0, ',', ','): '--' ?></td>
                                                </tr>
                                            <?php }
                                        } else { ?>
                                            <tr>
                                                <td colspan="5" style="text-align: center;">-</td>

                                            </tr>
                                        <?php } ?>

                                        <?php
                                        //if ($users) {
                                            //foreach ($users as $item) {
                                                ?>
                                                <!-- <tr>
                                                    <td>

                                                        <span style="color: #c0c0c0"> <?php echo $item->username ?></span>
                                                    </td>

                                                    <td><?php echo ($item->tduyet) ? $item->tduyet : '-'; ?></td>
                                                    <td><?php echo ($item->tdang) ? $item->tdang : '-'; ?></td>
                                                    <td><?php echo ($item->totalxb) ? $item->totalxb : '-'; ?></td>
                                                </tr> -->
                                            <?php //}
                                        //} else { ?>
                                            <!-- <tr>
                                                <td colspan="4" style="text-align: center;">-</td>

                                            </tr> -->
                                        <?php //} ?>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>

                        <div class="panel" id="browsersFlowWidget">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3 class="panel-title">
                                        Đại lý
                                    </h3>
                                    <table class="table table-analytics mb-0">
                                        <thead>
                                        <tr>
                                            <td></td>
                                            <td>Bài đăng</td>
                                            <td>Doanh số</td>

                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?php
                                        if ($agencies) {
                                            foreach ($agencies as $item) {
                                                ?>
                                                <tr>
                                                    <td><span style="color: #0000ff"> <b><?php echo $item->name; ?></b></span></td>
                                                    <td><?php echo $item->amount; ?></td>
                                                    <td><?php echo !empty($item->publish_price) ? number_format($item->publish_price ,0, ',', ','): '--' ?></td>
                                                </tr>
                                            <?php }
                                        } else { ?>
                                            <tr>
                                                <td colspan="3" style="text-align: center;">N/A</td>

                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel pad5 clearfix">
                            <div class="clearfix pad5">
                                <div class="col-md-12"><h4>Thống kê sản lượng</h4></div>
                            </div>

                            <ul class="nav nav-tabs pad5" style="border-bottom:0;" role="tablist">
                                <li role="presentation" class="active"><a href="#sl" aria-controls="home" role="tab" data-toggle="tab">Số lượng</a></li>
                                <li role="presentation"><a href="#ds" aria-controls="profile" role="tab" data-toggle="tab">Doanh số</a></li>
                            </ul>

                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="sl">
                                    <canvas id="chartsl" ref="canvas" width="500" height="300" data-char='<?php echo $charlinesl; ?>'></canvas>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="ds">
                                    <canvas id="chartds" ref="canvas" width="500" height="300" data-char='<?php echo $charlineds; ?>'></canvas>
                                </div>
                            </div>

                        </div>

                        <div class="panel">
                            <h3 class="panel-title">
                                Bài viết xuất bản gần đây
                            </h3>
                            <table class="table">
                                <thead>
                                <tr>
                                    <td style="width: 25%;">Bài viết</td>
                                    <td>Chuyên mục</td>
                                    <td>Đại lý</td>
                                    <td>Loại bài</td>
                                    <td>Giá tiền</td>
                                    <td>Ngày đăng</td>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if ($articles) {
                                    foreach ($articles as $item) {
                                        ?>
                                        <tr>
                                            <td>
                                                <strong style="color: #2b542c;"><?php echo $item->title; ?></strong><br>
                                                <span style="text-align: justify;color: #9d9d9d;"><?php echo $item->sapo; ?></span><br>
                                                url: <a style="color: #0000FF" href="<?php echo $item->url; ?>"
                                                        target="_blank"><?php echo $item->url; ?></a>
                                            </td>
                                            <td><?php echo $item->category_name; ?></td>
                                            <td><?php echo $item->agencies_name; ?></td>
                                            <td><?php echo $item->position_name; ?></td>
                                            <td><?php echo $item->price ? number_format($item->price) : '0' ?></td>
                                            <td><?php echo $item->publish_time . ' ' . date('d-m-Y', strtotime($item->publish_date)); ?></td>
                                        </tr>
                                    <?php }
                                } else { ?>
                                    <tr>
                                        <td colspan="6" style="text-align: center;">Hiện chưa có bài viết nào</td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<style>
    .Inputdateranger {
        text-align: center;
    }

    .card {
        padding: 10px 20px;
        background-clip: padding-box;
        background: #ffffff;
        box-shadow: 0 1px 4px rgba(24, 28, 33, 0.012);
    }

    #browsersFlowWidget {
        height: auto;
    }

    .dashboard-item {
        background: #f0f8ff;
    }

    .dashboard-item h4 {
        padding-top: 10px;
        font-weight: bold;
    }

    .dashboard-item button {
        width: 100%;
    }
    .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover{
        border-bottom-color:#ddd;
        border-radius:4px;
    }
</style>

<script>
    var ctx = document.getElementById("chartsl");
    var data  = ctx.getAttribute('data-char');
    data = JSON.parse(data);
    //data.type = "line";
    data.data = {};
    data.data.datasets = data.items;
    delete data.items;
    data.options = {};
    data.options.scales = {
        xAxes: [{
            type: 'time',
            offset: true,
            time: {
                unit: 'day',
                displayFormats: {
                    day: 'DD/MM/YYYY'
                },
            },
            ticks:{
                min: data.min,
                max: data.max,
            }
        }]
    };
    data.plugins = [{
        beforeInit: function(chart, options) {
            var time = chart.options.scales.xAxes[0].ticks,
                timeDiff = moment(time.max).diff(moment(time.min), 'days');
            for (i = 0; i <= timeDiff; i++) {
                var _label = moment(time.min).add(i, 'd');
                chart.data.labels.push(_label);
            }
        }
    }];
    var chart = new Chart(ctx,data);


    var ctx1 = document.getElementById("chartds");
    var data1  = ctx1.getAttribute('data-char');
        data1 = JSON.parse(data1);
        //data1.type = "line";
        data1.data = {};
        data1.data.datasets = data1.items;
        delete data1.items;
        data1.options = {};
        data1.options.scales = {
            xAxes: [{
                type: 'time',
                offset: true,
                time: {
                    unit: 'day',
                    displayFormats: {
                        day: 'DD/MM/YYYY'
                    },
                },
                ticks:{
                    min:data1.min,
                    max:data1.max,
                }
            }],
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    stepSize: 20000000,
                    callback: function(value, index, values) {
                        // Convert the number to a string and splite the string every 3 charaters from the end
                        value = value.toString();
                        value = value.split(/(?=(?:...)*$)/);
                        //value = value.join('.');
                        return value[0] + 'm';
                    },
                    
                }
            }]
        };
        data1.options.tooltips = {
            callbacks: {
                label: function(tooltipItem, data) {
                    // var label = data.datasets[tooltipItem.datasetIndex].label || '';

                    // if (label) {
                    //    label += ': ';
                    // }
                    // label += Math.round(tooltipItem.yLabel * 100) / 100;
                    // return label;

                   // return tooltipItem.yLabel.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                   //return Intl.NumberFormat().format((tooltipItem.yLabel/1000)) + 'K';
                   return Intl.NumberFormat().format(tooltipItem.yLabel);
                }
            }
        };
        data1.plugins = [{
            beforeInit: function(chart, options) {
                var time = chart.options.scales.xAxes[0].ticks,
                    timeDiff = moment(time.max).diff(moment(time.min), 'days');
                for (i = 0; i <= timeDiff; i++) {
                    var _label = moment(time.min).add(i, 'd');
                    chart.data.labels.push(_label);
                }
            }
        }];

    var chart1 = new Chart(ctx1,data1);

    $( "#chardate" ).change(function() {
        console.log($( this ).val());

    });
</script>

<script>
        $(document).ready(function () {
            $('.datepicker').datepicker({
                autoclose: true,
                format: 'dd-mm-yyyy'
            });
            $('.Inputdaterangers').daterangepicker({
                opens: 'left',
                // minDate: new Date("2019-01-01"),
                // maxDate: new Date("2050-01-01"),
                locale: {
                    format: 'DD/MM/YYYY'
                }
            }, function(start, end, label) {
                var formData = {
                    'start': start.format('YYYY-MM-DD'),
                    'end': end.format('YYYY-MM-DD'),
                };
                $.ajax({
                    url: base_url + 'auth/ajxChangeDate',
                    type: 'POST',
                    data: formData,
                    success: function (res) {
                        res = JSON.parse(res);
                        Swal.fire({
                            position: 'top-end',
                            title: 'Chuyển Ngày',
                            text: res.msg,
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'OK'
                        }).then((result) => {
                            location.reload();
                        })
                    }
                })
            });

        });
    </script>
