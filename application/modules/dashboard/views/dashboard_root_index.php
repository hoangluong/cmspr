<link rel="stylesheet"
      href="https://getbootstrapadmin.com/remark/mmenu/assets/examples/css/dashboard/analytics.min.css?v4.0.2">
<script src="https://www.chartjs.org/dist/2.9.3/Chart.min.js"></script>
<script src="https://www.chartjs.org/samples/latest/utils.js"></script>
<div class="container-fluid" style="background: transparent;">
    <div class="row">
        <div class="clearfix" style="margin: 20px 0">
            <div class="col-md-12">
                <div class="panel" id="browsersFlowWidget">

                    <h3 class="panel-title">
                        Bảng điều khiển
                    </h3>

                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel" id="browsersFlowWidget">

                <h3 class="panel-title">
                   Thống kê sản lượng
                </h3>

                <div class="panel-body">
                    <div class="form-group">
                        <table class="table">
                            <tr>
                                <td></td>
                                <td>Đăng</td>
                                <td>Hủy</td>
                            </tr>
                            <?php
                            foreach ($total as $item) {
                                ?>
                                <tr>
                                    <td><?php echo $item['website_name'];?></td>
                                    <td><?php echo $item['publish'];?></td>
                                    <td><?php echo $item['publish'];?></td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="panel" id="browsersFlowWidget">

                <h3 class="panel-title">

                </h3>

                <div class="panel-body">
                    <div class="form-group">

                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-6">
            <div class="panel" id="browsersFlowWidget">

                <h3 class="panel-title">
                   Bài viết mới xuất bản
                </h3>

                <div class="panel-body">
                    <div class="form-group">
                        <table>
                            <?php
                            foreach ($articles as $item) {
                                ?>
                                <tr>
                                    <td>
                                        <div style="color:red;">[<?php echo $item->publish_time.' '.date('d-m-Y',strtotime($item->publish_date))?>]</div>
                                        <strong><?php echo $item->title;?></strong>
                                        <div style="color: #9d9d9d"><?php echo $item->sapo;?></div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>