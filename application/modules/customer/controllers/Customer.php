<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends Front_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('user/User_model');
        $this->load->model('website/Website_model');
        $this->load->model('customer/Customer_model');

    }

    public function index($websiteId = null)
    {
        $website = $this->session->userdata('currentWebsite');
        if ($websiteId == null) {
            $websiteId = $website['id'];
        }
        $customers = $this->Customer_model->find_all_by('website_id', $websiteId);
        $temp['customers'] = $customers;

        $temp['template'] = 'index_view';
        $this->load->view('site_layout.php', $temp);
    }

    public function create()
    {
        $temp['template'] = 'create_view';
        $this->load->view('site_layout.php', $temp);
    }

    public function save()
    {
        $info = $this->input->post(null, true);
        $website = $this->session->userdata('currentWebsite');
        try {
            $arr = array(
                'name' => $info['name'],
                'desc' => $info['desc'],
                'website_id' => $website['id']
            );
            $this->Customer_model->add($arr);
            $result['status'] = 1;
            $result['message'] = 'Thêm mới khách hàng thành công!';
            $result['redirect'] = base_url('customer');
        } catch (Exception $e) {
            $result['status'] = -1;
            $result['message'] = $e->getMessage();
        }
        echo json_encode($result);
    }
}