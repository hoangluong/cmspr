<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_model extends Base_model
{

    protected $table = 'customers';

    function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }
}