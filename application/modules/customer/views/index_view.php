<div class="container-fluid">

    <div class="white-bg pd-10">
        <h1 class="pull-left">Danh sách khách hàng</h1>
        <a href="<?php echo base_url('customer/create')?>" class="btn btn-primary pull-right">Thêm mới</a>
        <table class="list-article">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Khách hàng</th>

                <th scope="col">Trạng thái</th>

                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            <?php
            if ($customers) {
                $i=0;
                foreach ($customers as $item) {
                    $i++;
                    ?>
                    <tr>
                        <td><?php echo $i;?></td>
                        <td style="text-align: left;width: 50%;"><b><?php echo $item->name;?></b><br><?php echo $item->desc;?></td>
                        <td><span class="label label-success">Hoạt động</span></td>
                    </tr>
                <?php }
            } ?>
            </tbody>
        </table>
    </div>
</div>

<div class="loadArticleInfo">

</div>
