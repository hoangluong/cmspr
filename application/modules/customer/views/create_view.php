<div class="container">
    <div class="white-bg">
        <h1>Thêm mới thông tin khách hàng</h1>
        <hr>
        <div class="col-md-12">
            <form method="post" action="<?php echo base_url('customer/save') ?>" id="customerCreate">

                <div class="clearfix">
                    <div class="col-md-12">
                        <div id="errorAlert" class=" alert alert-danger" style="display: none;"></div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên khách hàng</label>
                            <input type="text" name="name" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Mô tả</label>
                            <textarea name="desc" id="desc" cols="30" rows="4" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <hr>

                <button type="submit" class="btn btn-primary">Thêm mới</button>
            </form>
        </div>

    </div>

</div>