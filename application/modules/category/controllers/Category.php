<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Category extends Front_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('user/User_model');
        $this->load->model('website/Website_model');
        $this->load->model('log/Log_model');
        $this->load->model('category/Category_model');
        $this->load->model('article/Article_model');
    }

    public function index($websiteId = null)
    {
        $website = $this->session->userdata('currentWebsite');
        if ($websiteId == null) {
            $websiteId = $website['id'];
        }
        $categories = $this->Category_model->getCategoryByWebsite($websiteId);
        $temp['categories'] = $categories;
        $temp['website'] = $website;
        $temp['template'] = 'index_view';
        $this->load->view('site_layout.php', $temp);
    }

    public function save()
    {
        $result = array('status' => 1, 'message' => '');
        try {
            $user_data = $this->session->userdata('user_data');
            if (!$user_data) {
                throw new Exception('unlogin');
            }

            $info = $this->input->post(null, true);
            $arrSave = array(
                'name' => $info['name'],
                'website_id' => $info['website_id'],
                'desc' => $info['desc']
            );
            $idInsert = $this->Category_model->add($arrSave);
            $result['status'] = 1;
            $result['message'] = 'Thêm mới thành công';
            $result['redirect'] = base_url('category/index/'.$info['website_id']);

        } catch (Exception $e) {
            $result['status'] = -1;
            $result['message'] = $e->getMessage();
        }
        echo json_encode($result);
        die;
    }

}