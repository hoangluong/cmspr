<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends Base_model
{

    protected $table = 'categories';

    function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function getAllCategoryByWebsite($arrWebsite)
    {
        $this->db->select('*')
            ->where_in('website_id', $arrWebsite);
        $result = $this->db->get($this->table)->result();
        return $result;
    }

    public function getCategoryByWebsite($website_id)
    {
        $this->db->select('C.*, W.name as website')
            ->join('websites W','W.id = C.website_id','inner')
            ->where('C.website_id',$website_id);
        $result = $this->db->get($this->table. ' C')->result();
        return $result;
    }


}