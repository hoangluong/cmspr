<div class="container-fluid">

    <div class="white-bg pd-10 clearfix">

        <div class="col-md-12">
            <a href="<?php echo base_url('agency/create') ?>" class="btn btn-primary pull-right">Thêm mới</a>
        </div>
        <div class="col-md-6">
            <h1 class="pull-left">Danh sách đại lý</h1>
            <table class="list-article">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Đại lý</th>
                    <th scope="col">Mô tả</th>
                    <th scope="col">Website</th>
                    <th scope="col">Trạng thái</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                <?php
                if ($agencies) {
                    $i = 0;
                    foreach ($agencies as $item) {
                        $i++;
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td style="text-align: left;width: 50%;"><b><?php echo $item->name; ?></b></td>
                            <td><?php echo $item->desc; ?></td>
                            <td><?php echo $website['domain']; ?></td>
                            <td><span class="label label-success">Hoạt động</span></td>
                        </tr>
                    <?php }
                } ?>
                </tbody>
            </table>

        </div>
        <div class="col-md-6">
            <?php
            if (isset($agencyInfo->id)) {
                ?>
                <h1>Đại lý: <?php echo $agencyInfo->name; ?></h1>
                <span><?php echo $agencyInfo->desc; ?></span>
                <br>
                <br>
                <h2>Danh sách thành viên</h2>
                <table class="table">
                    <thead class="thead">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">User</th>
                        <th scope="col">Mô tả</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($agencyUser) {
                        foreach ($agencyUser as $item) {
                            ?>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        <?php }
                    } ?>
                    </tbody>
                </table>
            <?php } ?>
        </div>
    </div>
</div>

<div class="loadArticleInfo">

</div>
