<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Agency extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('article/Article_model');
        $this->load->model('log/Log_model');
        $this->load->model('website/Website_model');
        $this->load->model('agency/Agency_model');
        $this->load->model('user/User_model');
    }


    public function index()
    {
        $currentWebsite = $this->session->userdata('currentWebsite');
        $agency = $this->input->get('agency', true);
        if ($agency) {
            $info = $this->Agency_model->find_by('id', $agency);

            $agencyUser = $this->User_model->find_all_by('agency_id', $agency);
            $temp['agencyInfo'] = $info;
            $temp['agencyUser'] = $agencyUser;
            $temp['agencyId'] = $info->id;
        }
        $temp['website'] = $currentWebsite;
        $temp['agencies'] = $this->Agency_model->find_all_by('website_id', $currentWebsite['id']);
        $temp['template'] = 'index_view';
        $this->load->view('site_layout.php', $temp);
    }

    public function save()
    {
        $info = $this->input->post(null, true);
        $website = $this->session->userdata('currentWebsite');
        try {
            $arr = array(
                'name' => $info['name'],
                'desc' => $info['desc'],
                'website_id' => $website['id']
            );
            $this->Agency_model->add($arr);
            $result['status'] = 1;
            $result['message'] = 'Thêm mới đại lý thành công!';
            $result['redirect'] = base_url('agency');
        } catch (Exception $e) {
            $result['status'] = -1;
            $result['message'] = $e->getMessage();
        }
        echo json_encode($result);
    }

    public function create()
    {

        $temp['template'] = 'create_view';
        $this->load->view('site_layout.php', $temp);
    }

    public function ajx_loadinfo()
    {

    }

}