<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agency_model extends Base_model
{

    protected $table = 'agencies';

    function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }


    public function agencyPrice($startdate,$enddate)
    {
        $website = $this->session->userdata('currentWebsite');
        // $result = $this->db->select('ag.name,count(a.id) as amount,
        //     (select  sum(p.price) as price from articles as a1 inner JOIN positions as p ON p.id = a1.position_id where a1.status=4 and a1.website_id = '.$website['id'].') as publish_price,
        // ')
        $result = $this->db->select('ag.name,count(a.id) as amount,
            (select count(a.id) * p.price as price from articles as a inner join agencies as a1 inner join positions as p on a1.website_id = a.website_id and p.id = a.position_id where a.website_id='.$website['id'].' group by p.price limit 1) as publish_price,
        ')
        ->join('articles a', 'a.website_id = ag.website_id', 'inner')
        //->join('positions p', 'p.id = a.position_id', 'inner')
        ->where('a.website_id', $website['id'])
        ->where('a.created_on BETWEEN "'.$startdate.'" and "'.$enddate.'"')
        ->where('a.status', '4')
        ->group_by("ag.name,ag.id")
        ->order_by("ag.name")->get($this->table . ' ag')->result();

    return $result;
    }

}