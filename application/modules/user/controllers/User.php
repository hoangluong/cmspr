<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class User extends Front_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('user/User_model');
        $this->load->model('website/Website_model');
        $this->load->model('log/Log_model');
        $this->load->model('category/Category_model');
        $this->load->model('categoryrole/Categoryrole_model');
        $this->load->model('websiterole/Websiterole_model');
        $this->load->model('article/Article_model');
    }

    public function index()
    {
        $user_data = $this->session->userdata('user_data');

        // $allUser = $this->User_model->find_all_by('created_by', $user_data['id']);
        $allUser = $this->User_model->getAllUserManager();

        $temp['users'] = $allUser;
        $temp['template'] = 'index_view';
        $this->load->view('site_layout.php', $temp);
    }

    public function create()
    {
        $user_data = $this->session->userdata('user_data');
        if (!in_array($user_data['role'], array(-1, 1))) {
            echo "Bạn không có quyền truy cập chức năng này";
            die;
        }
        $temp['agencies'] =
        $temp['websiteList'] = $this->Website_model->getWebsiteLists();
        $temp['template'] = 'create_view';
        $this->load->view('site_layout.php', $temp);
    }

    public function update($userId = null)
    {
        $user_data = $this->session->userdata('user_data');
        if (!in_array($user_data['role'], array(-1, 1))) {
            echo "Bạn không có quyền truy cập chức năng này";
            die;
        }
        $websiteRole = $this->Websiterole_model->getWebsitesRoleByUser($userId);
        $arrWebsiteRole = array();
        if ($websiteRole) {
            foreach ($websiteRole as $item) {
                $arrWebsiteRole[$item->website_id] = 1;
            }
        }
        $temp['arrWebsiteRole'] = $arrWebsiteRole;
        $temp['user'] = $this->User_model->find_by('id', $userId);
        $temp['websiteList'] = $this->Website_model->getWebsiteLists();

        $temp['template'] = 'update_view';
        $this->load->view('site_layout.php', $temp);
    }

    public function save()
    {
        $user_data = $this->session->userdata('user_data');
        $result = array('status' => -1, 'message' => '');
        try {
            if (!in_array($user_data['role'], array(-1, 1))) {
                echo "Bạn không có quyền truy cập chức năng này";
                die;
            }
            $username = $this->input->post('username', true);

            $password = $this->input->post('password', true);
            $fullname = $this->input->post('fullname', true);
            $birthday = $this->input->post('birthday', true);
            $email = $this->input->post('email', true);
            $phone = $this->input->post('phone', true);
            $address = $this->input->post('address', true);
            $position = $this->input->post('position', true);
            $role = $this->input->post('role', true);
            $website = $this->input->post('website_id');
            if ($username == '') {
                throw new Exception('<strong>Lỗi:</strong> Tên đăng nhập không được để trống!');
            }
            $checkUsername = $this->User_model->checkUsername($username);
            if ($checkUsername) {
                throw new Exception('<strong>Lỗi:</strong> Tên đăng nhập đã tồn tại!');
            }
            if ($password == '') {
                throw new Exception('<strong>Lỗi:</strong> Mật khẩu không hợp lệ!');
            }
            if (!in_array($role, array(1, 2))) {
                throw new Exception('<strong>Lỗi:</strong> Bạn chưa chọn quyền cho người dùng này!');
            }
            if (count($website) < 1) {
           //     throw new Exception('<strong>Lỗi:</strong> Bạn chưa phân quyền website cho người dùng này!');
            }

            $arrSave = array(
                'username' => $username,
                'fullname' => $fullname,
                'birthday' => $birthday,
                'email' => $email,
                'address' => $address,
                'position' => $position,
                'role' => $role,
                'password' => md5($password),
                'created_by' => $user_data['id']
            );
            $user_id = $this->User_model->add($arrSave);
            foreach ($website as $item) {
                $arrInsertWebsiteRule = array(
                    'user_id' => $user_id,
                    'website_id' => $item
                );
                $this->Websiterole_model->add($arrInsertWebsiteRule);
            }
            echo json_encode(array('status' => 1, 'msg' => 'Thêm mới thành công', 'redirect' => base_url('user/grandRule/' . $user_id)));
            die;

        } catch (Exception $e) {
            $result['status'] = -1;
            $result['message'] = $e->getMessage();
        }
        echo json_encode($result);
    }

    public function update_proccess()
    {
        $user_data = $this->session->userdata('user_data');
        $result = array('status' => -1, 'message' => '');
        try {
            if (!in_array($user_data['role'], array(-1, 1))) {
                echo "Bạn không có quyền truy cập chức năng này";
                die;
            }
            $id = $this->input->post('id',true);
            $fullname = $this->input->post('fullname', true);
            $birthday = $this->input->post('birthday', true);
            $email = $this->input->post('email', true);
            $phone = $this->input->post('phone', true);
            $address = $this->input->post('address', true);
            $position = $this->input->post('position', true);
            $role = $this->input->post('role', true);
            $website = $this->input->post('website_id');


            if (!in_array($role, array(1, 2))) {
                throw new Exception('<strong>Lỗi:</strong> Bạn chưa chọn quyền cho người dùng này!');
            }
            if (count($website) < 1) {
                throw new Exception('<strong>Lỗi:</strong> Bạn chưa phân quyền website cho người dùng này!');
            }

            $arrSave = array(

                'fullname' => $fullname,
                'birthday' => $birthday,
                'email' => $email,
                'address' => $address,
                'position' => $position,
                'role' => $role

            );
            $this->User_model->update($id,$arrSave);
            $this->db->delete('website_roles',array('user_id'=>$id));
            foreach ($website as $item) {
                $arrInsertWebsiteRule = array(
                    'user_id' => $id,
                    'website_id' => $item
                );
                $this->Websiterole_model->add($arrInsertWebsiteRule);

            }
            echo json_encode(array('status' => 1, 'msg' => 'Cập nhật thành công', 'redirect' => base_url('user/update/' . $id)));die;
        } catch (Exception $e) {
            $result['status'] = -1;
            $result['message'] = $e->getMessage();
        }
        echo json_encode($result);
    }

    public function grandRule($user_id = null)
    {

        $user_data = $this->session->userdata('userdata');
        $currentWebsite = $this->session->userdata('currentWebsite');
        $categories = $this->Category_model->find_all_by('website_id', $currentWebsite['id']);
        $user = $this->User_model->find_by('id', $user_id);
        $temp['websiteList'] = $this->Websiterole_model->getWebsitesRoleByUser($user->id);
        $temp['user'] = $user;
        $temp['categories'] = $categories;
        $temp['template'] = 'grand_rule_view';
        $this->load->view('site_layout.php', $temp);
    }

    public function actionGrandApprove()
    {
        $approver = $this->input->post('approver', true);
        $catId = $this->input->post('catId', true);
        $website = $this->input->post('website', true);
        $value = $this->input->post('value', true);
        $type = $this->input->post('type', true);
        $arr = array(
            'user_id' => $approver,
            'category_id' => $catId,
            'website_id' => $website,
            'type' => $type
        );
        $chk = $this->Categoryrole_model->checkCategoryRule($approver, $catId, $type);

        if ($chk == 0 && $value == 1) {
            $this->Categoryrole_model->add($arr);
        }
        if ($value == 0) {
            $this->db->delete('category_roles', array('user_id' => $approver, 'category_id' => $catId, 'type' => $type));
        }
        echo json_encode(array('status' => 1, 'msg' => 'Cập nhật thành công'));
        die;
    }

    public function actionGrandPublish()
    {
        $approver = $this->input->post('publisher', true);
        $catId = $this->input->post('catId', true);
        $website = $this->input->post('website', true);
        $value = $this->input->post('value', true);
        $arr = array(
            'user_id' => $approver,
            'category_id' => $catId,
            'website_id' => $website
        );
        $chk = $this->Categoryrole_model->checkCategoryApproveRule($approver, $catId);

        if ($chk == 0 && $value == 1) {
            $this->Categoryrole_model->add($arr);
        }
        if ($value == 0) {
            $this->db->delete('category_roles', array('user_id' => $approver, 'category_id' => $catId));
        }
        echo json_encode(array('status' => 1, 'msg' => 'Cập nhật thành công'));
        die;
    }

    public function listing(){
        $user_data = $this->session->userdata('user_data');

        // $allUser = $this->User_model->find_all_by('created_by', $user_data['id']);
        $allUser = $this->User_model->find_all();

        $temp['users'] = $allUser;
        $temp['template'] = 'listing_view';
        $this->load->view('site_layout.php', $temp);
    }
}