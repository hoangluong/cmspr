<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends Base_model {

    protected $table = 'users';
    function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }
    function get_cond($cond){
        if(isset($cond['s']) && $cond['s']){
            $this->db->group_start();
            $this->db->or_like('username', $cond['s']);
            $this->db->or_like('name', $cond['s']);
            $this->db->or_like('email', $cond['s']);
            $this->db->group_end();
            unset($cond['s']);
        }
        return $cond;
    }
    function find($cond, $page, $per_page, $order=array(), $select_fields=array()){
        $cond = $this->get_cond($cond);
        return parent::find($cond, $page, $per_page, $order, $select_fields);
    }
    function total($cond){
        $cond = $this->get_cond($cond);
        return parent::total($cond);
    }
    public function checkUsername($username){
        $this->db->where(array(
            'username' => $username
        ));
        $res = $this->db->get($this->table, 1)->result();
        if(!$res) return 0;
        return $res[0];
    }
    public function checkEmail($email){
        $this->db->where(array(
            'email' => $email
        ));
        $res = $this->db->get($this->table, 1)->result();
        if(!$res) return 0;
        return $res[0];
    }
    public function checkLogin($username, $password){
        $this->db->where(array(
            'username' => $username,
            'password' => md5($password)
        ));
        $res = $this->db->get($this->table, 1)->result();
        if(!$res) return 0;
        return $res[0];
    }
 
    public function login_api($email, $password){
        $this->db->where(array(
            'email'     =>      $email,
            'password'     =>      md5($password)
        ));
        $res = $this->db->get($this->table, 1)->result_array();
        if($res) return $res[0];
        return 0;
    }


    public function getAllUserManager(){
        $userId = $this->session->userdata('user_id');
        $website = $this->session->userdata('currentWebsite');
       $this->db->select('U.*, U.username as username, W.name as website')
                ->where('website_id',$website['id'])
                ->join('users U','U.id = UR.user_id','inner')
                ->join('websites W','W.id = UR.website_id','inner');
        $res = $this->db->get('website_roles UR')->result();
        if($res) return $res;
        return 0;
    }

    public function can($type,$category = null){
        $userId = $this->session->userdata('user_id');
        $website = $this->session->userdata('currentWebsite');
        $this->db->select('count(id) as count')
            ->from('category_roles')
            ->where('website_id',$website['id'])
            ->where('user_id',$userId)
            ->where('type',$type);
        if($category){
            $this->db->where('category_id',$category);
        }
        return $this->db->count_all_results();
    }

}