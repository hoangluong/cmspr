<div class="container">
    <div class="white-bg clearfix">
        <div class="col-md-12">
            <h1>Tạo thông tin người dùng</h1>
        </div>
        <div class="col-md-12">
            <ul class="nav nav-pills nav-justified thumbnail setup-panel">
                <li class="active"><a href="#step-1">
                        <h4 class="list-group-item-heading">Bước 1</h4>
                        <p class="list-group-item-text">Thông tin tài khoản</p>
                    </a></li>
                <li class="disabled"><a href="#step-2">
                        <h4 class="list-group-item-heading">Bước 2</h4>
                        <p class="list-group-item-text">Phân quyền</p>
                    </a></li>

            </ul>
        </div>
        <div class="col-md-12">
            <form method="post" action="<?php echo base_url('user/update_proccess') ?>" id="userCreate">
                <div class="clearfix">
                    <div class="col-md-12">
                        <div id="errorAlert" class=" alert alert-danger" style="display: none;"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên đăng nhập</label>
                            <input type="text" class="form-control" name="username" id="username"
                                   aria-describedby="emailHelp"
                                   placeholder="Tên đăng nhập" value="<?php echo $user->username?>" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Mật khẩu</label>
                            <input type="password" class="form-control" name="password" id="password"
                                   aria-describedby="emailHelp"
                                   placeholder="Mật khẩu">
                        </div>
                    </div>
                </div>
                <hr>
                <div class="clearfix">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Họ và Tên</label>
                            <input type="text" class="form-control" name="fullname" id="fullname"
                                   aria-describedby="emailHelp"
                                   placeholder="Họ và Tên" value="<?php echo $user->fullname;?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Ngày sinh</label>
                            <input type="text" class="form-control datepicker" name="birthday" id="birthday"
                                   aria-describedby="emailHelp"
                                   placeholder="Ngày sinh" value="<?php echo $user->birthday?>">
                        </div>
                    </div>
                </div>

                <div class="clearfix">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Điện thoại</label>
                            <input type="text" class="form-control" id="phone"
                                   aria-describedby="emailHelp"
                                   placeholder="Số điện thoại" value="<?php echo $user->phone;?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="email" class="form-control" name="email" id="email"
                                   aria-describedby="emailHelp"
                                   placeholder="Email" value="<?php echo $user->email?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Địa chỉ</label>
                        <textarea name="address" rows="2" class="form-control"><?php echo $user->address?></textarea>
                    </div>
                </div>
                <div class="clearfix">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Chức vụ</label>
                            <input type="text" name="position" class="form-control" id="position"
                                   aria-describedby="emailHelp"
                                   placeholder="Chức vụ" value="<?php echo $user->position;?>">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Quyền hạn</label>
                            <select name="role" id="" class="form-control">
                                <option value=""> - Chọn quyền -</option>
                                <option value="1" <?php echo ($user->role == 1)?'selected':''?>>Quản lý</option>
                                <option value="2" <?php echo ($user->role == 2)?'selected':''?>>Biên tập viên</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Website</label>
                            <select id="multiple-checkboxes" name="website_id[]" class="form-control"
                                    multiple="multiple">
                                <?php
                                if ($websiteList) {
                                    foreach ($websiteList as $item) {
                                        ?>
                                        <option value="<?php echo $item->id; ?>" <?php echo isset($arrWebsiteRole[$item->id])?'selected':'';?>><?php echo $item->name; ?></option>
                                        <?php
                                    }
                                }
                                ?>

                            </select>
                        </div>
                    </div>
                </div>
                <input type="hidden" value="<?php echo $user->id?>" name="id">
                <button type="submit" class="btn btn-primary">Tiếp tục</button>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#multiple-checkboxes').multiselect({
            // /    includeSelectAllOption: true,
            buttonWidth: '100%',
            buttonText: function (options) {
                if (options.length == 0) {
                    return ' - Chọn website - ';
                } else if (options.length > 3) {
                    return options.length + ' selected  ';
                } else {
                    var selected = [];
                    options.each(function () {
                        selected.push([$(this).text(), $(this).data('order')]);
                    });

                    selected.sort(function (a, b) {
                        return a[1] - b[1];
                    })

                    var text = '';
                    for (var i = 0; i < selected.length; i++) {
                        text += selected[i][0] + ', ';
                    }

                    return text.substr(0, text.length - 2) + ' ';
                }
            },

        });
    });
</script>