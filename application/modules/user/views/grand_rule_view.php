<?php
$_CI = &get_instance();
$_CI->load->model('category/Category_model');
?>
<div class="container">
    <div class="white-bg clearfix">
        <div class="col-md-12">
            <h1>Tạo thông tin người dùng</h1>
        </div>
        <div class="col-md-12">
            <ul class="nav nav-pills nav-justified thumbnail setup-panel">
                <li class="disabled"><a href="#step-1">
                        <h4 class="list-group-item-heading">Bước 1</h4>
                        <p class="list-group-item-text">Thông tin tài khoản</p>
                    </a></li>
                <li class="active"><a href="#step-2">
                        <h4 class="list-group-item-heading">Bước 2</h4>
                        <p class="list-group-item-text">Phân quyền</p>
                    </a></li>

            </ul>
        </div>
        <div class="col-md-12">
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="row">
                        <div class="row">
                            <ul class="list-group">
                                <li class="list-group-item">Tài khoản: <?php echo $user->username; ?></li>
                                <li class="list-group-item">Quyền:
                                    <?php
                                    switch ($user->role) {
                                        case -1 :
                                            $role = 'ROOT';
                                            break;
                                        case 1 :
                                            $role = 'Quản lý website';
                                            break;
                                        case 2 :
                                            $role = 'Biên tập viên';
                                            break;
                                        default :
                                            $role = '-';
                                            break;
                                    }
                                    echo $role;
                                    ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <table class="table table-borderless">
                <?php

                foreach ($websiteList as $web) {
                    ?>
                    <tr>
                        <td colspan="3" class="bg-primary">
                            <script>
                                $(document).ready(function () {
                                    initCategoryRole(<?php echo $user->id?>,<?php echo $web->id;?>, 'approve');
                                    initCategoryRole(<?php echo $user->id?>,<?php echo $web->id;?>, 'publish');
                                })
                            </script>
                            <?php echo $web->name; ?></td>
                    </tr>
                    <?php
                    $categories = $_CI->Category_model->find_all_by('website_id', $web->id);
                    if ($categories) {
                        ?>
                        <tr class="table-primary">
                            <th class="bg-info">Chuyên mục</th>
                            <th class="bg-info">Quyền duyệt</th>
                            <th class="bg-info">Quyền đăng</th>
                        </tr>
                        <?php
                        foreach ($categories as $item) {
                            ?>
                            <tr>
                                <td style="width: 40%;"><strong><?php echo $web->name;?> </strong>\ <?php echo $item->name; ?></td>
                                <td>
                                    <select name="grandRuleApprove" style="width: 50%;"
                                            class="grandRuleApprove grandRuleApprove_approve_<?php echo $item->id ?> form-control"
                                            website-id="<?php echo $item->website_id ?>"
                                            approve-id="<?php echo $user->id; ?>"
                                            id="<?php echo $item->id; ?>"
                                            type="approve">
                                        <option value="1">Có</option>
                                        <option value="0" selected>Không</option>
                                    </select>
                                </td>
                                <td>
                                    <select name="grandRuleApprove" style="width: 50%;"
                                            class="grandRuleApprove grandRuleApprove_publish_<?php echo $item->id ?> form-control"
                                            website-id="<?php echo $item->website_id ?>"
                                            approve-id="<?php echo $user->id; ?>"
                                            id="<?php echo $item->id; ?>"
                                            type="publish">
                                        <option value="1">Có</option>
                                        <option value="0" selected>Không</option>
                                    </select>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </table>
        </div>
    </div>
</div>
<script>

    var initCategoryRole = (user_id, website_id, type) => {
        $.ajax({
            url: base_url + 'categoryrole/getCategoryRole',
            type: 'POST',
            dataType: 'JSON',
            data: {user_id: user_id, website_id: website_id, type: type},
            success: function (response) {

                response.forEach(function (item, index) {
                    console.log(item.id);
                    $('.grandRuleApprove_' + type + '_' + item.category_id).val(1);
                })
            }
        });
    };
</script>