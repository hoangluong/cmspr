<div class="container-fluid">

    <div class="white-bg pd-10">

        <h1 class="pull-left">Danh sách người dùng</h1>
        <a href="<?php echo base_url('user/create')?>" class="btn btn-primary pull-right">Thêm mới</a>
        <table class="list-article">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Tên đăng nhập</th>
                <th scope="col">Họ và Tên</th>
                <th scope="col">Quyền</th>

                <th scope="col">Trạng thái</th>

                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            <?php
            if ($users) {
                foreach ($users as $item) {
                    ?>
                    <tr>
                        <td></td>
                        <td><?php echo $item->username?></td>
                        <td><?php echo $item->fullname?></td>
                        <td><?php


                            switch ($item->role) {
                                case -1 :
                                    $role = 'ROOT';
                                    break;
                                case 1 :
                                    $role = 'Quản lý website';
                                    break;
                                case 2 :
                                    $role = 'Biên tập viên';
                                    break;
                                default :
                                    $role = '-';
                                    break;
                            }
                            echo $role;
                            ?></td>

                        <td><?php
                            echo ($item->status == 1)?'<span class="label label-success">Kích hoạt</span>':'close';?></td>
                        <td><a href="<?php echo base_url('user/update/'.$item->id)?>" class="btn btn-primary">Thay đổi thông tin</a></td>

                    </tr>
                <?php }
            } ?>
            </tbody>
        </table>
    </div>
</div>

<div class="loadArticleInfo">

</div>
