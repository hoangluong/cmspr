<?php

class Websiterole_model extends Base_model
{

    protected $table = 'website_roles';


    function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function getWebsitesRoleByUser($user_id)
    {
        $this->db->select('*');
        $this->db->where('WR.user_id', $user_id);
        $this->db->join('websites W', 'W.id = WR.website_id', 'inner');
        $res = $this->db->get($this->table . ' WR')->result();
        if (!$res) return 0;
        return $res;
    }


}
?>