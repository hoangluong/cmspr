<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class History_model extends Base_model
{

    protected $table = 'histories';

    function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

}