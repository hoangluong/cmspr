<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log_model extends Base_model
{

    protected $table = 'article_logs';

    function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function getLogBySite($site = null)
    {

    }

    public function getLogByUser($user_id,$site_id)
    {
        $this->db->select('*')
            ->from('article_logs L')
            ->join('articles A','A.id = L.article_id','inner')
            ->where('A.user_id',$user_id)
            ->where('A.website_id',$site_id)
            ->order_by('L.id','DESC')
            ->limit(10);
        $result = $this->db->get()->result();
        if(!$result) return 0;
        return $result;
    }
}