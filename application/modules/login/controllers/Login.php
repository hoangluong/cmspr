<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Login extends Front_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('user/User_model');
        $this->load->model('websiterole/Websiterole_model');
        $this->load->model('website/Website_model');
    }

    private function redirect_back()
    {
        $current_url = $this->input->get('back', true);
        if ($current_url) {
            redirect(urldecode($current_url));
        } else {
            $user_data = $this->session->userdata('user_data');
            if($user_data['role']==1){
                redirect(base_url());
            }
            redirect(base_url());
        }
    }

    public function index()
    {
        if ($this->session->userdata('user_id')) $this->redirect_back();

        $temp = array();
        if ($_POST) {
            $temp['message'] = $this->checkLogin();
        }
        $temp['template'] = 'login_view';
        $this->load->view('login_layout.php', $temp);

    }

    public function logout(){
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('user_data');
        $back = $this->input->get('back', true);
        if($back){
            redirect(urldecode($back));
        }else{
            redirect(site_url('login'));
        }

    }

    private function checkLogin()
    {
        $username = $this->input->post('username', true);
        $password = $this->input->post('password', true);
        if (!$username || !$password) {
            return $this->message('Tên đăng nhập và mật khẩu không được để trống');
        }

        $user_data = $this->User_model->checkUsername($username);

        if (!$user_data) {
            return $this->message('Sai tên đăng nhập hoặc mật khẩu');
        }
        if ($user_data->password !== md5($password)) {
            return $this->message('Sai tên đăng nhập hoặc mật khẩu');
        }
        if (!$user_data->status) {
            return $this->message('Tài khoản của bạn hiện đang bị khóa');
        }
        $websiteRole = $this->Websiterole_model->getWebsitesRoleByUser($user_data->id);
        $domain = $this->Website_model->find_by('id',$websiteRole[0]->id);
        if($domain) {
            $this->session->set_userdata('currentWebsite',(array)$domain);
        }
        $this->session->set_userdata('user_id', $user_data->id);
        $this->session->set_userdata('role', $user_data->role);
        $this->session->set_userdata('user_data', (array)$user_data);
        $canApprove = $this->User_model->can('approve');
        $canPublish = $this->User_model->can('publish');
        $this->session->set_userdata('can',array('approve'=>$canApprove,'publish'=>$canPublish));
        //	print_r($this->session->userdata('user_id'));die;
        $this->redirect_back();
        return $this->message('Login is successful');
    }

}