<div class="limiter">
    <div class="container-login100">
        <a href="#" class="logo">
            <img src="assets/images/logo-login.png" />
        </a>
        <div class="wrap-login100">
            <!-- <div class="login100-form-title" style="background-image: url('assets/images/bg-01.jpg');">
                <span class="login100-form-title-1">
                CMSPR Login
                </span>
            </div> -->
            <form class="login100-form validate-form" method="post" action="">
                <div class="wrap-input100 validate-input m-b-26" data-validate="Username is required">

                    <input class="input100" type="text" name="username" placeholder="Tên đăng nhập">
                    <span class="focus-input100"></span>
                </div>
                <div class="wrap-input100 validate-input m-b-18" data-validate="Password is required">

                    <input class="input100" type="password" name="password" placeholder="Mật khẩu">
                    <span class="focus-input100"></span>
                </div>
                <div class="flex-sb-m w-full p-b-30">
                    <?php if (isset($message) && $message) { ?>
                        <p class="message error"><?php echo $message['msg']; ?></p>
                    <?php } ?>
                </div>

                <div class="container-login100-form-btn">
                    <button class="login100-form-btn">
                        Đăng nhập
                    </button>
                </div>
            </form>
        </div>
        
        <div class="wrap-login100footer">
            <img src="https://pr.admicro.vn/skins/images/admicro.png" alt=""/>
            <span>| 2020 © PRSolution. All Rights Reserved.</span>
        </div>

    </div>
</div>