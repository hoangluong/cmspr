<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends Front_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('user/User_model');
        $this->load->model('website/Website_model');

    }
    private function redirect_back(){
        $current_url = $this->input->get('back', true);
        if($current_url){
            redirect( urldecode($current_url) );
        }else{
            redirect(base_url());
        }
    }
    public function ajxChangeDomain($domainId){
        $domain = $this->Website_model->find_by('id',$domainId);
        if($domain){
            $this->session->set_userdata('currentWebsite',(array)$domain);
            echo json_encode(array('status'=>-1,'msg'=>'Chuyển website thành công!'));
        }else{
            echo json_encode(array('status'=>-1,'msg'=>'Có lỗi xảy ra!'));
        }
        die;
    }

    public function ajxChangeDate(){
        $start = $this->input->post('start');
        $end = $this->input->post('end');
        if(!empty($start) && !empty($end)){
            $this->session->set_userdata('currentDate',[
                'start' => $start,
                'end' => $end,
            ]);
            echo json_encode(array('status'=>true,'msg'=>'Chuyển ngày thành công!'));
        }else{
            echo json_encode(array('status'=>false,'msg'=>'Có lỗi xảy ra!'));
        }
        die;
    }

    public function ajxChangeDateReport(){
        $start = $this->input->post('start');
        $end = $this->input->post('end');
        if(!empty($start) && !empty($end)){
            $this->session->set_userdata('currentDateReport',[
                'start' => $start,
                'end' => $end,
            ]);
            echo json_encode(array('status'=>true,'msg'=>'Chuyển ngày thành công!'));
        }else{
            echo json_encode(array('status'=>false,'msg'=>'Có lỗi xảy ra!'));
        }
        die;
    }

}
