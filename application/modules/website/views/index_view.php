<div class="container-fluid">
    <div class="white-bg pd-10">
        <h1 class="pull-left">Danh sách website</h1>
        <a href="<?php echo base_url('website/create')?>" class="btn btn-primary pull-right">Thêm mới</a>
        <table class="list-article">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Website</th>
                <th scope="col" >đường dẫn</th>
                <th scope="col">Mô tả</th>
                <th scope="col"></th>

            </tr>
            </thead>
            <tbody>
            <?php
            if ($website) {
                foreach ($website as $item) {
                    ?>
                    <tr>

                        <td><?php echo $item->id;?></td>
                        <td><?php echo $item->name;?></td>
                        <td><?php echo $item->domain;?></td>
                        <td><?php echo $item->desc;?></td>
                        <td><a href="<?php echo base_url('category/index/'.$item->id)?>" class="btn btn-info">Chuyên mục</a></td>
                    </tr>
                <?php }
            } ?>
            </tbody>
        </table>
    </div>
</div>

<div class="loadArticleInfo">

</div>
