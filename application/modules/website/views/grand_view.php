<div class="container">
    <div class="white-bg clearfix">
        <div class="col-md-12">
            <h1>Phân quyền</h1>
            <hr>
        </div>

        <div class="col-md-12">
            <form method="post" action="<?php echo base_url('website/save') ?>" id="websiteCreate">
                <div class="clearfix">
                    <div class="col-md-12">
                        <div id="errorAlert" class=" alert alert-danger" style="display: none;"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Tên website</label>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Tên website">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Domain</label>
                            <input type="text" class="form-control" name="domain" id="domain" placeholder="Url">
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Mô tả</label>
                            <textarea name="desc" id="desc" cols="30" rows="10" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <hr>
                <button type="submit" class="btn btn-primary">Thêm mới</button>
            </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {

    });
</script>