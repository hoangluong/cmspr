<?php

class Website_model extends Base_model
{

    protected $table = 'websites';
    protected $table_role = 'website_roles';

    function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function getWebsiteLists()
    {

        $user_data = $this->session->userdata('user_data');
        if ($user_data['role'] == -1) {
            return $this->find_all();
        }
        $this->db->select('*');

        $this->db->where('WR.user_id', $user_data['id']);

        $this->db->join('websites W', 'W.id = WR.website_id', 'inner');
        $res = $this->db->get($this->table_role . ' WR')->result();
        if (!$res) return 0;
        return $res;
    }


}

?>