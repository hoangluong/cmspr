<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Website extends Front_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('user/User_model');
        $this->load->model('website/Website_model');
        $this->load->model('log/Log_model');
        $this->load->model('category/Category_model');
        $this->load->model('article/Article_model');
        $this->load->model('websiterole/Websiterole_model');
    }

    public function index()
    {
        $websites = $this->Website_model->find_all();
        $temp['website'] = $websites;

        $temp['template'] = 'index_view';
        $this->load->view('site_layout.php', $temp);
    }


    public function create()
    {
        $user_data = $this->session->userdata('user_data');
        if ($user_data['role'] != -1) {
            die('error');
        }

        $temp['template'] = 'create_view';
        $this->load->view('site_layout.php', $temp);
    }

    public function save()
    {
        $result = array('status' => 1, 'message' => '');
        try {
            $user_data = $this->session->userdata('user_data');
            if (!$user_data) {
                throw new Exception('unlogin');
            }

            $info = $this->input->post(null, true);
            if ($info['name'] == '') throw new Exception("<strong>Lỗi:</strong> Tên website không được để trống.");
            if ($info['domain'] == '') throw new Exception("<strong>Lỗi:</strong> Domain không được để trống.");

            $arrSave = array(
                'name' => $info['name'],
                'domain' => $info['domain'],
                'desc' => $info['desc']
            );
            $username = $info['username'];
            $userInfo = $this->User_model->find_by('username', $username);
            if (!$userInfo) {
                throw new Exception("<strong>Lỗi:</strong> Username không tồn tại");
            }
            $idInsert = $this->Website_model->add($arrSave);
            $infoWeb = $this->Website_model->find_by('id', $idInsert);
            $this->Websiterole_model->add(array('user_id' => $userInfo->id, 'website_id' => $idInsert));
            $result['status'] = 1;
            $result['message'] = 'Thêm mới thành công';
            $this->session->set_userdata('currentWebsite', (array)$infoWeb);
            $result['redirect'] = base_url('category/index/' . $idInsert);

        } catch (Exception $e) {
            $result['status'] = -1;
            $result['message'] = $e->getMessage();
        }
        echo json_encode($result);
        die;
    }

    public function grand($websiteId = null)
    {

    }
}