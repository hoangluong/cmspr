<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Report extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('category/Category_model');
        $this->load->model('article/Article_model');
        $this->load->model('position/Position_model');
        $this->load->model('log/Log_model');
        $this->load->model('website/Website_model');
        $this->load->model('user/User_model');
        $this->load->model('agency/Agency_model');
    }

    public function index(){

        $key = $this->input->get('key', true);
        $category = $this->input->get('cat', true);
        $agencie = $this->input->get('ag', true);
        $position = $this->input->get('pos', true);

        $temp['key_'] = $key;
        $temp['category_'] = $category;
        $temp['agencie_'] = $agencie;
        $temp['position_'] = $position;


        $website = $this->session->userdata('currentWebsite');

        $currentDate = $this->session->userdata('currentDateReport');
        $startdate = !empty($currentDate) ? date('Y-m-d', strtotime($currentDate['start'])) : date('Y-m-d', strtotime('-7 days'));
        $enddate = !empty($currentDate) ? date('Y-m-d', strtotime($currentDate['end'])) : date('Y-m-d');
        
        
        $temp['startdate'] =  date('d/m/Y', strtotime($startdate));
        $temp['enddate'] = date('d/m/Y', strtotime($enddate));

        $myArticle = $this->Article_model->getMyAll();
        $categories = $this->Category_model->find_all_by('website_id', $website['id']);
        $agencies = $this->Agency_model->find_all_by('website_id', $website['id']);
        $positions = $this->Position_model->find_all_by('website_id', $website['id']);

        $temp['positions'] = $positions;
        $temp['agencies'] = $agencies;
        $temp['categories'] = $categories;
        $temp['articles'] = $myArticle;
        $temp['articles_list'] = $this->Article_model->getArticlePublishedDay($startdate, $enddate,$key,$category,$agencie,$position);
        $charlinesl = [
            "type"=> "bar",
            "min" =>$startdate,
            "max" =>$enddate,
            "items"=> [
                [
                    "label"=> "Số lượng",
                    "data"=> $this->Article_model->getArticleAmountDay(array(4),$startdate,$enddate),
                    "fill"=> false,
                    "borderColor"=> "rgb(54, 162, 235)",
                    "backgroundColor" => "rgb(54, 162, 235)",
                    "lineTension"=> 0.1
                ]
            ]
        ];
        $charlineds = [
            "type"=> "bar",
            "min" =>$startdate,
            "max" =>$enddate,
            "items"=> [
                [
                    "label"=> "Doanh số",
                    "data"=> $this->Article_model->getArticlePriceDay(array(4),$startdate,$enddate),
                    "fill"=> false,
                    "borderColor"=> "rgb(255, 99, 132)",
                    "backgroundColor" => "rgb(255, 99, 132)",
                    "lineTension"=> 0.1
                ]
            ]
        ];

        $temp['charlinesl'] = json_encode($charlinesl);
        $temp['charlineds'] = json_encode($charlineds);

        // echo "<pre>";
        // print_r($this->Article_model->getArticlePriceDay(array(4),$startdate,$enddate));
        // echo "</pre>";
        
        $temp['template'] = 'index_view';
        $this->load->view('site_layout.php', $temp);
    }

}