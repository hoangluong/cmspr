<?php
$user_data = $this->session->userdata('userdata');
?>
<link rel="stylesheet"
      href="https://getbootstrapadmin.com/remark/mmenu/assets/examples/css/dashboard/analytics.min.css?v4.0.2">
<script src="https://www.chartjs.org/dist/2.9.3/Chart.min.js"></script>
<script src="https://www.chartjs.org/samples/latest/utils.js"></script>
<div class="container-fluid" style="background: transparent;">
    <div class="row">
        <div class="clearfix" style="margin: 20px 0">
            <div class="col-md-12">
                <div class="panel" id="browsersFlowWidget">

                    <h3 class="panel-title">
                        Số liệu thống kê
                    </h3>

                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="panel pad5 clearfix">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-pills pad5" role="tablist">
                    <li role="presentation" class="active"><a href="#news" aria-controls="home" role="tab" data-toggle="tab">Thống kê bài viết</a></li>
                    <li role="presentation"><a href="#newsprice" aria-controls="profile" role="tab" data-toggle="tab">Thống kê sản lượng bài</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="news">
                        <form class="filter-box white-bg" action="" method="get" id="report_filter1">
                            <br>
                            <div class="form-inline">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="key" value="<?php echo !empty($key_) ? $key_ : ''; ?>" placeholder="Tên bài viết" size="40">
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></div>
                                        <input type="text" class="form-control datereport1" size="23" value="<?php echo $startdate.' - '.$enddate ?>" placeholder="">
                                    </div>
                                    
                                </div>
                                <div class="form-group">&nbsp;
                                    <select class="form-control" name="cat">
                                        <option value="">Chuyên mục</option>
                                        <?php  if ($categories) { foreach ($categories as $item) { ?>
                                            <option value="<?php echo $item->id; ?>" <?php echo ($item->id == $category_) ? 'selected' : ''; ?>><?php echo $item->name; ?></option>
                                        <?php }}?>
                                    </select>
                                </div>
                                <div class="form-group">&nbsp;
                                    <select class="form-control" name="ag">
                                        <option value="">Đại lý</option>
                                        <?php  if ($agencies) { foreach ($agencies as $item) { ?>
                                            <option value="<?php echo $item->id; ?>" <?php echo ($item->id == $agencie_) ? 'selected' : ''; ?>><?php echo $item->name; ?></option>
                                        <?php }}?>
                                    </select>
                                </div>
                                <div class="form-group">&nbsp;
                                    <select class="form-control" name="pos">
                                        <option value="">Loại bài</option>
                                        <?php  if ($positions) { foreach ($positions as $item) { ?>
                                            <option value="<?php echo $item->id; ?>" <?php echo ($item->id == $position_) ? 'selected' : ''; ?>><?php echo $item->name; ?></option>
                                        <?php }}?>
                                    </select>
                                </div>
                                &nbsp;
                                <button type="submit" class="btn btn-primary">Tìm kiếm</button>&nbsp;
                                <button type="button" class="btn btn-warning">Xuất Excel</button>
                            </div>
                            <br>
                            <table class="table table-condensed">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col" style="width: 60%;">Bài viết</th>
                                        <th scope="col">Chuyên mục</th>
                                        <th scope="col">Đại lý</th>
                                        <th scope="col">Loại bài</th>
                                        <th scope="col">Giá tiền</th>
                                        <th scope="col">Ngày đăng</th>
                                    </tr>
                                    </thead>
                                <tbody>
                                    <?php 
                                    if ($articles_list) {
                                        foreach ($articles_list as $key=>$item) { ?>
                                            <tr>
                                                <td>
                                                    <strong style="color: #2b542c;"><?php echo $item->title; ?></strong><br>
                                                    <span style="text-align: justify;color: #9d9d9d;"><?php echo $item->sapo; ?></span><br>
                                                    url: <a style="color: #0000FF" href="<?php echo $item->url; ?>"
                                                            target="_blank"><?php echo $item->url; ?></a>
                                                </td>
                                                <td><?php echo $item->category_name; ?></td>
                                                <td><?php echo $item->agencies_name; ?></td>
                                                <td><?php echo $item->position_name; ?></td>
                                                <td><?php echo $item->price ? number_format($item->price) : '0' ?></td>
                                                <td><?php echo $item->publish_time . ' ' . date('d-m-Y', strtotime($item->publish_date)); ?></td>
                                            </tr>

                                    <?php    }
                                    } ?>
                                </tbody>
                            </table>
                        </form>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="newsprice">
                        <form class="filter-box white-bg" action="" method="get" id="report_filter2">
                            <br>
                            <div class="form-inline">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></div>
                                        <input type="text" class="form-control datereport1" size="23" value="<?php echo $startdate.' - '.$enddate ?>" placeholder="">
                                    </div>
                                    
                                </div>
                                <div class="form-group">&nbsp;
                                    <select class="form-control" name="cat">
                                        <option value="">Chuyên mục</option>
                                        <?php  if ($categories) { foreach ($categories as $item) { ?>
                                            <option value="<?php echo $item->id; ?>" <?php echo ($item->id == $category_) ? 'selected' : ''; ?>><?php echo $item->name; ?></option>
                                        <?php }}?>
                                    </select>
                                </div>
                                <div class="form-group">&nbsp;
                                    <select class="form-control" name="ag">
                                        <option value="">Đại lý</option>
                                        <?php  if ($agencies) { foreach ($agencies as $item) { ?>
                                            <option value="<?php echo $item->id; ?>" <?php echo ($item->id == $agencie_) ? 'selected' : ''; ?>><?php echo $item->name; ?></option>
                                        <?php }}?>
                                    </select>
                                </div>
                                <div class="form-group">&nbsp;
                                    <select class="form-control" name="pos">
                                        <option value="">Loại bài</option>
                                        <?php  if ($positions) { foreach ($positions as $item) { ?>
                                            <option value="<?php echo $item->id; ?>" <?php echo ($item->id == $position_) ? 'selected' : ''; ?>><?php echo $item->name; ?></option>
                                        <?php }}?>
                                    </select>
                                </div>
                                &nbsp;
                                <button type="submit" class="btn btn-primary">Tìm kiếm</button>&nbsp;
                                <button type="button" class="btn btn-warning">Xuất Excel</button>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6"><canvas id="report_chartjs-1" ref="canvas" width="500" height="300" data-char='<?php echo $charlinesl; ?>'></canvas></div>
                                <div class="col-md-6"><canvas id="report_chartjs-2" ref="canvas" width="500" height="300" data-char='<?php echo $charlineds; ?>'></canvas></div>
                            </div>
                        </form>
                        
                    </div>
                </div>
            
            </div>
       

        
        </div>

    </div>
</div>

<script>
 
 var ctx = document.getElementById("report_chartjs-1");
    var data  = ctx.getAttribute('data-char');
    data = JSON.parse(data);
    data.data = {};
    data.data.datasets = data.items;
    delete data.items;
    data.options = {};
    data.options.scales = {
        xAxes: [{
            type: 'time',
            offset: true,
            time: {
                unit: 'day',
                displayFormats: {
                    day: 'DD/MM/YYYY'
                },
            },
            ticks:{
                min: data.min,
                max: data.max,
            }
        }]
    };
    data.plugins = [{
        beforeInit: function(chart, options) {
            var time = chart.options.scales.xAxes[0].ticks,
                timeDiff = moment(time.max).diff(moment(time.min), 'days');
            for (i = 0; i <= timeDiff; i++) {
                var _label = moment(time.min).add(i, 'd');
                chart.data.labels.push(_label);
            }
        }
    }];
    var chart = new Chart(ctx,data);

    console.log(chart);
    


    var ctx1 = document.getElementById("report_chartjs-2");
    var data1  = ctx1.getAttribute('data-char');
        data1 = JSON.parse(data1);
        data1.data = {};
        data1.data.datasets = data1.items;
        delete data1.items;
        data1.options = {};
        data1.options.scales = {
            xAxes: [{
                type: 'time',
                offset: true,
                time: {
                    unit: 'day',
                    displayFormats: {
                        day: 'DD/MM/YYYY'
                    },
                },
                ticks:{
                    min:data1.min,
                    max:data1.max,
                }
            }],
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    stepSize: 20000000,
                    callback: function(value, index, values) {
                        // Convert the number to a string and splite the string every 3 charaters from the end
                        value = value.toString();
                        value = value.split(/(?=(?:...)*$)/);
                        //value = value.join('.');
                        return value[0] + 'm';
                    },
                    
                }
            }]
        };
        data1.options.tooltips = {
            callbacks: {
                label: function(tooltipItem, data) {
                    // var label = data.datasets[tooltipItem.datasetIndex].label || '';

                    // if (label) {
                    //    label += ': ';
                    // }
                    // label += Math.round(tooltipItem.yLabel * 100) / 100;
                    // return label;

                   // return tooltipItem.yLabel.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
                   //return Intl.NumberFormat().format((tooltipItem.yLabel/1000)) + 'K';
                   return Intl.NumberFormat().format(tooltipItem.yLabel);
                }
            }
        };
        data1.plugins = [{
            beforeInit: function(chart, options) {
                var time = chart.options.scales.xAxes[0].ticks,
                    timeDiff = moment(time.max).diff(moment(time.min), 'days');
                for (i = 0; i <= timeDiff; i++) {
                    var _label = moment(time.min).add(i, 'd');
                    chart.data.labels.push(_label);
                }
            }
        }];

    var chart1 = new Chart(ctx1,data1);

</script>

<script>
        $(document).ready(function () {
            $('.datereport1').daterangepicker({
                opens: 'left',
                locale: {
                    format: 'DD/MM/YYYY'
                }
            }, function(start, end, label) {
                var formData = {
                    'start': start.format('YYYY-MM-DD'),
                    'end': end.format('YYYY-MM-DD'),
                };
                $.ajax({
                    url: base_url + 'auth/ajxChangeDateReport',
                    type: 'POST',
                    data: formData,
                    success: function (res) {
                        res = JSON.parse(res);
                        location.reload();
                    }
                })
            });

            $('.datereport2').daterangepicker({
                opens: 'left',
                locale: {
                    format: 'DD/MM/YYYY'
                }
            }, function(start, end, label) {
                var formData = {
                    'start': start.format('YYYY-MM-DD'),
                    'end': end.format('YYYY-MM-DD'),
                };
                $.ajax({
                    url: base_url + 'auth/ajxChangeDateReport',
                    type: 'POST',
                    data: formData,
                    success: function (res) {
                        res = JSON.parse(res);
                        location.reload();
                    }
                })
            });
        });
    </script>