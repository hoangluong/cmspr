<div class="container-fluid">

    <div class="white-bg pd-10 clearfix">

        <div class="col-md-6">
            <h2>Thêm vị trí bài</h2>
            <form method="post" action="<?php echo base_url('position/save') ?>" id="positionCreate">
                <div class="clearfix">
                    <div class="col-md-12">
                        <div id="errorAlert" class=" alert alert-danger" style="display: none;"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Website</label>
                            <input type="hidden" value="<?php echo $website['id']; ?>" name="website_id" class="">
                            <input type="text" class="form-control" value="<?php echo $website['name']; ?>"
                                   name="website" id="website" placeholder="Tên website">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Vị trí</label>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Tên chuyên mục">
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                    <div class="col-md-12">
                        <div id="errorAlert" class=" alert alert-danger" style="display: none;"></div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Giá tiền</label>

                            <input type="text" class="form-control" value=""
                                   name="price" id="price" placeholder="Giá tiền">
                        </div>
                    </div>

                </div>
                <div class="clearfix">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Mô tả</label>
                            <textarea name="desc" id="desc" cols="30" rows="10" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <hr>
                <button type="submit" class="btn btn-primary">Thêm mới</button>
            </form>
        </div>
        <div class="col-md-6">
            <table class="list-article">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Vị trí</th>
                    <th scope="col">Giá tiền</th>
                    <th scope="col">Website</th>
                    <th scope="col">Mô tả</th>
                    <th scope="col"></th>

                </tr>
                </thead>
                <tbody>
                <?php
                if ($positions) {
                    $i=0;
                    foreach ($positions as $item) {
                        $i++;
                        ?>
                        <tr>

                            <td><?php echo $i; ?></td>
                            <td><?php echo $item->name; ?></td>
                            <td><?php echo number_format($item->price); ?></td>
                            <td><?php echo $website['name']; ?></td>
                            <td style="width: 30%;"><?php echo $item->desc; ?></td>
                            <td></td>
                        </tr>
                    <?php }
                } ?>
                </tbody>
            </table>
        </div>
    </div>

</div>

<div class="loadArticleInfo">

</div>
