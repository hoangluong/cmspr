<div class="white-bg">
    <div class="col-md-12">
        <br>
        <br>
        <ul class="nav nav-pills nav-justified thumbnail setup-panel">
            <li class="disabled"><a href="#step-1">
                    <h4 class="list-group-item-heading">Bước 1</h4>
                    <p class="list-group-item-text">Thông tin bài đăng</p>
                </a></li>
            <li class="active"><a href="#step-2">
                    <h4 class="list-group-item-heading">Bước 2</h4>
                    <p class="list-group-item-text">Dựng bài</p>
                </a></li>
            <!--  <li class="disabled"><a href="#step-3">
                      <h4 class="list-group-item-heading">Bước 3</h4>
                      <p class="list-group-item-text">Duyệt bài</p>
                  </a></li>
              <li class="disabled"><a href="#step-3">
                      <h4 class="list-group-item-heading">Bước 4</h4>
                      <p class="list-group-item-text">Đăng bài</p>
                  </a></li>-->
        </ul>
        <!-- /.Horizontal Steppers -->
    </div>

    <iframe src="<?php echo $editor_url; ?>" data-booking_id="<?php echo $article->id; ?>" id="frame_editor"
            style="border:none;"></iframe>

    <div class="col-md-12">
        <form method="post" action="<?php echo base_url('article/updateArticleContent') ?>" id="editorSave">
            <div class="clearfix">

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Website</label>
                        <input type="hidden" value="<?php echo $website['id'];?>" name="website_id" class="">
                        <input type="text" class="form-control" value="<?php echo $website['name'];?>" name="website" id="website" readonly placeholder="Tên website">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tên chuyên mục</label>
                        <input type="text" class="form-control" readonly name="name" id="name" value="<?php echo $article->category_name;?>" placeholder="Tên chuyên mục">
                    </div>
                </div>
            </div>
            <div class="clearfix">
                <div class="col-md-12">
                    <div id="errorAlert" class=" alert alert-danger" style="display: none;"></div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tiêu đề</label>
                        <input type="text" name="title" class="form-control">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Sapo</label>
                        <textarea name="sapo" id="sapo" cols="30" rows="4" class="form-control"></textarea>
                    </div>
                </div>
            </div>
            <hr>
            <input type="hidden" value="<?php echo $article->id;?>" name="id">
            <button type="submit" class="btn btn-primary">Cập nhật</button>
        </form>
    </div>
    
</div>
<script type="text/javascript">
    var cb_save_pr_article = function (params) {
        //save article
        console.log('cb_save_pr_article', params);
        var dataInput = params.data;
        dataInput.booking_id = $('#frame_editor').data('booking_id');
        if (dataInput.hasOwnProperty('act')) {
            console.log('action', dataInput.act);
            if (dataInput.act === 'save') {
                $.ajax({
                    url: base_url + 'article/save_article',
                    type: 'post',
                    dataType: 'json',
                    data: dataInput,
                    success: function (result) {
                        $('#frame_editor').attr('src', '<?php echo API_CHANNEL_URL;?>/Signout.aspx');
                        window.location = '<?php echo $back_url;?>';
                    }
                });
            } else if (dataInput.act === 'canceledit') {
                window.location = '<?php echo $back_url;?>';
            } else {
            }
        }
    }
    window.addEventListener("message", cb_save_pr_article, false);


    $(document).ready(function () {
        $('#frame_editor').css({
            width: ($(window).width() - 50) + 'px',
            height: ($(window).height()) + 'px'
        }).on('load', function () {
            /*var header_height = $('header').height();
            var footer_height = $('footer').height();
            //var height_iframe = document.getElementById('frame_editor').contentWindow.document.body.offsetHeight;
            var height_iframe = $(window).height() - header_height - footer_height;*/
            var height_iframe = $('.main').height();
            $('#frame_editor').css({
                height: height_iframe + 'px'
            });
        });
    });
</script>