<div class="container-fluid">
    <div class="white-bg pd-10">

        <form class="filter-box white-bg" action="" method="get" id="booking_filter">

            <div class="row">
                <div class="col-xs-12 cols-sm-6 col-md-12">
                    <h1 class="entry-title pull-left" style="margin-bottom: 10px;">Bài viết đã đăng</h1>
                </div>

                <div class="col-xs-12 cols-sm-6 col-md-2">
                    <input type="text" class="form-control" placeholder="Từ khóa tìm kiếm">
                </div>


                <div class="col-xs-12 cols-sm-6 col-md-1">
                    <div class="input-group">
                        <input type="submit" class="btn btn-primary" style="width: 100%; margin-top: 0;"
                               value="tìm kiếm">
                    </div>
                </div>
                <!-- <div class="col-xs-12 cols-sm-6 col-md-1">
                    <input id="search" class="btn btn-sm btn-info" type="submit" value="View"/>
                </div> -->
            </div>
        </form>
        <table class="list-article">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Trạng thái</th>
                <th scope="col" colspan="2">Bài viết</th>
                <th scope="col">Chuyên mục</th>
                <th scope="col">Đại lý</th>
                <th scope="col">Loại bài</th>
                <th scope="col">Giá tiền</th>
                <th scope="col">Ngày đăng</th>
                <th scope="col">Tác giả</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            <?php
            if ($articles) {
                foreach ($articles as $item) {
                    ?>
                    <tr class="article-list-item" data-article-id="<?php echo $item->id; ?>">
                        <td scope="row"><?php echo $item->id; ?></td>
                        <td>
                            <?php
                            echo $this->common->articleStatus($item->status);
                            ?>
                        </td>
                        <td><img src="<?php echo $item->avatar?>"  style="width: 50px; height: 50px;" alt=""></td>
                        <td style="text-align: left">
                            <?php
                            if (!$item->channel_article_id) {
                                ?>
                                <span class="label label-danger">Chưa dựng bài</span>
                                <?php
                            }else{
                                ?>
                                <div><a href="#" style="font-weight: bold;"><?php echo $item->title?></a></div>
                                <div><div style="color: #c0c0c0"><?php echo $item->sapo?></div></div>
                            <?php }?>
                        </td>
                        <td><?php echo $item->category_name; ?></td>
                        <td><?php echo $item->agencie_name; ?></td>
                        <td><?php echo $item->position_name; ?></td>
                        <td><?php echo $item->price ? number_format($item->price) : '0' ?></td>
                        <td><?php echo $item->publish_time . ' ' . date('d/m/Y', strtotime($item->publish_date)); ?></td>
                        <td><?php echo $item->username; ?></td>
                        <td>
                            <a href="<?php echo ($item->layout)?$item->layout:'';?>" class="btn btn-sm btn-info <?php echo ($item->layout)?'':'disabled';?>"> <i class="fa fa-link"></i> Layout</a>
                            <a href="<?php echo ($item->url)?$item->url:'';?>" class="btn btn-sm btn-success <?php echo ($item->url)?'':'disabled';?>"> <i class="fa fa-link"></i> Url</a>
                        </td>

                    </tr>
                <?php }
            } ?>
            </tbody>
        </table>
    </div>
</div>

<div class="loadArticleInfo">

</div>
