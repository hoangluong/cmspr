<!-- Modal -->
<script style="<?php echo base_url('js/main.js') ?>"></script>
<div class="modal right fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
    <div class="modal-dialog" role="document" style="width: 30%;">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" style="color: red;opacity: 1;">#<?php echo $article->id; ?>
                </button>
                <h4 class="modal-title" id="myModalLabel2"><strong>Thông tin bài viết</strong></h4>
            </div>

            <div class="modal-body">

                <div class="form-group clearfix">
                    <label for="kpi_request" class="control-label col-sm-3">Website</label>

                    <div class="col-sm-9">
                        <input type="text" class="form-control" readonly value="<?php echo $article->website_name; ?>">
                    </div>
                </div>
                <div class="form-group clearfix">
                    <label for="kpi_request" class="control-label col-sm-3">Chuyên mục</label>

                    <div class="col-sm-9">
                        <input type="text" class="form-control" readonly value="<?php echo $article->category_name; ?>">
                    </div>
                </div>
                <div class="form-group clearfix">
                    <label for="kpi_request" class="control-label col-sm-3">Ngày đăng</label>
                    <div class="col-sm-9">
                        <input type="text" readonly
                               value="<?php echo date('d/m/Y', strtotime($article->publish_date)) ?>"
                               class="form-control">
                    </div>
                </div>
                <div class="form-group clearfix">
                    <label for="kpi_request" class="control-label col-sm-3">Giờ đăng</label>

                    <div class="col-sm-9">

                        <input type="text" readonly value="<?php echo $article->publish_time; ?>" class="form-control">
                    </div>
                </div>
                <div class="form-group clearfix">
                    <label for="kpi_request" class="control-label col-sm-3">Loại bài</label>

                    <div class="col-sm-9">

                    </div>
                </div>
                <br>

                <!-- <div class="box box-contract">
                    <h4 class="box-title"><b>Thông tin khách hàng</b></h4>
                    <div class="box-body">
                        <p><?php echo $article->customer_name; ?></p>

                    </div>
                </div>
                <br> -->

                <div class="box box-article">
                    <h4 class="box-title"><b>Bài viết</b></h4>
                    <div class="box-body">
                        <p><?php echo $article->title; ?></p>
                        <p><strong>Layout:</strong><br><a target="_blank"
                                                          href="<?php echo $article->layout ?>"><?php echo $article->layout ?></a>
                        </p>
                        <p><strong>Link bài:</strong><br><a target="_blank"
                                                            href="<?php echo $article->url ?>"><?php echo $article->url ?></a>
                        </p>
                    </div>
                </div>

                <div style="text-align: right;" class="pad10"><a class="bold" style="color: black"
                                                                 href="javascript:$('.history_list').toggle('slow');">Lịch
                        sử bài viết</a></div>
                <div class="history_list"
                     style="border: 1px solid rgb(204, 204, 204); border-radius: 10px; margin-bottom: 10px; background: rgba(227, 239, 227, 0.8);display: none;">
                    <table class="table">
                        <tbody>
                        <?php
                        if ($logs) {
                            foreach ($logs as $item) {
                                ?>
                                <tr>
                                    <td><?php echo $item->action ?></td>
                                    <td><?php echo date('H:i d/m/Y', strtotime($item->created_on)); ?></td>
                                </tr>
                            <?php }
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <!-- Action:  hủy-->
                <?php
                if (in_array($article->status, array(0, 1, 2, 3)) && $article->user_id == $this->session->userdata('user_id')) {
                    ?>
                    <button type="button" data-article-id="<?php echo $article->id ?>" id="destroyArticle"
                            class="btn btn-danger btn-sm pull-left"> Hủy
                    </button>
                <?php } ?>
                <!-- Action: Khôi phục-->
                <?php
                if (in_array($article->status, array(-1))) {
                    ?>
                    <button type="button" data-article-id="<?php echo $article->id ?>" id="unDestroyArticle"
                            class="btn btn-danger btn-sm pull-left"> Khôi phục
                    </button>
                <?php } ?>
                <!-- Action: Viết bài-->
                <?php
                if (in_array($article->status, array(0, 1, 2, 3)) && !$article->channel_article_id) {
                    ?>
                    <a type="button" href="<?php echo base_url('article/editor/' . $article->id) ?>"
                       data-book_id="22998"
                       id="load_editor"
                       class="btn btn-primary btn-sm pull-left">Viết bài</a>
                <?php } ?>
                <!-- Action: Sửa bài-->
                <?php
                if ($article->channel_article_id && (in_array($article->status, array(0, 1, 2, 3)))) {
                    ?>
                    <a type="button" href="<?php echo base_url('article/editor/' . $article->id) ?>"
                       class="btn btn-primary btn-sm pull-left">Sửa bài</a>
                <?php } ?>
                <!-- Action: Gửi duyệt bài-->
                <?php
                if ($article->channel_article_id && (in_array($article->status, array(0,-2)))) {
                    ?>
                    <button type="button" class="btn btn-warning btn-sm pull-left"
                            data-article-id="<?php echo $article->id ?>" id="sendToAprrove">Gửi duyệt
                    </button>
                <?php } ?>
                <!-- Action: Duyệt bài-->
                <?php
                if ($article->channel_article_id && (in_array($article->status, array(1))) && $canApprove != 0) {
                    ?>
                    <button type="button" class="btn btn-warning btn-sm pull-left"
                            data-article-id="<?php echo $article->id ?>" id="actionApprove">Duyệt bài
                    </button>

                    <button type="button" class="btn btn-danger btn-sm pull-left"
                            data-article-id="<?php echo $article->id ?>" data-toggle="modal" data-target="#unApproved">
                        Không duyệt
                    </button>
                <?php } ?>
                <!-- Action: Duyệt đăng-->
                <?php
                if ($article->channel_article_id && (in_array($article->status, array(3))) && $canPublish != 0) {
                    ?>
                    <button type="button" class="btn btn-info btn-sm pull-left"
                            data-article-id="<?php echo $article->id ?>" id="actionPublish">Đăng bài
                    </button>
                    <button type="button" class="btn btn-danger btn-sm pull-left"
                            data-article-id="<?php echo $article->id ?>" data-toggle="modal" data-target="#unPublish">
                       Không đăng
                    </button>
                <?php } ?>

                <!-- Action: Cập nhật thông tin-->
                <div class="text-left inline-block">
                    <span class="modal-notice text-danger bold ver-middle marleft10"></span>

                    <button id="booking_save" type="submit" class="btn btn-success btn-sm" data-dismiss="modal"
                            aria-label="Close">
                        Đóng
                    </button>

                </div>
            </div>
        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div><!-- modal -->
<div id="unApproved" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form action="<?php echo base_url('article/ajx_actionUnAprrove')?>" method="get" id="formUnApprovedReason">
            <!-- Modal content-->
            <input type="hidden" value="<?php echo $article->id?>" name="article_id">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Lý do bạn không duyệt bài viết này?</h4>
                </div>
                <div class="modal-body">

                    <textarea name="reason" id="reason" cols="30" rows="10" class="form-control"></textarea>
                </div>
                <div class="modal-footer">
                    <span id="errorAlert"></span>
                    <button type="submit" class="btn btn-danger">Không duyệt</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div id="unPublish" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form action="<?php echo base_url('article/ajx_actionUnPublish')?>" method="get" id="formUnPublish">
            <!-- Modal content-->
            <input type="hidden" value="<?php echo $article->id?>" name="article_id">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Lý do bạn không đăng bài viết này?</h4>
                </div>
                <div class="modal-body">

                    <textarea name="reason" id="reason" cols="30" rows="10" class="form-control"></textarea>
                </div>
                <div class="modal-footer">
                    <span id="errorAlert"></span>
                    <button type="submit" class="btn btn-danger">Không đăng</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    /*
* Action: Destroy article
* */
    $('#destroyArticle').click(function () {
        var articleId = $(this).attr('data-article-id');


        $.ajax({
            url: base_url + 'article/ajx_destroyArticle',
            type: "GET",
            dateType: "JSON",
            data: {
                article_id: articleId
            },
            success: function (result) {
                var result = JSON.parse(result);
                if (result.status == -1) {
                    showAlertAction('error', result.message);
                } else {
                    showAlertAction('success', result.message);
                }
            }
        });

    });
    /*
    * Action: Khôi phục bài viết bị hủy
    * */
    $('#unDestroyArticle').click(function () {
        var articleId = $(this).attr('data-article-id');
        $.ajax({
            url: base_url + 'article/ajx_UnDestroyArticle',
            type: "GET",
            dateType: "JSON",
            data: {
                article_id: articleId
            },
            success: function (result) {
                var result = JSON.parse(result);
                if (result.status == -1) {
                    showAlertAction('error', result.message);
                } else {
                    showAlertAction('success', result.message);
                }
            }
        });
    });
    /*
    * Action: Gửi duyệt
    * */
    $('#sendToAprrove').click(function () {
        var articleId = $(this).attr('data-article-id');
        $.ajax({
            url: base_url + 'article/ajx_sendToAprrove',
            type: "GET",
            dateType: "JSON",
            data: {
                article_id: articleId
            },
            success: function (result) {
                var result = JSON.parse(result);
                if (result.status == -1) {
                    showAlertAction('error', result.message);
                } else {
                    showAlertAction('success', result.message);
                }
            }
        });
    });
    /*
        * Action: Duyệt bài
        * */
    $('#actionApprove').click(function () {
        var articleId = $(this).attr('data-article-id');
        $.ajax({
            url: base_url + 'article/ajx_actionAprrove',
            type: "GET",
            dateType: "JSON",
            data: {
                article_id: articleId
            },
            success: function (result) {
                var result = JSON.parse(result);
                if (result.status == -1) {
                    showAlertAction('error', result.message);
                } else {
                    showAlertAction('success', result.message);
                }
            }
        });
    });
    /*
            * Action: Đăng bài
            * */
    $('#actionPublish').click(function () {
        var articleId = $(this).attr('data-article-id');
        $.ajax({
            url: base_url + 'article/ajx_actionPublish',
            type: "GET",
            dateType: "JSON",
            data: {
                article_id: articleId
            },
            success: function (result) {
                var result = JSON.parse(result);
                if (result.status == -1) {
                    showAlertAction('error', result.message);
                } else {
                    showAlertAction('success', result.message);
                }
            }
        });
    });

    var showAlertAction = (icon, message) => {
        Swal.fire({
            icon: icon,
            html: message,
            showConfirmButton: true
        }).then((result) => {
            location.reload();
        });
    }

    /*
    * Action: Không duyệt
    * */

    $("#formUnApprovedReason,#formUnPublish").submit(function (e) {
        e.preventDefault();
        var form = $(this);
        var url = form.attr('action');


        $.ajax({
            type: "POST",
            url: url,
            dataType: 'JSON',
            data: form.serialize(), // serializes the form's elements.
            success: function (response) {
                setTimeout(function () {
                    swal.close();
                    var dat = (response);
                    if (dat.status == -1) {
                        $('#errorAlert').show().css('color','red').html(dat.message);
                    } else {
                        window.location.href = dat.redirect;
                    }

                }, 1000);
            }
        });

    });

</script>
