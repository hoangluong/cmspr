<div class="container-fluid">
    <div class="white-bg clearfix">
        <div class="col-md-12">
            <h1>Đăng bài</h1>
        </div>
        <div class="col-md-12">
            <br>
            <br>
            <ul class="nav nav-pills nav-justified thumbnail setup-panel">
                <li class="active"><a href="#step-1">
                        <h4 class="list-group-item-heading">Bước 1</h4>
                        <p class="list-group-item-text">Khởi tạo thông tin bài đăng</p>
                    </a></li>
                <li class="disabled"><a href="#step-2">
                        <h4 class="list-group-item-heading">Bước 2</h4>
                        <p class="list-group-item-text">Dựng bài</p>
                    </a></li>
               <!-- <li class="disabled"><a href="#step-3">
                        <h4 class="list-group-item-heading">Bước 3</h4>
                        <p class="list-group-item-text">Duyệt bài</p>
                    </a></li>
                <li class="disabled"><a href="#step-3">
                        <h4 class="list-group-item-heading">Bước 4</h4>
                        <p class="list-group-item-text">Đăng bài</p>
                    </a></li>-->
            </ul>
            <!-- /.Horizontal Steppers -->
        </div>
        <div class="col-md-6">
            <div class="article-create">
                <form method="post" action="save" id="formSaveArticle">
                    <div class="alert alert-danger" id="errorAlert" style="display: none;">

                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="exampleInputEmail1">Website</label>
                                <input type="hidden" value="<?php echo $website['id']; ?>" name="site_id">
                                <input type="text" class="form-control" readonly
                                       value="<?php echo $website['domain']; ?>">
                            </div>
                            <div class="col-md-6">
                                <label for="exampleInputEmail1">Chuyên mục</label>
                                <select name="channel_id" id="channel_id" class="form-control select2">
                                    <option value=""> - Chuyên mục -</option>
                                    <?php
                                    if (isset($categories[0])) {
                                        foreach ($categories as $item) {
                                            ?>
                                            <option value="<?php echo $item->id ?>"><?php echo $item->name ?></option>
                                        <?php }
                                    } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="exampleInputEmail1">Loại bài</label>
                                <select name="fm_id" id="fm_id" class="form-control select2">
                                    <option value=""> - Loại bài -</option>
                                    <?php
                                    if (isset($postions[0])) {
                                        foreach ($postions as $item) {
                                            ?>
                                            <option value="<?php echo $item->id ?>" data-price="<?php echo number_format($item->price)?>"><?php echo $item->name ?></option>
                                        <?php }
                                    } ?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="exampleInputEmail1">Giá tiền</label>
                                <input type="text" readonly id="price" class="form-control" style="color: red;font-weight: bold">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="exampleInputEmail1">Ngày</label>
                                <input type="text" name="publish_date" class="form-control datepicker" value="<?php echo date('d-m-Y') ?>">
                            </div>
                            <div class="col-md-6">
                                <label for="exampleInputEmail1">Giờ</label>
                                <input type="text" id="publish_time" autocomplete="off" name="publish_time" class="form-control">
                            </div>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="exampleInputEmail1">Khách hàng</label>
                                <select name="customer_id" id="" class="form-control select2">
                                    <option value=""> - Khách hàng -</option>
                                    <?php
                                    if (isset($customers[0])) {
                                        foreach ($customers as $item) {
                                            ?>
                                            <option value="<?php echo $item->id ?>" ><?php echo $item->name ?></option>
                                        <?php }
                                    } ?>
                                </select>
                            </div>

                        </div>
                    </div> -->
                    <div class="form-group">
                        <div class="input-group input-file" name="Fichier1">
                            <label for="">File đính kèm (nếu có)</label>
                            <input type="file" class="form-control" placeholder='Choose a file...'/>
                        </div>
                    </div>




                    <button type="submit" class="btn btn-primary">Tiếp tục</button>
                </form>
            </div>
        </div>

    </div>
</div>
<script>
    $('#publish_time').timepicker({
        timeFormat: 'H:mm',
        interval: 15,
        defaultTime: '<?php echo date('H')?>',
        startTime: '00:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
    $('#fm_id').change(function () {
        var option = $('option:selected', this).attr('data-price');
        $('#price').val(option);

    });
</script>