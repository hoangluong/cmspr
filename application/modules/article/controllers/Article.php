<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Article extends Front_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('user/User_model');
        $this->load->model('website/Website_model');
        $this->load->model('log/Log_model');
        $this->load->model('category/Category_model');
        $this->load->model('position/Position_model');
        $this->load->model('article/Article_model');
       // $this->load->model('customer/Customer_model');
    }

    public function index()
    {
        $website = $this->session->userdata('currentWebsite');
        $userId = $this->session->userdata('user_id');
        $category = $this->input->get('category', true);
        $status = $this->input->get('status', true);
        $category = (isset($category)) ? $category : 'all';
        $status = (isset($status)) ? $status : 'all';

        $key = ($this->input->get('key', true)) ? $this->input->get('key', true) : null;
        $myArticle = $this->Article_model->getMyAll($key, $status, $category);
        $categories = $this->Category_model->find_all_by('website_id', $website['id']);
        $temp['category'] = $category;
        $temp['status'] = $status;
        $temp['key'] = $key;
        $temp['categories'] = $categories;
        $temp['articles'] = $myArticle;
        $temp['template'] = 'index_view';
        $this->load->view('site_layout.php', $temp);
    }

    public function approve()
    {
        $userId = $this->session->userdata('user_id');
        $myArticle = $this->Article_model->getArticleForApprove();

        $temp['articles'] = $myArticle;
        $temp['template'] = 'wait_approve_view';
        $this->load->view('site_layout.php', $temp);
    }

    public function approved()
    {
        $userId = $this->session->userdata('user_id');
        $myArticle = $this->Article_model->getArticleApproved();

        $temp['articles'] = $myArticle;
        $temp['template'] = 'approved_view';
        $this->load->view('site_layout.php', $temp);
    }

    public function notapproved()
    {
        $userId = $this->session->userdata('user_id');
        $myArticle = $this->Article_model->getNotArticleApproved();

        $temp['articles'] = $myArticle;
        $temp['template'] = 'notapproved_view';
        $this->load->view('site_layout.php', $temp);
    }

    public function published()
    {
        $userId = $this->session->userdata('user_id');
        $myArticle = $this->Article_model->getArticlePublished();

        $temp['articles'] = $myArticle;
        $temp['template'] = 'published_view';
        $this->load->view('site_layout.php', $temp);
    }
    public function notpublished()
    {
        $userId = $this->session->userdata('user_id');
        $myArticle = $this->Article_model->getNotArticlePublished();

        $temp['articles'] = $myArticle;
        $temp['template'] = 'notpublished_view';
        $this->load->view('site_layout.php', $temp);
    }
    public function publish()
    {
        $userId = $this->session->userdata('user_id');
        $myArticle = $this->Article_model->getArticleForPublish();

        $temp['articles'] = $myArticle;
        $temp['template'] = 'wait_publish_view';
        $this->load->view('site_layout.php', $temp);
    }

    public function create()
    {

        $userId = $this->session->userdata('user_id');
        $website = $this->session->userdata('currentWebsite');

        $categories = $this->Category_model->find_all_by('website_id', $website['id']);
        $positions = $this->Position_model->find_all_by('website_id', $website['id']);
        //$customers = $this->Customer_model->find_all_by('website_id', $website['id']);
        $temp['website'] = $website;
        $temp['postions'] = $positions;
        $temp['categories'] = $categories;
       // $temp['customers'] = $customers;
        $temp['template'] = 'create_view';
        $this->load->view('site_layout.php', $temp);
    }

    public function save()
    {

        $user_data = $this->session->userdata('user_data');
        $result = array('status' => 1, 'message' => '');
        try {
            $fm_id = $this->input->post('fm_id', true);
            $site_id = $this->input->post('site_id', true);
            $channel_id = $this->input->post('channel_id', true);
            $publish_date = $this->input->post('publish_date', true);
            $publish_time = $this->input->post('publish_time', true);
            //$customer_id = $this->input->post('customer_id', true);
            if (!$site_id) throw new Exception('<strong>Lỗi:</strong> không tìm thấy website');
            if (!$channel_id) throw new Exception('<strong>Lỗi:</strong> Bạn chưa chọn chuyên mục');
            if (!$fm_id) throw new Exception("<strong>Lỗi:</strong> Bạn chưa chọn loại bài");
            if (!$publish_date) throw new Exception("<strong>Lỗi:</strong> Bạn chưa chọn ngày đăng");

            $arrSave = array(
                'website_id' => $site_id,
                'category_id' => $channel_id,
                'publish_date' => date('Y-m-d', strtotime($publish_date)),
                'publish_time' => $publish_time,
                'status' => 0,
                'position_id' => $fm_id,
                //'customer_id' => $customer_id,
                'user_id' => $user_data['id']
            );
            $articleId = $this->Article_model->add($arrSave);
            /* Save Log */
            $arrSaveLog = array(
                'user_id' => $user_data['id'],
                'article_id' => $articleId,
                'action' => '[' . $user_data['username'] . '] đã khởi tạo bài viết #' . $articleId
            );
            $this->Log_model->add($arrSaveLog);
            /* end Save Log*/
            $result['status'] = 1;
            $result['message'] = 'Khởi tạo thành công';
            $result['redirect'] = base_url('article/editor/' . $articleId);
        } catch (Exception $e) {
            $result['status'] = -1;
            $result['message'] = $e->getMessage();
        }
        echo json_encode($result);


    }

    public function ajx_LoadArticleInfo()
    {
        $articleId = $this->input->get('article_id', true);
        $article = $this->Article_model->getArtileInfo($articleId);
        $logs = $this->Log_model->find_all_by('article_id', $articleId);
        $canApprove = $this->User_model->can('approve', $article->category_id);
        $canPublish = $this->User_model->can('publish', $article->category_id);
        $temp['canApprove'] = $canApprove;
        $temp['canPublish'] = $canPublish;
        $temp['article'] = $article;
        $temp['logs'] = $logs;
        $temp['template'] = 'partical/popup_info_view';
        $this->load->view('null_layout.php', $temp);
    }

    public function editor($articleId = null)
    {
        $article = $this->Article_model->getArtileInfo($articleId);
        $data = array();
        $website = $this->session->userdata('currentWebsite');
        $temp['website'] = $website;
        $temp['article'] = $article;
        $data['editor_token'] = $this->get_editor_token();
        $editor_url = API_CHANNEL_URL . '/Signin.aspx?' . 'token=' . urlencode($data['editor_token']) . '&r_id=' . $article->id;
        $temp['editor_url'] = $editor_url;
        $temp['back_url'] = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : base_url('article');
        $temp['template'] = 'editor_view';
        $this->load->view('site_layout.php', $temp);
    }

    public function ajx_approve()
    {
        $articleId = $this->input->get('article_id', true);
        $articleInfo = $this->Article_model->find_by();
        $isAprroveRule = $this->Article_model->get_category_approve_role();
    }

    public function ajx_destroyArticle()
    {
        $articleId = $this->input->get('article_id', true);
        $result = array('status' => 1, 'message' => '');
        try {
            $articleInfo = $this->Article_model->find_by('id', $articleId);
            if (!isset($articleInfo->id)) {
                throw new Exception('<strong>Lỗi:</strong> Bài viết không tồn tại!');
            }
            $user_id = $this->session->userdata('user_id');
            $user_data = $this->session->userdata('user_data');
            if (!$user_id) {
                throw new Exception('<strong>Lỗi:</strong> Bạn chưa đăng nhập. hãy đăng nhập để thực hiện chức năng này!');
            }
            if ($articleInfo->user_id != $user_id) {
                throw new Exception('<strong>Lỗi:</strong> Bạn không có quyền hủy bài viết này!');
            }
            if (in_array($articleInfo->status, array(-1, 4))) {
                throw new Exception('<strong>Lỗi:</strong> Bạn không có quyền hủy bài viết này!');
            }
            $this->Article_model->update($articleId, array('status' => -1));
            $arrSaveLog = array(
                'user_id' => $user_id,
                'article_id' => $articleId,
                'action' => '[' . $user_data['username'] . '] đã hủy bài viết #' . $articleId
            );
            $this->Log_model->add($arrSaveLog);
            $result['status'] = 1;
            $result['message'] = 'Hủy bài viết thành công!';

        } catch (Exception $e) {
            $result['status'] = -1;
            $result['message'] = $e->getMessage();
        }
        echo json_encode($result);


    }

    public function ajx_UnDestroyArticle()
    {
        $articleId = $this->input->get('article_id', true);
        $result = array('status' => 1, 'message' => '');
        try {
            $articleInfo = $this->Article_model->find_by('id', $articleId);
            if (!isset($articleInfo->id)) {
                throw new Exception('<strong>Lỗi:</strong> Bài viết không tồn tại!');
            }
            $user_id = $this->session->userdata('user_id');
            $user_data = $this->session->userdata('user_data');
            if (!$user_id) {
                throw new Exception('<strong>Lỗi:</strong> Bạn chưa đăng nhập. hãy đăng nhập để thực hiện chức năng này!');
            }
            if ($articleInfo->user_id != $user_id) {
                throw new Exception('<strong>Lỗi:</strong> Bạn không có quyền khôi phục bài viết này!');
            }
            if (in_array($articleInfo->status, array(0, 1, 2, 3, 4, 5))) {
                throw new Exception('<strong>Lỗi:</strong> Bạn không có quyền  khôi phục bài viết này.!');
            }
            $this->Article_model->update($articleId, array('status' => 0));
            $arrSaveLog = array(
                'user_id' => $user_id,
                'article_id' => $articleId,
                'action' => '[' . $user_data['username'] . '] đã khôi phục bài viết #' . $articleId
            );
            $this->Log_model->add($arrSaveLog);
            $result['status'] = 1;
            $result['message'] = 'Khôi phục bài viết thành công!';

        } catch (Exception $e) {
            $result['status'] = -1;
            $result['message'] = $e->getMessage();
        }
        echo json_encode($result);
    }

    public function ajx_sendToAprrove()
    {
        $articleId = $this->input->get('article_id', true);
        $result = array('status' => 1, 'message' => '');
        try {
            $articleInfo = $this->Article_model->find_by('id', $articleId);
            if (!isset($articleInfo->id)) {
                throw new Exception('<strong>Lỗi:</strong> Bài viết không tồn tại!');
            }
            $user_id = $this->session->userdata('user_id');
            $user_data = $this->session->userdata('user_data');
            if (!$user_id) {
                throw new Exception('<strong>Lỗi:</strong> Bạn chưa đăng nhập. hãy đăng nhập để thực hiện chức năng này!');
            }
            if ($articleInfo->user_id != $user_id) {
                throw new Exception('<strong>Lỗi:</strong> Bạn không có quyền gửi duyệt bài viết này!');
            }
            if (!in_array($articleInfo->status, array(0,-2))) {
                throw new Exception('<strong>Lỗi:</strong> Bạn không có quyền gửi đuyệt bài viết này.!');
            }
            $this->Article_model->update($articleId, array('status' => 1));
            $arrSaveLog = array(
                'user_id' => $user_id,
                'article_id' => $articleId,
                'action' => '[' . $user_data['username'] . '] đã gửi duyệt bài viết #' . $articleId
            );
            $this->Log_model->add($arrSaveLog);
            $result['status'] = 1;
            $result['message'] = 'Gửi duyệt bài viết thành công!';
            $result['redirect'] = base_url('article');
        } catch (Exception $e) {
            $result['status'] = -1;
            $result['message'] = $e->getMessage();
        }
        echo json_encode($result);
    }

    public function ajx_actionAprrove()
    {
        $articleId = $this->input->get('article_id', true);
        $result = array('status' => 1, 'message' => '');
        try {
            $articleInfo = $this->Article_model->find_by('id', $articleId);
            if (!isset($articleInfo->id)) {
                throw new Exception('<strong>Lỗi:</strong> Bài viết không tồn tại!');
            }
            $user_id = $this->session->userdata('user_id');
            $user_data = $this->session->userdata('user_data');
            if (!$user_id) {
                throw new Exception('<strong>Lỗi:</strong> Bạn chưa đăng nhập. hãy đăng nhập để thực hiện chức năng này!');
            }

            if (!in_array($articleInfo->status, array(1))) {
                throw new Exception('<strong>Lỗi:</strong> Bạn không có quyền duyệt bài viết này.!');
            }
            $this->Article_model->update($articleId, array('status' => 3, 'approved_by_id' => $user_id, 'approved_by_name' => $user_data['username']));
            $arrSaveLog = array(
                'user_id' => $user_id,
                'article_id' => $articleId,
                'action' => '[' . $user_data['username'] . '] Duyệt bài viết "<span style="color: #0000FF;">' .  word_limiter($articleInfo->title,5,'...').'</span>"'
            );
            $this->Log_model->add($arrSaveLog);
            $result['status'] = 1;
            $result['message'] = 'Duyệt bài viết thành công!';

        } catch (Exception $e) {
            $result['status'] = -1;
            $result['message'] = $e->getMessage();
        }
        echo json_encode($result);
    }

    public function ajx_actionUnAprrove()
    {
        $articleId = $this->input->post('article_id', true);
        $reason = $this->input->post('reason', true);
        $result = array('status' => 1, 'message' => '');
        try {
            $articleInfo = $this->Article_model->find_by('id', $articleId);
            if (!isset($articleInfo->id)) {
                throw new Exception('<strong>Lỗi:</strong> Bài viết không tồn tại!');
            }
            $user_id = $this->session->userdata('user_id');
            $user_data = $this->session->userdata('user_data');
            if (!$user_id) {
                throw new Exception('<strong>Lỗi:</strong> Bạn chưa đăng nhập. hãy đăng nhập để thực hiện chức năng này!');
            }
            if ($reason == '') {
                throw new Exception('<strong>Lỗi:</strong> Bạn phải nhập lý do không duyệt bài viết này!');
            }
            if (!in_array($articleInfo->status, array(1))) {
                throw new Exception('<strong>Lỗi:</strong> Bạn không có quyền duyệt bài viết này.!');
            }
            $this->Article_model->update($articleId, array('status' => -2, 'approved_by_id' => $user_id, 'approved_by_name' => $user_data['username']));
            $arrSaveLog = array(
                'user_id' => $user_id,
                'article_id' => $articleId,
                'action' => '[' . $user_data['username'] . '] đã không duyệt bài viết "<span style="color: #0000FF;">' . word_limiter($articleInfo->title,5,'...') . '</span>". lý do: <span style="color: #0000FF;">' . $reason . '</span>'
            );
            $this->Log_model->add($arrSaveLog);
            $result['status'] = 1;
            $result['message'] = 'Thao tác thành công!';
            $result['redirect'] = base_url('article/approve');
            

        } catch (Exception $e) {
            $result['status'] = -1;
            $result['message'] = $e->getMessage();
        }
        echo json_encode($result);
    }



    public function ajx_actionPublish()
    {
        $articleId = $this->input->get('article_id', true);
        $result = array('status' => 1, 'message' => '');
        try {
            $articleInfo = $this->Article_model->find_by('id', $articleId);
            if (!isset($articleInfo->id)) {
                throw new Exception('<strong>Lỗi:</strong> Bài viết không tồn tại!');
            }
            $user_id = $this->session->userdata('user_id');
            $user_data = $this->session->userdata('user_data');
            if (!$user_id) {
                throw new Exception('<strong>Lỗi:</strong> Bạn chưa đăng nhập. hãy đăng nhập để thực hiện chức năng này!');
            }

            if (!in_array($articleInfo->status, array(3))) {
                throw new Exception('<strong>Lỗi:</strong> Bạn không có quyền đăng bài viết này.!');
            }
            $this->Article_model->update($articleId, array('status' => 4, 'published_by_id' => $user_id, 'published_by_name' => $user_data['username']));
            $arrSaveLog = array(
                'user_id' => $user_id,
                'article_id' => $articleId,
                'action' => '[' . $user_data['username'] . '] Đăng bài viết <b>' . $articleInfo->title . '</b>'
            );
            $this->Log_model->add($arrSaveLog);
            $result['status'] = 1;
            $result['message'] = 'Đăng bài viết thành công!';

        } catch (Exception $e) {
            $result['status'] = -1;
            $result['message'] = $e->getMessage();
        }
        echo json_encode($result);
    }

    public function ajx_actionUnPublish()
    {
        $articleId = $this->input->post('article_id', true);
        $reason = $this->input->post('reason', true);
        $result = array('status' => 1, 'message' => '');
        try {
            $articleInfo = $this->Article_model->find_by('id', $articleId);
            if (!isset($articleInfo->id)) {
                throw new Exception('<strong>Lỗi:</strong> Bài viết không tồn tại!');
            }
            $user_id = $this->session->userdata('user_id');
            $user_data = $this->session->userdata('user_data');
            if (!$user_id) {
                throw new Exception('<strong>Lỗi:</strong> Bạn chưa đăng nhập. hãy đăng nhập để thực hiện chức năng này!');
            }
            if ($reason == '') {
                throw new Exception('<strong>Lỗi:</strong> Bạn phải nhập lý do không duyệt bài viết này!');
            }
            if (!in_array($articleInfo->status, array(3))) {
                throw new Exception('<strong>Lỗi:</strong> Bạn không có quyền duyệt bài viết này.!');
            }
            $this->Article_model->update($articleId, array('status' => -4, 'approved_by_id' => $user_id, 'approved_by_name' => $user_data['username']));
            $arrSaveLog = array(
                'user_id' => $user_id,
                'article_id' => $articleId,
                'action' => '[' . $user_data['username'] . '] đã không đăng bài viết "<span style="color: #0000FF;">' . word_limiter($articleInfo->title,5,'...') . '</span>". lý do: <span style="color: #0000FF;">' . $reason . '</span>'
            );
            $this->Log_model->add($arrSaveLog);
            $result['status'] = 1;
            $result['message'] = 'Thao tác thành công!';
            $result['redirect'] = base_url('acticle/approve');

        } catch (Exception $e) {
            $result['status'] = -1;
            $result['message'] = $e->getMessage();
        }
        echo json_encode($result);
    }

    public function updateArticleContent()
    {
        $id = $this->input->post('id', true);
        $title = $this->input->post('title', true);
        $sapo = $this->input->post('sapo', true);
        $arr = array(
            'title' => $title,
            'sapo' => $sapo,
            'channel_article_id' => rand(1000, 9999),
            'layout' => 'http://prlayout.cnnd.vn/previews/komagroup-ngay-lap-tuc-bat-tay-03-doi-tac-chien-luoc-trong-ngay-ra-mat-thi-truong-viet-nam-20200204151200388.chn'
        );
        $this->Article_model->update($id, $arr);
        $result['status'] = 1;
        $result['message'] = 'Dựng bài thành công!';
        $result['redirect'] = base_url('article');
        echo json_encode($result);
        die;
    }

}