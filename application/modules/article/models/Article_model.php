<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article_model extends Base_model
{

    protected $table = 'articles';

    function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }


    public function getMyAll($key = null, $status = 'all', $category = 'all')
    {
        $user_id = $this->session->userdata('user_id');
        $website = $this->session->userdata('currentWebsite');
        $this->db->select('A.*, W.name as website_name, C.name as category_name, U.username as username, ag.name as agencie_name, P.name as position_name,P.price as price');
        $this->db->join('websites W', 'W.id = A.website_id', 'inner');
        $this->db->join('categories C', 'C.id = A.category_id', 'inner');
        $this->db->join('users U', 'U.id = A.user_id', 'inner');
        $this->db->join('agencies ag', 'ag.website_id = A.website_id', 'inner');
        $this->db->join('positions P', 'P.id = A.position_id', 'inner');
        $this->db->where('A.website_id', $website['id']);
        if ($status != 'all') $this->db->where('A.status', $status);
        if ($category != 'all') $this->db->where('A.category_id', $category);
        if ($key) {
            $this->db->like('A.title', $key);

        }
        $this->db->where('A.user_id', $user_id);
        $this->db->order_by('created_on', 'desc');
        $res = $this->db->get($this->table . ' A')->result();
        if (!$res) return 0;
        return $res;
    }

    public function getArtileInfo($articleId)
    {
        $website = $this->session->userdata('currentWebsite');
        $this->db->select('A.*, W.name as website_name, C.name as category_name, U.username as username');
        $this->db->join('websites W', 'W.id = A.website_id', 'inner');
        $this->db->join('categories C', 'C.id = A.category_id', 'inner');
        $this->db->join('users U', 'U.id = A.user_id', 'inner');
        $this->db->where('A.website_id', $website['id']);
        $this->db->where('A.id', $articleId);

        $res = $this->db->get($this->table . ' A')->result();
        if (!$res[0]) return 0;
        return $res[0];
    }

    public function get_category_approve_role($category_id = null)
    {
        $website = $this->session->userdata('currentWebsite');
        $user_id = $this->session->userdata('user_id');
        $this->db->select('*')
            ->where('A.website_id', $website['id'])
            ->where('A.user_id', $user_id)
            ->where('A.type', 'approve');
        if ($category_id != null) {
            $this->db->where('A.category_id', $category_id);
        }
        $res = $this->db->get('category_roles A')->result();
        if (!$res) return 0;
        return $res;
    }

    public function get_category_publish_role()
    {
        $website = $this->session->userdata('currentWebsite');
        $user_id = $this->session->userdata('user_id');
        $this->db->select('*')
            ->where('A.website_id', $website['id'])
            ->where('A.user_id', $user_id)
            ->where('A.type', 'publish');
        $res = $this->db->get('category_roles A')->result();
        if (!$res) return 0;
        return $res;
    }

    public function getArticleForApprove()
    {
        $category = $this->get_category_approve_role();

        $catId = array(1000000);
        if ($category) {
            foreach ($category as $item) {
                $catId[] = $item->category_id;
            }
        }

        $website = $this->session->userdata('currentWebsite');
        $this->db->select('A.*, W.name as website_name, C.name as category_name, U.username as username, ag.name as agencie_name,P.name as position_name, P.price as price');
        $this->db->join('websites W', 'W.id = A.website_id', 'inner');
        $this->db->join('categories C', 'C.id = A.category_id', 'inner');
        $this->db->join('users U', 'U.id = A.user_id', 'inner');
        $this->db->join('agencies ag', 'ag.website_id = A.website_id', 'inner');
        $this->db->join('positions P', 'P.id = A.position_id', 'inner');
        $this->db->where('A.website_id', $website['id']);
        $this->db->where('A.status', 1);
        $this->db->where_in('A.category_id', $catId);
        $res = $this->db->get($this->table . ' A')->result();
        if (!$res) return 0;
        return $res;
    }

    public function getArticleForPublish()
    {
        $category = $this->get_category_publish_role();
        $catId = array(1000000);
        if ($category) {
            foreach ($category as $item) {
                $catId[] = $item->category_id;
            }
        }

        $website = $this->session->userdata('currentWebsite');
        $this->db->select('A.*, W.name as website_name, C.name as category_name, U.username as username, ag.name as agencie_name,P.name as position_name, P.price as price');
        $this->db->join('websites W', 'W.id = A.website_id', 'inner');
        $this->db->join('categories C', 'C.id = A.category_id', 'inner');
        $this->db->join('users U', 'U.id = A.user_id', 'inner');
        $this->db->join('agencies ag', 'ag.website_id = A.website_id', 'inner');
        $this->db->join('positions P', 'P.id = A.position_id', 'inner');
        $this->db->where('A.website_id', $website['id']);
        $this->db->where('A.status', 3);
        $this->db->where_in('A.category_id', $catId);
        $res = $this->db->get($this->table . ' A')->result();
        if (!$res) return 0;
        return $res;
    }

    public function getArticleApproved()
    {
        $user_id = $this->session->userdata('user_id');
        $website = $this->session->userdata('currentWebsite');
        $this->db->select('A.*, W.name as website_name, C.name as category_name, U.username as username,ag.name as agencie_name,P.name as position_name, P.price as price');
        $this->db->join('websites W', 'W.id = A.website_id', 'inner');
        $this->db->join('categories C', 'C.id = A.category_id', 'inner');
        $this->db->join('users U', 'U.id = A.user_id', 'inner');
        $this->db->join('agencies ag', 'ag.website_id = A.website_id', 'inner');
        $this->db->join('positions P', 'P.id = A.position_id', 'inner');
        $this->db->where('A.website_id', $website['id']);
        $this->db->where('A.approved_by_id', $user_id);

        $res = $this->db->get($this->table . ' A')->result();
        if (!$res) return 0;
        return $res;
    }

    public function getNotArticleApproved()
    {
        $user_id = $this->session->userdata('user_id');
        $website = $this->session->userdata('currentWebsite');
        $this->db->select('A.*, W.name as website_name, C.name as category_name, U.username as username,ag.name as agencie_name,P.name as position_name, P.price as price');
        $this->db->join('websites W', 'W.id = A.website_id', 'inner');
        $this->db->join('categories C', 'C.id = A.category_id', 'inner');
        $this->db->join('users U', 'U.id = A.user_id', 'inner');
        $this->db->join('agencies ag', 'ag.website_id = A.website_id', 'inner');
        $this->db->join('positions P', 'P.id = A.position_id', 'inner');
        $this->db->where('A.website_id', $website['id']);
        $this->db->where('A.status', '-2');

        $res = $this->db->get($this->table . ' A')->result();
        if (!$res) return 0;
        return $res;
    }

    public function getArticlePublished($user_id = null, $website_id = null)
    {
        if (!$user_id) $user_id = $this->session->userdata('user_id');

        $website = $this->session->userdata('currentWebsite');
        if ($website_id) $website['id'] = $website_id;
        $this->db->select('A.*, W.name as website_name, C.name as category_name, U.username as username,ag.name as agencie_name,P.name as position_name, P.price as price');
        $this->db->join('websites W', 'W.id = A.website_id', 'inner');
        $this->db->join('categories C', 'C.id = A.category_id', 'inner');
        $this->db->join('users U', 'U.id = A.user_id', 'inner');
        $this->db->join('agencies ag', 'ag.website_id = A.website_id', 'inner');
        $this->db->join('positions P', 'P.id = A.position_id', 'inner');
        $this->db->where('A.website_id', $website['id']);
        $this->db->where('A.published_by_id', $user_id);

        $res = $this->db->get($this->table . ' A')->result();
        if (!$res) return 0;
        return $res;
    }

    public function getNotArticlePublished($user_id = null, $website_id = null)
    {
        if (!$user_id) $user_id = $this->session->userdata('user_id');

        $website = $this->session->userdata('currentWebsite');
        if ($website_id) $website['id'] = $website_id;
        $this->db->select('A.*, W.name as website_name, C.name as category_name, U.username as username,ag.name as agencie_name,P.name as position_name, P.price as price');
        $this->db->join('websites W', 'W.id = A.website_id', 'inner');
        $this->db->join('categories C', 'C.id = A.category_id', 'inner');
        $this->db->join('users U', 'U.id = A.user_id', 'inner');
        $this->db->join('agencies ag', 'ag.website_id = A.website_id', 'inner');
        $this->db->join('positions P', 'P.id = A.position_id', 'inner');
        $this->db->where('A.website_id', $website['id']);
        $this->db->where('A.status', '-4');
        //$this->db->where('A.published_by_id', $user_id);

        $res = $this->db->get($this->table . ' A')->result();
        if (!$res) return 0;
        return $res;
    }

    public function getArticleBtv($user_id, $fromdate, $todate)
    {
        $website = $this->session->userdata('currentWebsite');
        $this->db->select('A.*, W.name as website_name, C.name as category_name, U.username as username, P.price as price');
        $this->db->join('websites W', 'W.id = A.website_id', 'inner');
        $this->db->join('categories C', 'C.id = A.category_id', 'inner');
        $this->db->join('users U', 'U.id = A.user_id', 'inner');
        $this->db->join('positions P', 'P.id = A.position_id', 'inner');
        $this->db->where('A.website_id', $website['id']);
        $this->db->where('A.user_id', $user_id);
        $this->db->where('A.publish_date >=', $fromdate);
        $this->db->where('A.publish_date <=', $todate);

        $res = $this->db->get($this->table . ' A')->result();
        if (!$res) return 0;
        return $res;
    }

    public function getArticleContinue($user_id,$status,$startdate,$enddate)
    {
        $website = $this->session->userdata('currentWebsite');
        $this->db->select('*');
        $this->db->where('A.website_id', $website['id']);
        $this->db->where('A.user_id', $user_id);
        //$this->db->where_not_in('A.status', array(-1,4));
        $this->db->where_not_in('A.status', $status);
        $this->db->where('A.created_on BETWEEN "'.$startdate.'" and "'.$enddate.'"');
        $res = $this->db->get($this->table . ' A')->result();
        if (!$res) return 0;
        return $res;
    }

    public function getArticlePrice($user_id,$status,$startdate,$enddate)
    {
        $website = $this->session->userdata('currentWebsite');
        $this->db->select('count(A.id) as amount');
        $this->db->select_sum('P.price');
        $this->db->join('positions P', 'P.id = A.position_id', 'inner');
        $this->db->where('A.website_id', $website['id']);
        $this->db->where('A.user_id', $user_id);
        //$this->db->where_not_in('A.status', array(-1,4));
        $this->db->where_in('A.status', $status);
        $this->db->where('A.created_on BETWEEN "'.$startdate.'" and "'.$enddate.'"');
        $res = $this->db->get($this->table . ' A')->row();
        if (!$res) return 0;
        return $res;
    }

    public function getArticleWithCategory($startdate,$enddate){
        $website = $this->session->userdata('currentWebsite');
        $result = $this->db->select('c.name,c.id,COUNT(a.id) AS count,
                    (select count(id) from articles where status = 2 and category_id = c.id) as approved,
                    (select count(id) from articles where status = 4 and category_id = c.id) as posted,
                    (select count(id) from articles where status = 4 and category_id = c.id) as publish')
                //->select('(select  SUM(publish * p.price) from articles as ar INNER JOIN positions p ON p.id = ar.position_id where ar.status = 4 and ar.category_id = c.id GROUP BY p.price) as publish_price')
                ->join('articles a', 'a.category_id = c.id', 'inner')
                ->where('a.website_id', $website['id'])
                ->where('a.created_on BETWEEN "'.$startdate.'" and "'.$enddate.'"')
                ->group_by("c.name,c.id")
                ->order_by("c.name")->get('categories c')->result();

        return $result;

    }



    function date_between($date_start, $date_end)
    {
        if(!$date_start || !$date_end) return 0;
       
        if( class_exists('DateTime') )
        {
            $date_start    = new DateTime( $date_start );
            $date_end    = new DateTime( $date_end );
            return $date_end->diff($date_start)->format('%a');
        }
        else
        {           
            return abs( round( ( strtotime($date_start) - strtotime($date_end) ) / 86400 ) );
        }
    }

    function combineWithValues($array1, $array2, $search)
    {
        foreach($array1 as $key1 => $val1){
            foreach($array2 as $key2 => $val2){
                if($array1[$key1][$search] == $array2[$key2][$search]){
                    $array1[$key1] = $array2[$key2];
                }
            }
        }
        return $array1;
    } 

    public function getArticlePriceDay($status,$startdate,$enddate)
    {
        $website = $this->session->userdata('currentWebsite');
        $daylist= [];
        $countday = $this->date_between($startdate,$enddate);
        for($i=0;$i<=$countday;$i++){
            $datetime = new DateTime($startdate);
            $datetime->modify("+".$i." day");
            array_push($daylist,array('price'=>0,'date'=>$datetime->format('Y-m-d')));
        }

        $this->db->select('P.price,DATE_FORMAT(A.created_on, "%Y-%m-%d") as date');
        $this->db->join('positions P', 'P.id = A.position_id', 'inner');
        $this->db->where('A.website_id', $website['id']);
        $this->db->where_in('A.status', $status);
        $this->db->where('date(A.created_on) >= "'.$startdate.'" and date(A.created_on) <= "'.$enddate.'"');
        $res = $this->db->get($this->table . ' A')->result_array();
        
        $result = array_map(function($v){
            return intval($v['price']);
        },$this->combineWithValues($daylist, $res, 'date'));

        if (!$result) return 0;
        return $result;
    }


    public function getArticleAmountDay($status,$startdate,$enddate)
    {
        $website = $this->session->userdata('currentWebsite');
       
        $daylist= [];
        $countday = $this->date_between($startdate,$enddate);
        for($i=0;$i<=$countday;$i++){
            $datetime = new DateTime($startdate);
            $datetime->modify("+".$i." day");
            array_push($daylist,array('amount'=>0,'date'=>$datetime->format('Y-m-d')));
        }
        $this->db->select('count(A.id) as amount, DATE_FORMAT(A.created_on, "%Y-%m-%d") as date');
        $this->db->where('A.website_id', $website['id']);
        $this->db->where_in('A.status', $status);
        $this->db->where('date(A.created_on) >= "'.$startdate.'" and date(A.created_on) <= "'.$enddate.'"');
        $this->db->group_by("A.id");
        $res = $this->db->get($this->table . ' A')->result_array();

        $result = array_map(function($v){
                return intval($v['amount']);
            },$this->combineWithValues($daylist, $res, 'date'));

        if (!$result) return 0;
        return $result;
    }


    public function getArticlePublishedDay($startdate,$enddate,$key=null,$category=null,$agencie=null,$position=null)
    {

        $website = $this->session->userdata('currentWebsite');
        $this->db->select('A.*, W.name as website_name, ag.name as agencies_name, C.name as category_name, P.name as position_name, P.price as price');
        $this->db->join('websites W', 'W.id = A.website_id', 'inner');
        $this->db->join('categories C', 'C.id = A.category_id', 'inner');
        $this->db->join('agencies ag', 'ag.website_id = A.website_id', 'inner');
        $this->db->join('positions P', 'P.id = A.position_id', 'inner');
        $this->db->where('A.website_id', $website['id']);
        $this->db->where('A.status', '4');
        $this->db->where('date(A.created_on) >= "'.$startdate.'" and date(A.created_on) <= "'.$enddate.'"');

        if($key != null){
            $this->db->like('A.title', $key);
        }
        if($category != null){
            $this->db->where('C.id', $category);
        }
        if($agencie != null){
            $this->db->where('ag.id', $agencie);
        }
        if($position != null){
            $this->db->where('P.id', $position);
        }


        $res = $this->db->get($this->table . ' A')->result();
        if (!$res) return 0;
        return $res;
    }


}