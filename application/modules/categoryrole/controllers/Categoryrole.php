<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Categoryrole extends Front_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('user/User_model');
        $this->load->model('website/Website_model');
        $this->load->model('log/Log_model');
        $this->load->model('category/Category_model');
        $this->load->model('categoryrole/Categoryrole_model');
        $this->load->model('article/Article_model');
    }

    public function getCategoryRole()
    {
        $user_id = $this->input->post('user_id',true);
        $website_id = $this->input->post('website_id',true);
        $type = $this->input->post('type',true);
        $info = $this->Categoryrole_model->getCategoryRoleByWebsite($user_id, $website_id,$type);
       echo json_encode($info);die;
    }
}