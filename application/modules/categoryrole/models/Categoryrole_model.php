<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoryrole_model extends Base_model
{

    protected $table = 'category_roles';

    function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function checkCategoryRule($userid, $categoryId, $type)
    {
        $this->db->select('*')
            ->where('user_id', $userid)
            ->where('category_id', $categoryId)
            ->where('type', $type);
        $result = $this->db->get('category_roles', 1)->result();
        if (!isset($result[0])) return 0;
        return $result;
    }


    public function getCategoryRoleByWebsite($userid, $website_id,$type)
    {
        $this->db->select('*')
            ->where('user_id', $userid)
            ->where('type', $type)
            ->where('website_id', $website_id);
        $result = $this->db->get('category_roles')->result();
        if (!isset($result[0])) return 0;
        return $result;
    }
}