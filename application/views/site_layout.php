<?php
$_CI = &get_instance();
$_CI->load->model('website/Website_model');

$ssCurentWebsite = $this->session->userdata('currentWebsite');
$user_data = $this->session->userdata('user_data');
$can = $this->session->userdata('can');

if (!$this->session->userdata('user_id')) {
    redirect('login');
}
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">


    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/mengamenu.css') ?>">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css') ?>">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css">

    <script>var base_url = '<?php echo base_url()?>';</script>
    <script src="<?php echo base_url('assets/js/jquery-1.11.3.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/megamenu.js') ?>"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-datepicker.min.js') ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-autocomplete/1.0.7/jquery.auto-complete.min.js"></script>

    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-date-range-picker/0.20.0/daterangepicker.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-date-range-picker/0.20.0/jquery.daterangepicker.min.js"></script> -->

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />


    <script src="<?php echo base_url('js/main.js') ?>"></script>
    <title>CMS PR</title>
    <script>
        $(document).ready(function () {
             $('.select2').select2({});
        });
    </script>
    <style>
        .date-picker-wrapper .footer {
            background: transparent !important;
        }
    </style>
</head>
<body>
<header>
    <section class="nav-menu">
        <div class="container-fluid">

            <div class="logo hidden-xs">
                <a href="<?php echo base_url() ?>" class="menu-logo">
                    <img alt="Booking PR Admicro" src="<?php echo base_url('assets/images/logo.png') ?>">
                </a>
            </div>
            <div class="menu margin-center">

                <ul>
                    <!-- <li>
                        <?php
                            $fromdate = date('Y-m-d', strtotime('-7 days'));
                            $todate = date('Y-m-d');
                        ?>
                        <input type="text"
                            value="<?php echo date('d-m-Y', strtotime($fromdate)) . ' to ' . date('d-m-Y', strtotime($todate)) ?>"
                            class="Inputdateranger form-control">
                    </li> -->
                    <li>
                        <a href="<?php echo base_url(); ?>"><i class="fa fa-history"></i>Bảng điều khiển</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('article/create'); ?>"><i class="fa fa-pencil"></i>
                            Viết bài </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('article'); ?>"><i class="fa fa-calendar"></i>
                            Danh sách bài viết </a>
                    </li>
                    <?php
                    if ($can['approve'] > 0 || $this->session->userdata('role') == 1) {
                        ?>
                        <li class="menu-dropdown-icon">
                            <a href="<?php echo base_url('article/approve'); ?>"><i class="fa fa-check"></i>
                                Duyệt bài </a>
                            <ul class="normal-sub" style="display: none;">
                                <li><a href="<?php echo base_url('article/approve'); ?>">Bài viết chờ duyệt</a></li>
                                <li><a href="<?php echo base_url('article/approved'); ?>">Bài viết đã duyệt</a></li>
                                <li><a href="<?php echo base_url('article/notapproved'); ?>">Bài viết không duyệt</a></li>

                            </ul>
                        </li>
                    <?php } ?>
                    <?php
                    if ($can['publish'] > 0 || $this->session->userdata('role') == 1) {
                        ?>
                        <li class="menu-dropdown-icon">
                            <a href="<?php echo base_url('article/publish'); ?>"><i class="fa fa-upload"></i>
                                Đăng bài </a>
                            <ul class="normal-sub" style="display: none;">
                                <li><a href="<?php echo base_url('article/publish'); ?>">Bài viết chờ đăng</a></li>
                                <li><a href="<?php echo base_url('article/published'); ?>">Bài viết đã đăng</a></li>
                                <li><a href="<?php echo base_url('article/notpublished'); ?>">Bài viết không đăng</a></li>

                            </ul>
                        </li>

                    <?php } ?>
                    <?php if ($this->session->userdata('role') == 1) { ?>
                    <li class="menu-dropdown-icon">
                        <a href="javascript:void(0);"><i class="glyphicon glyphicon-cog"></i>
                            Quản lý
                        </a>
                        <ul class="normal-sub" style="display: none;">
                            <?php //if ($this->session->userdata('role') == 1) { ?>
                                <li><a href="<?php echo base_url('agency'); ?>">Quản lý đại lý</a></li>
                                <li><a href="<?php echo base_url('user'); ?>">Quản lý người dùng</a></li>
                                <li><a href="<?php echo base_url('category'); ?>">Quản lý chuyên
                                        mục <?php echo $ssCurentWebsite['name']; ?></a></li>
                                <li><a href="<?php echo base_url('position'); ?>">Quản lý vị trí bài</a></li>
                            <?php //} ?>
                            <!-- <li><a href="<?php echo base_url('customer'); ?>">Quản lý khách hàng</a></li> -->

                        </ul>
                    </li>
                    <?php } ?>

                    <li class="menu-dropdown-icon">
                        <a href="<?php echo base_url('report'); ?>"><i class="glyphicon glyphicon-cog"></i>
                            Thống kê
                        </a>
                    </li>

                    <?php
                    if ($this->session->userdata('role') == -1) {
                        ?>
                        <li class="menu-dropdown-icon">
                            <a href="javascript:void(0);"><i class="glyphicon glyphicon-cog"></i>
                                Quản lý danh mục
                            </a>
                            <ul class="normal-sub" style="display: none;">
                                <li><a href="<?php echo base_url('website'); ?>">Quản lý website</a></li>
                                <li><a href="<?php echo base_url('customer') ?>"> Quản lý khách hàng </a></li>
                                <li><a href="<?php echo base_url('position') ?>"> Quản lý loại bài </a></li>
                                <li><a href="<?php echo base_url('user/listing') ?>"> Quản lý thành viên </a></li>
                            </ul>
                        </li>
                    <?php } ?>
                    <!--<li class="menu-dropdown-icon">
                        <a href="javascript:void(0);">
                            <i class="fa fa-user-circle-o"></i>luonghoangxuan<i class="fa fa-angle-down caret-down"></i>
                        </a>
                        <ul class="normal-sub" style="display: none;">
                            <li><a href="<?php /*echo base_url();*/ ?>">Thông tin tài khoản</a></li>

                            <li>
                                <a href="https://pr.admicro.vn/logout?url=https%3A%2F%2Fpr.admicro.vn%2F">
                                    Thoát </a>
                            </li>
                        </ul>
                    </li>-->
                    <li>
                        <?php
                        $websiteRole = $_CI->Website_model->getWebsiteLists();

                        ?>
                        <select name="" id="headerChangeWebsite" class="form-control selectWebsite select2">
                            <?php
                            if ($websiteRole) {
                                foreach ($websiteRole as $item) {
                                    ?>
                                    <option value="<?php echo $item->id ?>"<?php echo ($ssCurentWebsite['id'] == $item->id) ? 'selected' : ''; ?>><?php echo $item->name; ?>
                                        - <?php echo $item->domain; ?></option>
                                    <?php
                                }
                            } else {
                                ?>
                                <option value="">Chuyển website</option>
                                <?php
                            }
                            ?>
                        </select>
                    </li>
                    <li class="menu-dropdown-icon"><a
                                href="<?php echo base_url('logout') ?>"><?php echo $user_data['username']; ?> <i class="glyphicon glyphicon-user	
"></i></a>
                        <ul class="normal-sub" style="display: none;">
                            <!--    <li><a href="#">Thông tin tài khoản</a></li>-->

                            <li><a href="<?php echo base_url('logout') ?>"> Thoát </a></li>
                        </ul>
                    </li>
                </ul>

            </div>
        </div>
    </section>
    <style>
        .selectWebsite {
            border-radius: 1px;
            border: 0;
            box-shadow: none;
            border-bottom: 1px #ccc solid;
            width: 150px;
        }
    </style>
</header>
<div class="main">
    <?php echo $this->load->view($template); ?>
</div>
</div>
<footer>
    <section class="footer">
        <div class="container-fluid">
            <a href="http://admicro.vn" target="_blank" class="ver-middle vccorp">
                <img class="ver-middle marleft5" alt="admicro.vn"
                     src="https://pr.admicro.vn/skins/images/admicro.png">
            </a>
			<span class="ver-middle copyright">| 2020 © PRSolution. All Rights Reserved.</span>
            <!-- <a target="_blank" href="http://vccorp.vn" class="vccorp">
                <img src="http://adi.vcmedia.vn/logo/runbyvcc.v.png" alt="vccorp.vn" height="45">
            </a>
            <a href="http://admicro.vn" target="_blank" class="ver-middle vccorp">
                <img class="ver-middle marleft5" alt="admicro.vn"
                     src="https://admicro.vn/skins/default/images/logo.png">
            </a> -->


        </div>
    </section>
</footer>
<script>

</script>
<!--<pre>
    <?php


    $queries = $this->db->queries;

    foreach ($queries as $query) {
        echo $query . '<br/>----<br/>';
    }

    print_r($_SESSION);
    ?>
</pre>-->
</body>
</html>