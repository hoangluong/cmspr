<?php
$user = $this->session->userdata('user_data');
if($user['role']==1)
{
    $role='admin';
}
if($user['role']==2)
{
    $role = 'user';
}
?>
<style>
    .ul-black li a{
        color: black !important;
    }
</style>
<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url(); ?>">CAPTCHA.VN</a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
                    <li><a href="#">Giới thiệu</a></li>
                    <li><a href="#">Tích hợp API</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <?php
                    if (isset($user)) {
                        ?>
                        <?php 
                            if($user['role']==1)
                            {
                                ?>
                                <li>
                            <a href="<?php echo base_url('logging') ?>"><button class="btn btn-info">Admin</button></a>
                        </li>
                                <?php
                            }
                         ?>
                        
                        <li>
                           <div class="dropdown" style="">
                              <button style="margin-top: 7px;" class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <?php 
                                    echo "<b><span class='text-danger'>$role</span></b>".":".$user['username'];
                                 ?>
                                <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu ul-black" aria-labelledby="dropdownMenu1">
                                <li><a href="<?php echo base_url('bill/createbill') ?>"><i class="fas fa-shopping-cart"></i> Mua Request</a></li>
                                <li><a href="<?php echo base_url('auth/change_user_info') ?>"> Đổi thông tin tài khoản</a></li>
                                <?php 
                                    if($user['role']==1)
                                    {
                                        ?>
                                        <li><a href="<?php echo base_url('logging') ?>"><i class="fas fa-chart-line"></i> Thống kê</a></li>
                                        <li><a href="<?php echo base_url('manage') ?>"><i class="fas fa-users-cog"></i> Quản lý user</a></li>
                                        <li><a href='<?php echo base_url('bill/adminapprovedbill') ?>'><i class='fas fa-circle-notch'></i> Duyệt tiền</a></li>
                                        <?php
                                    }
                                 ?>
                                <li role="separator" class="divider"></li>
                                <li><a href="<?php echo base_url('logout'); ?>"><i class="fas fa-sign-out-alt"></i> Đăng xuất</a></li>
                              </ul>
                            </div>
                        <li>

                        </li>
                        <?php
                    } else {
                        ?>
                        <li><a href="<?php echo base_url('register') ?>"><span
                                        class="glyphicon glyphicon-log-in"></span>
                                Đăng ký</a></li>
                        <li><a href="<?php echo base_url('login') ?>"><span class="glyphicon glyphicon-user"></span>
                                Đăng
                                nhập</a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</nav>