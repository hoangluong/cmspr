<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css">

    <link rel="stylesheet" href="<?php echo base_url('publics/fontawesome/css/all.css'); ?>">

    <link rel="stylesheet" href="<?php echo base_url('publics/css/style.css'); ?>">
    <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>

    <script style="<?php echo base_url('publics/js/js.js'); ?>"></script>
    <title>Captcha.vn</title>
</head>
<body>
<?php $this->load->view('header'); ?>
<?php $this->load->view($template); ?>
<section class="index-link">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="link-area">
                    <h3>CAPTCHA.VN</h3>
                    <hr>
                    <P>
                    </P>
                    <li class="fa-li"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li class="fa-li"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li class="fa-li"><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                </div>
            </div>

            <div class="col-md-3">
                <div class="link-area">
                    <h3>HƯỚNG DẪN</h3>
                    <hr>
                    <ul>
                        <li><a href="#"> Tích hợp API</a></li>
                        <li><a href="#"> Điều khoản sử dụng dịch vụ</a></li>

                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="link-area">
                    <h3>Hỗ trợ 24/7</h3>
                    <hr>
                    <ul>
                        <li>CSKH:</li>
                        <li>Kỹ thuật:</li>
                        <li>Email: shopdoithe@gmail.com</li>
                        <li>Telegram:</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="index-social">
    <div class="container">
        <div class="row index-social-link text-center">
            <p class="copy-c">Bản quyền thuộc về © CAPTCHA.VN </p>
        </div>
    </div>
</section>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
</body>
</html>