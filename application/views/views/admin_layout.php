<?php 
	if($this->session->userdata('user_data')['role']!=1)
	{
		redirect(base_url());
	}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>ADMIN CAPTCHAVN</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css">

    <link rel="stylesheet" href="<?php echo base_url('publics/fontawesome/css/all.css'); ?>">

    <link rel="stylesheet" href="<?php echo base_url('publics/css/style.css'); ?>">
    <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>

    <script src="<?php echo base_url('publics/js/js.js'); ?>"></script>  
</head>
<body>	
	<div class="container-fluid admin_layout">
		<div class="row">
			<div class="col-md-2 menu-left">
				<div class="container-fluid menu-left-title">
					<i class="fas fa-user-tie"></i> <?php echo $this->session->userdata('user_data')['username'] ?>
				</div>
				<div class="container-fluid menu-left-content">
					<div class="main main-menu">
			<div class="row">
				<div class="col-md-10"><i class="fas fa-home"></i>Tổng quan</div>
				<div class="col-md-2"><i class="fas fa-caret-down"></i></div>
			</div>
		</div>
		<div class="sub sub-menu">
			<ul>
				<li><a href="<?php echo base_url('logging') ?>">Thống kê</a></li>
				<li><a href='<?php echo base_url('bill/adminapprovedbill') ?>'> Duyệt đơn</a></li>
			</ul>
		</div>

		<div class="main main-menu">
			<div class="row">
				<div class="col-md-10"><i class="fas fa-info-circle"></i> Thông tin</div>
				<div class="col-md-2"><i class="fas fa-caret-down"></i></div>
			</div>
		</div>
		<div class="sub sub-menu">
			<ul>
				<li><a href="">Thông tin</a></li>
			</ul>
		</div>

		<div class="main main-menu">
			<div class="row">
				<div class="col-md-10"><i class="fas fa-rss"></i> Tin tức</div>
				<div class="col-md-2"><i class="fas fa-caret-down"></i></div>
			</div>
		</div>
		<div class="sub sub-menu">
			<ul>
				<li><a href="">Tin tức</a></li>
				<li><a href="">Danh mục tin tức</a></li>
			</ul>
		</div>

		<div class="main main-menu">
			<div class="row">
				<div class="col-md-10"><i class="fas fa-history"></i> Lịch sử</div>
				<div class="col-md-2"><i class="fas fa-caret-down"></i></div>
			</div>
		</div>
		<div class="sub sub-menu">
			<ul>
				<li><a href="">Nạp tiền</a></li>
				<li><a href="">Mua hàng</a></li>
			</ul>
		</div>

		<div class="main main-menu">
			<div class="row">
				<div class="col-md-10"><i class="fas fa-users"></i> Khách hàng</div>
				<div class="col-md-2"><i class="fas fa-caret-down"></i></div>
			</div>
		</div>
		<div class="sub sub-menu">
			<ul>
				<li><a href="<?php echo base_url('manage') ?>">Khách hàng</a></li>
			</ul>
		</div>

		<div class="main main-menu">
			<div class="row">
				<div class="col-md-10"><i class="fas fa-cog"></i> Cấu hình</div>
				<div class="col-md-2"><i class="fas fa-caret-down"></i></div>
			</div>
		</div>
		<div class="sub sub-menu">
			<ul>
				<li><a href="">Thiết lập chung</a></li>
				<li><a href="">Menu</a></li>
				<li><a href="">SlideShow</a></li>
			</ul>
		</div>
				</div>
			</div>
			<div class="col-md-10 menu-right">				
				<div class="container-fliud">
					<nav class="navbar-default navbar">
						<button class="navbar-right btn btn-danger navbar-btn" style="margin-right: 30px;" data-toggle="modal" data-target=".exitmodal">Thoát</button>
					</nav>
					<?php $this->load->view($template); ?>
				</div>
			</div>
		</div>
	</div>
	
<div class="modal fade exitmodal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <h3 class="text-center">Xác nhận thoát</h3>
      <div class="container-fluid text-center" style="padding: 20px;">
      	<a href="<?php echo base_url('logout') ?>"><button class="btn btn-danger">Thoát</button></a>
      </div>
    </div>
  </div>
</div>
	
	
</body>
</html>






